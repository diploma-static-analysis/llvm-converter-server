package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty

class Annotations(
  @JsonProperty("annotations")
  val annotations: List<Annotation>
)

class Annotation(
  @JsonProperty("class_name")
  val className: String,
  @JsonProperty("parameters")
  val parameters: List<String>
)