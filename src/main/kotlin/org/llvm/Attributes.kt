package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.EnumDeserializer

@JsonIgnoreProperties("const_formals", "is_generated", "is_obj_c_instance_method", "is_cpp_no_except_method",
  "is_java_synchronized_method", "is_c_sharp_synchronized_method", "is_model", "is_specialized", "is_variadic", "translation_unit")
class Attributes(
  @JsonProperty("access")
  val access: Access,
  @JsonProperty("captured")
  val captured: List<CapturedVariable>,
  @JsonProperty("formals")
  val formals: List<Formal>,
  @JsonProperty("method_annotations")
  val methodAnnotation: MethodAnnotation,
  @JsonProperty("locals")
  val locals: List<Local>,
  @JsonProperty("loc")
  val location: Location,
  @JsonProperty("proc_name")
  val procName: Method,
  @JsonProperty("exceptions")
  val exceptions: List<String>,
  @JsonProperty("is_abstract")
  val isAbstract: Boolean,
  @JsonProperty("is_bridge_method")
  val isBridgeMethod: Boolean,
  @JsonProperty("is_defined")
  val isDefined: Boolean,
  @JsonProperty("is_synthetic_method")
  val isSyntheticMethod: Boolean,
  @JsonProperty("ret_type")
  val returnType: Type
) {
  @JsonDeserialize(using = Access.Deserializer::class)
  enum class Access(val identifier: String) {
    DEFAULT("Default"), PUBLIC("Public"), PRIVATE("Private"), PROTECTED("Protected");

    override fun toString(): String = identifier

    class Deserializer : EnumDeserializer<Access>(Access.entries, { it.identifier })
  }

  class CapturedVariable(
    @JsonProperty("name")
    val name: PVariable,
    @JsonProperty("type")
    val type: Type
  )

  class Formal(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("type")
    val type: Type
  )

  class MethodAnnotation(
    @JsonProperty("params")
    val annotations: List<Annotation>,
    @JsonProperty("return_value")
    val returnValueAnnotation: Annotations
  )

  class Local(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("type")
    val type: Type,
    @JsonProperty("modify_in_block")
    val modifyInBlock: Boolean,
    @JsonProperty("is_const_expr")
    val isConstExpr: Boolean
  )
}