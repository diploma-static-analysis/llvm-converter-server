package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty


class ClassDescription(
  @JsonProperty("type_name")
  val typeName: TypeName,
  @JsonProperty("type_struct")
  val typeStruct: TypeStruct
) {
  class ClassDescriptionContainer(
    val typeName: TypeName,
    val typeStruct: TypeStructContainer
  ) {
    class TypeStructContainer(
      val instanceFields: MutableList<Field.InstanceField> = mutableListOf(),
      val staticFields: MutableList<Field.StaticField> = mutableListOf(),
      val supers: MutableList<SuperTypeKind> = mutableListOf(),
      val methods: MutableList<Method> = mutableListOf(),
      val annotations: MutableList<Annotation> = mutableListOf()
    ) {
      fun toTypeStruct(): TypeStruct = TypeStruct(
        instanceFields = instanceFields.toList(),
        staticFields = staticFields.toList(),
        supers = supers.toList(),
        methods = methods.toList(),
        annotations = annotations.toList()
      )
    }
    
    fun toClassDescription(): ClassDescription = ClassDescription(
      typeName = typeName,
      typeStruct = typeStruct.toTypeStruct(),
    )
  }
}

typealias ClassDescriptionsContainer = MutableMap<String, ClassDescription.ClassDescriptionContainer>