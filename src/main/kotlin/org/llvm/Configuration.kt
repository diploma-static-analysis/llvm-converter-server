package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties("priority_set")
class Configuration(
  @JsonProperty("procs")
  val procs: Map<String, Proc>,
  @JsonProperty("nodes")
  val nodes: List<Node>
)