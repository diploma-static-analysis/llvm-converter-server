package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.EnumDeserializer
import org.llvm.util.MapDeserializer

@JsonDeserialize(using = Expression.Deserializer::class)
sealed class Expression(
  @JsonProperty("expr_kind")
  val expressionKind: String // TODO Kind should be enum
) {
  @JsonDeserialize(using = JsonDeserializer.None::class)
  @JsonIgnoreProperties("from_this")
  class VariableExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "VarExpression",
    @JsonProperty("identifier")
    val identifier: Identifier
  ) : Expression(expressionKind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class UnaryOperationExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "UnopExpression",
    @JsonProperty("operator")
    val operator: Operator,
    @JsonProperty("expression")
    val expression: Expression,
    @JsonProperty("type")
    val type: Type?
  ): Expression(expressionKind) {
    @JsonDeserialize(using = Operator.Deserializer::class)
    enum class Operator(val identifier: String) {
      NEG("Neg"), BNOT("BNot"), LNOT("LNot");
      
      override fun toString(): String = identifier
      
      class Deserializer(vc: Class<*>? = null): EnumDeserializer<Operator>(Operator.entries, Operator::identifier, vc)
    }
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class BinaryOperationExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "BinopExpression",
    @JsonProperty("operator")
    val operator: Operator,
    @JsonProperty("left")
    val left: Expression,
    @JsonProperty("right")
    val right: Expression
  ): Expression(expressionKind) {
    @JsonDeserialize(using = Operator.Deserializer::class)
    enum class Operator(val identifier: String) {
      PLUSPI("PlusPI"), MINUSA("MinusA"), MINUSPI("MinusPI"), MINUSPP("MinusPP"),
      MULT("Mult"), DIV("Div"), DIVI("DivI"), DIVF("DivF"), MOD("Mod"),
      SHIFTLT("Shiftlt"), SHIFTRT("Shiftrt"), LT("Lt"), GT("Gt"), LE("Le"),
      GE("Ge"), EQ("Eq"), NE("Ne"), BAND("BAnd"), BXOR("BXor"),
      BOR("BOr"), LAND("LAnd"), LOR("LOr");

      override fun toString(): String = identifier

      class Deserializer(vc: Class<*>? = null): EnumDeserializer<Operator>(Operator.entries, Operator::identifier, vc)
    }
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class ExitExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "ExnExpression",
    @JsonProperty("expression")
    val expression: Expression
  ): Expression(expressionKind)

  @JsonDeserialize(using = ConstExpression.Deserializer::class)
  sealed class ConstExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "ConstExpression",
    @JsonProperty("kind")
    val kind: String
  ): Expression(expressionKind) {

    @JsonDeserialize(using = JsonDeserializer.None::class)
    class IntConst(
      @JsonProperty("expr_kind")
      expressionKind: String = "ConstExpression",
      @JsonProperty("kind")
      kind: String = "Int",
      @JsonProperty("const_value")
      val constValue: SubValue
    ) : ConstExpression(expressionKind, kind) {
      @JsonIgnoreProperties("unsigned")
      class SubValue(
        @JsonProperty("value")
        val value: Long,
        @JsonProperty("is_pointer")
        val isPointer: Boolean
      )
    }

    @JsonDeserialize(using = JsonDeserializer.None::class)
    class FloatConst(
      @JsonProperty("expr_kind")
      expressionKind: String = "ConstExpression",
      @JsonProperty("kind")
      kind: String = "Float",
      @JsonProperty("const_value")
      val constValue: Float?
    ): ConstExpression(expressionKind, kind)

    @JsonDeserialize(using = JsonDeserializer.None::class)
    class FunctionConst(
      @JsonProperty("expr_kind")
      expressionKind: String = "ConstExpression",
      @JsonProperty("kind")
      kind: String = "Fun",
      @JsonProperty("const_value")
      val constValue: Method
    ): ConstExpression(expressionKind, kind)

    @JsonDeserialize(using = JsonDeserializer.None::class)
    class StringConst(
      @JsonProperty("expr_kind")
      expressionKind: String = "ConstExpression",
      @JsonProperty("kind")
      kind: String = "Str",
      @JsonProperty("const_value")
      val constValue: String
    ): ConstExpression(expressionKind, kind)

    class ClassConst(
      @JsonProperty("expr_kind")
      expressionKind: String = "ConstExpression",
      @JsonProperty("kind")
      kind: String = "Class",
      @JsonProperty("const_value")
      val constValue: String
    ): ConstExpression(expressionKind, kind)
    class Deserializer(vc: Class<*>? = null): MapDeserializer<ConstExpression>(vc, "kind", {
      when (it) {
        "Int" -> IntConst::class.java
        "Float" -> FloatConst::class.java
        "Fun" -> FunctionConst::class.java
        "Str" -> StringConst::class.java
        "Class" -> ClassConst::class.java
        else -> throw JsonParseException("Can't find type of const with type: $it")
      }
    })
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class CastExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "CastExpression",
    @JsonProperty("type")
    val type: Type,
    @JsonProperty("expression")
    val expression: Expression
  ): Expression(expressionKind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class LVariableExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "LvarExpression",
    @JsonProperty("pvar")
    val pvar: PVariable
  ): Expression(expressionKind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class LFieldExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "LfieldExpression",
    @JsonProperty("expression")
    val expression: Expression,
    @JsonProperty("identifier")
    val identifier: Identifier,
    @JsonProperty("type")
    val type: Type
  ): Expression(expressionKind) {
    class Identifier(
      @JsonProperty("field_name")
      val fieldName: String
    )
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class LIndexExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "LindexExpression",
    @JsonProperty("array")
    val array: Expression,
    @JsonProperty("index")
    val index: Expression
  ): Expression(expressionKind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class SizeOfExpression(
    @JsonProperty("expr_kind")
    expressionKind: String = "SizeofExpression",
    @JsonProperty("type")
    val type: Type,
    @JsonProperty("kind")
    val kind: Kind,
    @JsonProperty("dynamic_length")
    val dynamicLength: Expression?
  ): Expression(expressionKind) {
    @JsonDeserialize(using = Kind.Deserializer::class)
    enum class Kind(val identifier: String) {
      EXACT("exact"), INSTOF("instof"), CAST("cast");

      override fun toString(): String = identifier

      class Deserializer(vc: Class<*>? = null): EnumDeserializer<Kind>(Kind.entries, Kind::identifier, vc)
    }
  }

  class Deserializer(vc: Class<*>? = null): MapDeserializer<Expression>(vc, "expr_kind", {
    when (it) {
      "VarExpression" -> VariableExpression::class.java
      "UnopExpression" -> UnaryOperationExpression::class.java
      "BinopExpression" -> BinaryOperationExpression::class.java
      "ExnExpression" -> ExitExpression::class.java
      "ConstExpression" -> ConstExpression::class.java
      "CastExpression" -> CastExpression::class.java
      "LvarExpression" -> LVariableExpression::class.java
      "LfieldExpression" -> LFieldExpression::class.java
      "LindexExpression" -> LIndexExpression::class.java
      "SizeOfExpression" -> SizeOfExpression::class.java
      else -> throw JsonParseException("Can't find type of class with type: $it")
    }
  })
}