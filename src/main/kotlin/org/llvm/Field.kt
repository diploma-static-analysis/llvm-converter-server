package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty

abstract class Field(
  @JsonProperty("field_name")
  val fieldName: String,
  @JsonProperty("type")
  val type: Type,
  @JsonProperty("annotation")
  val annotation: Annotations
) {
  class InstanceField(
    @JsonProperty("field_name")
    fieldName: String,
    @JsonProperty("type")
    type: Type,
    @JsonProperty("annotation")
    annotation: Annotations
  ) : Field(fieldName, type, annotation)

  class StaticField(
    @JsonProperty("field_name")
    fieldName: String,
    @JsonProperty("type")
    type: Type,
    @JsonProperty("annotation")
    annotation: Annotations
  ) : Field(fieldName, type, annotation)
}