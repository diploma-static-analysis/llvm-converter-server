package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.EnumDeserializer

class Identifier(
  @JsonProperty("kind")
  val kind: Kind,
  @JsonProperty("name")
  val name: String,
  @JsonProperty("stamp")
  val stamp: Int
) {
  @JsonDeserialize(using = Kind.Deserializer::class)
  enum class Kind(val identifier: String) {
    NORMAL("Normal"), PRIMED("Primed"), FOOTPRINT("Footprint"), NONE("None");

    override fun toString(): String = identifier

    class Deserializer(vc: Class<*>? = null): EnumDeserializer<Kind>(Kind.entries, Kind::identifier, vc)
  }
}