package org.llvm

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.EnumDeserializer

@JsonDeserialize(using = IfKind.Deserializer::class)
enum class IfKind(val identifier: String) {
  K_BEXP("Ik_bexp"), IK_DOWHILE("Ik_dowhile"), IK_FOR("Ik_for"), IK_IF("Ik_if"),
  IK_LAND_FOR("Ik_land_for"), IK_SWITCH("Ik_switch");

  override fun toString(): String = identifier

  class Deserializer : EnumDeserializer<IfKind>(IfKind.entries, IfKind::identifier)
}