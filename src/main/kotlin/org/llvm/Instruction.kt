package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.MapDeserializer

@JsonDeserialize(using = Instruction.Deserializer::class)
sealed class Instruction(
  @JsonProperty("instruction_kind")
  val instructionKind: String, // TODO kind should be enum
  @JsonProperty("location")
  val location: Location
) {

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Load(
    @JsonProperty("instruction_kind")
    instructionKind: String = "Load",
    @JsonProperty("location")
    location: Location,
    @JsonProperty("identifier")
    val identifier: Identifier,
    @JsonProperty("expression")
    val expression: Expression,
    @JsonProperty("type")
    val type: Type
  ) : Instruction(instructionKind, location)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Store(
    @JsonProperty("instruction_kind")
    instructionKind: String = "Store",
    @JsonProperty("location")
    location: Location,
    @JsonProperty("lvalue")
    val lvalue: Expression,
    @JsonProperty("rvalue")
    val rvalue: Expression,
    @JsonProperty("type")
    val type: Type
  ) : Instruction(instructionKind, location)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Prune(
    @JsonProperty("instruction_kind")
    instructionKind: String = "Prune",
    @JsonProperty("location")
    location: Location,
    @JsonProperty("condition")
    val condition: Expression,
    @JsonProperty("true_branch")
    val trueBranch: Boolean,
    @JsonProperty("if_kind")
    val ifKind: IfKind
  ) : Instruction(instructionKind, location)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Call(
    @JsonProperty("instruction_kind")
    instructionKind: String = "Call",
    @JsonProperty("location")
    location: Location,
    @JsonProperty("return_var")
    val returnValue: Identifier,
    @JsonProperty("return_type")
    val returnType: Type,
    @JsonProperty("function_expression")
    val functionExpression: Expression,
    @JsonProperty("args")
    val arguments: List<Argument>,
    @JsonProperty("flags")
    val flags: Flags
  ) : Instruction(instructionKind, location) {
    class Argument(
      @JsonProperty("expression")
      val expression: Expression,
      @JsonProperty("type")
      val type: Type
    )
    @JsonIgnoreProperties("cf_noreturn")
    class Flags(
      @JsonProperty("cf_virtual")
      val virtual: Boolean,
      @JsonProperty("cf_is_objc_block")
      val isObjectBlock: Boolean
    )
  }

  class Deserializer(vc: Class<*>? = null): MapDeserializer<Instruction>(vc, "instruction_kind", {
    when (it) {
      "Load" -> Load::class.java
      "Store" -> Store::class.java
      "Prune" -> Prune::class.java
      "Call" -> Call::class.java
      else -> throw IllegalArgumentException("Unknown instruction kind: $it")
    }
  })
}