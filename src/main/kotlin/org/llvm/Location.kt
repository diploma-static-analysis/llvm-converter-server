package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

class Location(
  @JsonProperty("line")
  val line: Int,
  @JsonProperty("col")
  val column: Int,
  @JsonProperty("source_file")
  val sourceFile: SourceFile
) {
  @JsonIgnoreProperties("path_type")
  class SourceFile(
    @JsonProperty("path")
    val path: String
  )
}