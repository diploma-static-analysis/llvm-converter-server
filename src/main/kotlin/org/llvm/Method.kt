package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.MapDeserializer

@JsonDeserialize(using = Method.Deserializer::class)
abstract class Method {
  @JsonDeserialize(using = JsonDeserializer.None::class)
  class SpecialMethod(@JsonProperty("method_name") val methodName: String) : Method()
  @JsonDeserialize(using = JsonDeserializer.None::class)
  class RegularMethod(
    @JsonProperty("method_name")
    val methodName: String,
    @JsonProperty("parameters")
    val parameters: List<String>,
    @JsonProperty("class_name")
    val className: String,
    @JsonProperty("return_type")
    val returnType: String?, // todo if method_name == constructor_name
    @JsonProperty("is_static")
    val isStatic: Boolean
  ) : Method()
  class Deserializer(vc: Class<*>? = null): MapDeserializer<Method>(vc, "method_name", {
    when (it) {
      "__new_", "_new_array", "__set_locked_attribute",
      "__delete_locked_attribute", "__instanceof",
      "__cast", "__unwrap_exception", "__throw",
      "__get_array_length" -> SpecialMethod::class.java
      else -> RegularMethod::class.java
    }
  })
}