package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.EnumDeserializer
import org.llvm.util.MapDeserializer

@JsonIgnoreProperties("nd_temps", "nd_dead_pvars_before", "nd_dead_pvars_after")
@JsonDeserialize(using = Node.Deserializer::class)
sealed class Node(
  @JsonProperty("nd_instrs")
  val instructions: List<Instruction>,
  @JsonProperty("nd_proc_id")
  val procId: Int,
  @JsonProperty("nd_id")
  val id: Int,
  @JsonProperty("nd_exn_ids")
  val exitNodes: List<Int>,
  @JsonProperty("nd_loc")
  val location: Location,
  @JsonProperty("nd_pred_ids")
  val previousIds: List<Int>,
  @JsonProperty("nd_succ_ids")
  val successIds: List<Int>,
  @JsonProperty("nd_kind")
  val kind: String
) {
  @JsonDeserialize(using = JsonDeserializer.None::class)
  class StartNode(
    @JsonProperty("nd_instrs")
    instructions: List<Instruction>,
    @JsonProperty("nd_proc_id")
    procId: Int,
    @JsonProperty("nd_id")
    id: Int,
    @JsonProperty("nd_exn_ids")
    exitNodes: List<Int>,
    @JsonProperty("nd_loc")
    location: Location,
    @JsonProperty("nd_pred_ids")
    previousIds: List<Int>,
    @JsonProperty("nd_succ_ids")
    successIds: List<Int>,
    @JsonProperty("nd_kind")
    kind: String = "StartNode"
  ): Node(instructions, procId, id, exitNodes, location, previousIds, successIds, kind) {

    fun copy(
      instructions: List<Instruction>? = null,
      procId: Int? = null,
      id: Int? = null,
      exitNodes: List<Int>? = null,
      location: Location? = null,
      previousIds: List<Int>? = null,
      successIds: List<Int>? = null,
      kind: String? = null
    ): StartNode {
      return StartNode(
        instructions = instructions ?: this.instructions,
        procId = procId ?: this.procId,
        id = id ?: this.id,
        exitNodes = exitNodes ?: this.exitNodes,
        location = location ?: this.location,
        previousIds = previousIds ?: this.previousIds,
        successIds = successIds ?: this.successIds,
        kind = kind ?: this.kind
      )
    }
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class ExitNode(
    @JsonProperty("nd_instrs")
    instructions: List<Instruction>,
    @JsonProperty("nd_proc_id")
    procId: Int,
    @JsonProperty("nd_id")
    id: Int,
    @JsonProperty("nd_exn_ids")
    exitNodes: List<Int>,
    @JsonProperty("nd_loc")
    location: Location,
    @JsonProperty("nd_pred_ids")
    previousIds: List<Int>,
    @JsonProperty("nd_succ_ids")
    successIds: List<Int>,
    @JsonProperty("nd_kind")
    kind: String = "ExitNode"
  ): Node(instructions, procId, id, exitNodes, location, previousIds, successIds, kind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class StatementNode(
    @JsonProperty("nd_instrs")
    instructions: List<Instruction>,
    @JsonProperty("nd_proc_id")
    procId: Int,
    @JsonProperty("nd_id")
    id: Int,
    @JsonProperty("nd_exn_ids")
    exitNodes: List<Int>,
    @JsonProperty("nd_loc")
    location: Location,
    @JsonProperty("nd_pred_ids")
    previousIds: List<Int>,
    @JsonProperty("nd_succ_ids")
    successIds: List<Int>,
    @JsonProperty("stmt_node_comment")
    val comment: String?,
    @JsonProperty("stmt_node_kind")
    val statementKind: Kind,
    @JsonProperty("nd_kind")
    kind: String = "StatementNode"
  ): Node(instructions, procId, id, exitNodes, location, previousIds, successIds, kind) {
    @JsonDeserialize(using = Kind.Deserializer::class)
    enum class Kind(val instruction: String) {
      ASSERTION_FAILURE("AssertionFailure"), BETWEEN_JOIN_AND_EXIT("BetweenJoinAndExit"),
      BINARY_CONDITIONAL_STMT_INIT("BinaryConditionalStmtInit"), BINARY_OPERATOR_STMT("BinaryOperatorStmt"),
      CALL("Call"), CALL_OBJC_NEW("CallObjCNew"), CLASS_CAST_EXCEPTION("ClassCastException"),
      CONDITIONAL_STMT_BRANCH("ConditionalStmtBranch"), CONSTRUCTOR_INIT("ConstructorInit"),
      CXX_DYNAMIC_CAST("CXXDynamicCast"), CXX_NEW_EXPR("CXXNewExpr"),
      CXX_STD_INITIALIZER_LIST_EXPR("CXXStdInitializerListExpr"), CXX_TYPEID_EXPR("CXXTypeidExpr"),
      DECL_STMT("DeclStmt"), DEFINE_BODY("DefineBody"), EXCEPTION_HANDLER("ExceptionHandler"),
      EXCEPTIONS_SINK("ExceptionsSink"), FINALLY_BRANCH("FinallyBranch"), GCC_ASM_STMT("GCCAsmStmt"),
      GENERIC_SELECTION_EXPR("GenericSelectionExpr"), IF_STMT_BRANCH("IfStmtBranch"),
      INITIALIZE_DYNAMIC_ARRAY_LENGTH("InitializeDynamicArrayLength"), INIT_LIST_EXP("InitListExp"),
      MESSAGE_CALL("MessageCall"), METHOD_BODY("MethodBody"), MONITOR_ENTER("MonitorEnter"),
      MONITOR_EXIT("MonitorExit"), OBJCCPP_THROW("ObjCCPPThrow"), OUT_OF_BOUND("OutOfBound"),
      RETURN_STMT("ReturnStmt"), SKIP("Skip"), SWITCH_STMT("SwitchStmt"), THIS_NOT_NULL("ThisNotNull"),
      THROW("Throw"), THROWNPE("ThrowNPE"), UNARY_OPERATOR("UnaryOperator");

      override fun toString(): String = instruction

      class Deserializer : EnumDeserializer<Kind>(Kind.entries, { it.instruction })
    }
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class JoinNode(
    @JsonProperty("nd_instrs")
    instructions: List<Instruction>,
    @JsonProperty("nd_proc_id")
    procId: Int,
    @JsonProperty("nd_id")
    id: Int,
    @JsonProperty("nd_exn_ids")
    exitNodes: List<Int>,
    @JsonProperty("nd_loc")
    location: Location,
    @JsonProperty("nd_pred_ids")
    previousIds: List<Int>,
    @JsonProperty("nd_succ_ids")
    successIds: List<Int>,
    @JsonProperty("nd_kind")
    kind: String = "JoinNode"
  ): Node(instructions, procId, id, exitNodes, location, previousIds, successIds, kind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class PruneNode(
    @JsonProperty("nd_instrs")
    instructions: List<Instruction>,
    @JsonProperty("nd_proc_id")
    procId: Int,
    @JsonProperty("nd_id")
    id: Int,
    @JsonProperty("nd_exn_ids")
    exitNodes: List<Int>,
    @JsonProperty("nd_loc")
    location: Location,
    @JsonProperty("nd_pred_ids")
    previousIds: List<Int>,
    @JsonProperty("nd_succ_ids")
    successIds: List<Int>,
    @JsonProperty("true_branch")
    val trueBranch: Boolean,
    @JsonProperty("if_kind")
    val ifKind: IfKind,
    @JsonProperty("prune_node_kind")
    val pruneKind: Kind,
    @JsonProperty("nd_kind")
    kind: String = "PruneNode"
  ): Node(instructions, procId, id, exitNodes, location, previousIds, successIds, kind) {
    @JsonDeserialize(using = Kind.Deserializer::class)
    enum class Kind(val instruction: String) {
      EXCEPTION_HANDLER("ExceptionHandler"), FALSE_BRANCH("FalseBranch"), IN_BOUND("InBound"),
      IS_INSTANCE("IsInstance"), METHOD_BODY("MethodBody"), NOT_NULL("NotNull"), TRUE_BRANCH("TrueBranch");

      override fun toString(): String = instruction

      class Deserializer : EnumDeserializer<Kind>(Kind.entries, { it.instruction })
    }
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class SkipNode(
    @JsonProperty("nd_instrs")
    instructions: List<Instruction>,
    @JsonProperty("nd_proc_id")
    procId: Int,
    @JsonProperty("nd_id")
    id: Int,
    @JsonProperty("nd_exn_ids")
    exitNodes: List<Int>,
    @JsonProperty("nd_loc")
    location: Location,
    @JsonProperty("nd_pred_ids")
    previousIds: List<Int>,
    @JsonProperty("nd_succ_ids")
    successIds: List<Int>,
    @JsonProperty("skip_node_comment")
    val comment: String,
    @JsonProperty("nd_kind")
    kind: String = "SkipNode"
  ): Node(instructions, procId, id, exitNodes, location, previousIds, successIds, kind)

  class Deserializer : MapDeserializer<Node>(null, "nd_kind", {
    when (it) {
      "StartNode" -> StartNode::class.java
      "ExitNode" -> ExitNode::class.java
      "StatementNode" -> StatementNode::class.java
      "JoinNode" -> JoinNode::class.java
      "PruneNode" -> PruneNode::class.java
      "SkipNode" -> SkipNode::class.java
      else -> throw IllegalArgumentException("Unknown node kind: $it")
    }
  })
}