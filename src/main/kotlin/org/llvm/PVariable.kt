package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.EnumDeserializer
import org.llvm.util.MapDeserializer

@JsonDeserialize(using = PVariable.Deserializer::class)
sealed class PVariable(
  @JsonProperty("pv_name")
  val name: String,
  @JsonProperty("pv_kind")
  val kind: Kind
) {
  @JsonDeserialize(using = JsonDeserializer.None::class)
  class LocalVariable(
    @JsonProperty("pv_name")
    name: String,
    @JsonProperty("pv_kind")
    kind: Kind = Kind.LOCALVARIABLE,
    @JsonProperty("proc_name")
    val procName: Method
  ) : PVariable(name, kind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class CalledVariable(
    @JsonProperty("pv_name")
    name: String,
    @JsonProperty("pv_kind")
    kind: Kind = Kind.CALLEDVARIABLE,
    @JsonProperty("proc_name")
    val procName: Method
  ) : PVariable(name, kind)

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class GlobalVariable(
    @JsonProperty("pv_name")
    name: String,
    @JsonProperty("pv_kind")
    kind: Kind = Kind.GLOBALVARIABLE
  ) : PVariable(name, kind)

  class Deserializer(vc: Class<*>? = null): MapDeserializer<PVariable>(vc, "pv_kind", {
    when (it) {
      "LocalVariable" -> LocalVariable::class.java
      "CalledVariable" -> CalledVariable::class.java
      "GlobalVariable" -> GlobalVariable::class.java
      else -> throw JsonParseException("Can't find variable type with kind $it")
    }
  })

  @JsonDeserialize(using = Kind.Deserializer::class)
  enum class Kind(val identifier: String) {
    LOCALVARIABLE("LocalVariable"), CALLEDVARIABLE("CalledVariable"), GLOBALVARIABLE("GlobalVariable");

    override fun toString(): String = identifier

    class Deserializer(vc: Class<*>? = null): EnumDeserializer<Kind>(Kind.entries, Kind::identifier, vc)
  }
}