package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties("pd_nodes", "pd_exn_node")
class Proc(
  @JsonProperty("pd_start_node")
  val startNode: Int,
  @JsonProperty("pd_exit_node")
  val exitNode: Int,
  @JsonProperty("pd_id")
  val id: Int,
  @JsonProperty("pd_attributes")
  val attributes: Attributes
)