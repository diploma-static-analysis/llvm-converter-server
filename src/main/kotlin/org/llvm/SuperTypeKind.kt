package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties("type_name_kind")
class SuperTypeKind(
  @JsonProperty("csu_kind")
  val csuKind: String,
  @JsonProperty("name")
  val name: String
)