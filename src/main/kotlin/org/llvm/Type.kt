package org.llvm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.Type.Int.Kind
import org.llvm.util.EnumDeserializer
import org.llvm.util.MapDeserializer

@JsonIgnoreProperties("\$type", "is_inst_return_type")
@JsonDeserialize(using = Type.Deserializer::class)
sealed class Type(
  @JsonProperty("type_kind")
  val typeKind: String // TODO kind should be enum
) {
  abstract override fun toString(): String
  
  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Array(
    @JsonProperty("content_type")
    val contentType: Type,
    @JsonProperty("type_kind")
    typeKind: String = "Tarray"
  ) : Type(typeKind) {
    override fun toString(): String = "[$contentType]"
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Float(
    @JsonProperty("kind")
    val kind: String, // TODO, can be enum "FFloat", "FDouble", "FLongDouble"
    @JsonProperty("type_kind")
    typeKind: String = "Tfloat"
  ) : Type(typeKind) {
    override fun toString(): String = "float"
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Int(
    @JsonProperty("kind")
    val kind: Kind,
    @JsonProperty("type_kind")
    typeKind: String = "Tint"
  ) : Type(typeKind) {
    enum class Kind(val identifier: String) {
      I_CHAR("IChar"), IS_CHAR("ISChar"), IU_CHAR("IUChar"), I_BOOL("IBool"), 
      I_INT("IInt"), IU_INT("IUInt"), I_SHORT("IShort"), IU_SHORT("IUShort"), 
      I_LONG("ILong"), IU_LONG("IULong"), I_LONG_LONG("ILongLong"), 
      IU_LONG_LONG("IULongLong"), I128("I128"), IU128("IU128");

      override fun toString(): String = identifier

      class Deserializer : EnumDeserializer<Kind>(Kind.entries, Kind::identifier)
    }

    override fun toString(): String = kind.identifier
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Ptr(
    @JsonProperty("kind")
    val kind: Kind,
    @JsonProperty("type")
    val type: Type,
    @JsonProperty("type_kind")
    typeKind: String = "Tptr"
  ) : Type(typeKind) {
    enum class Kind(val identifier: String) {
      PK_POINTER("Pk_pointer"), PK_LVALUE_REFERENCE("Pk_lvalue_reference"), 
      PK_RVALUE_REFERENCE("Pk_rvalue_reference"), PK_OBJC_WEAK("Pk_objc_weak"), 
      PK_OBJC_UNSAFE_UNRETAINED("Pk_objc_unsafe_unretained"), 
      PK_OBJC_AUTORELEASING("Pk_objc_autoreleasing");

      override fun toString(): String = identifier

      class Deserializer : EnumDeserializer<Kind>(Kind.entries, Kind::identifier)
    }

    override fun toString(): String = "ptr<$type>"
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Struct(
    @JsonProperty("struct_name")
    val structName: String,
    @JsonProperty("type_kind")
    typeKind: String = "Tstruct"
  ) : Type(typeKind) {
    override fun toString(): String = structName
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Var(
    @JsonProperty("type_name")
    val typeName: TypeName,
    @JsonProperty("type_kind")
    typeKind: String = "Tvar"
  ) : Type(typeKind) {
    override fun toString(): String = TODO()
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Void(
    @JsonProperty("type_kind")
    typeKind: String = "Tvoid"
  ) : Type(typeKind) {
    override fun toString(): String = "void"
  }

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class Fun(
    @JsonProperty("type_kind")
    typeKind: String = "Tfun"
  ) : Type(typeKind) {
    override fun toString(): String = "fun"
  }

  class Deserializer(vc: Class<*>? = null): MapDeserializer<Type>(vc, "type_kind", {
    when (val typeKind = it) {
      "Tarray" -> Array::class.java
      "Tfloat" -> Float::class.java
      "Tint" -> Int::class.java
      "Tptr" -> Ptr::class.java
      "Tstruct" -> Struct::class.java
      "Tvar" -> Var::class.java
      "Tvoid" -> Void::class.java
      "Tfun" -> Fun::class.java
      else -> throw IllegalArgumentException("Invalid type kind: $typeKind")
    }
  })
}