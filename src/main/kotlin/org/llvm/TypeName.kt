package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.llvm.util.MapDeserializer

@JsonDeserialize(using = TypeName.Deserializer::class)
sealed class TypeName {
  @JsonDeserialize(using = JsonDeserializer.None::class)
  class CsuTypeName(
    @JsonProperty("csu_kind")
    val csuKind: String,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("type_name_kind")
    val typeNameKind: String = "CsuTypeName"
  ) : TypeName()

  @JsonDeserialize(using = JsonDeserializer.None::class)
  class TNTypeDef(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("type_name_kind")
    val typeNameKind: String = "TN_typedef"
  ) : TypeName()

  class Deserializer(vc: Class<*>? = null) : MapDeserializer<TypeName>(vc, "type_name_kind", {
    when (val value = it) {
      "CsuTypeName" -> CsuTypeName::class.java
      "TN_typedef" -> TNTypeDef::class.java
      else -> throw JsonParseException("Unknown type kind name: $value")
    }
  })
}