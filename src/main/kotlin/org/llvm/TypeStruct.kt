package org.llvm

import com.fasterxml.jackson.annotation.JsonProperty

class TypeStruct(
  @JsonProperty("instance_fields")
  val instanceFields: List<Field.InstanceField>,
  @JsonProperty("static_fields")
  val staticFields: List<Field.StaticField>,
  @JsonProperty("supers")
  val supers: List<SuperTypeKind>,
  @JsonProperty("methods")
  val methods: List<Method>,
  @JsonProperty("annotations")
  val annotations: List<Annotation>
)