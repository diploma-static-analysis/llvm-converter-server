package org.llvm.util

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import kotlin.enums.EnumEntries

open class EnumDeserializer<T : Enum<T>>(private val enumEntries: EnumEntries<T>, private val identifier: (T) -> String, vc: Class<*>? = null): StdDeserializer<T>(vc) {
  override fun deserialize(jsonParser: JsonParser, p1: DeserializationContext?): T {
    val text = jsonParser.codec.readTree<JsonNode>(jsonParser).asText()
    return enumEntries.first { identifier(it) == text }
  }
}