package org.llvm.util

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

open class MapDeserializer<T>(vc: Class<*>? = null, private val control: String, private val transform: (String) -> Class<out T>) : StdDeserializer<T>(vc) {
  override fun deserialize(jsonParser: JsonParser, deserializationContext: DeserializationContext): T {
    val node = jsonParser.codec.readTree<JsonNode>(jsonParser)
    val value = node.get(control).asText()
    return jsonParser.codec.treeToValue(node, transform(value))
  }
}