package ru.kuznetsov

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.grpc.ManagedChannelBuilder
import org.llvm.Configuration
import proto.LLVMParseServiceGrpcKt
import proto.lLVMRequest
import ru.kuznetsov.translator.FileData
import ru.kuznetsov.translator.Module
import kotlin.io.path.*

object Client {
  suspend fun sendProtobuf(inputPath: String, address: String) {
    val skipped = 0
    val skippedFromEnd = 0
    
    val listFiles = if (Path(inputPath).isDirectory()) {
      Path(inputPath).listDirectoryEntries().filter { !it.isDirectory() }.drop(skipped).dropLast(skippedFromEnd)
    } else {
      listOf(Path(inputPath))
    }
    val channel = ManagedChannelBuilder.forAddress(address, 50051).usePlaintext().build()
    val service = LLVMParseServiceGrpcKt.LLVMParseServiceCoroutineStub(channel)
    
    val fileDatas = listFiles.map { filePath ->
      val llvm = filePath.readText()
      val request = lLVMRequest { this.file = llvm }
      val response = service.parse(request)
      
      FileData(filePath.toAbsolutePath().toString(), llvm, response.module)
    }
    
    channel.shutdown()

    val state = Module.translateModule(fileDatas)
    
    val objectMapper = ObjectMapper()
      .setDefaultPrettyPrinter(DefaultPrettyPrinter())
      .registerKotlinModule()
    val writer = objectMapper.writerWithDefaultPrettyPrinter().with(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
    
    writer.writeValue(Path("output-tenv-example.json").outputStream(), state.classDescriptions.map { it.value.toClassDescription() })
    writer.writeValue(Path("output-cfg-example.json").outputStream(), Configuration(state.procs, state.nodes))
  }
}