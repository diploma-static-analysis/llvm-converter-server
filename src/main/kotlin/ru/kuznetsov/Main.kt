package ru.kuznetsov

suspend fun main(args: Array<String>) {
  if (args.size != 2) {
    println("Parameters: <path to llvm file> <address>")
  }
  Client.sendProtobuf(args[0], args[1])
}