package ru.kuznetsov.translator

import proto.value.ValueOuterClass

data class FileData(val fileName: String, val fileText: String, val module: ValueOuterClass.Module)