package ru.kuznetsov.translator

import org.llvm.*
import proto.location.location
import proto.type.copy
import proto.value.*
import proto.value.CallInstKt.callInstClassic
import ru.kuznetsov.translator.graph.Graph
import ru.kuznetsov.translator.graph.GraphTranslator.translate
import ru.kuznetsov.translator.type.StructType.addStruct
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier
import ru.kuznetsov.translator.value.argument.Argument.translateArgumentsAsFormals
import ru.kuznetsov.translator.value.argument.Argument.translateArgumentsAsStrings
import ru.kuznetsov.translator.value.user.instruction.callBase.CallInst.getNameOfFunctionOrFake

object Module {
  
  private fun getNodes(newState: TranslatorState, graph: Graph): List<Node> {
    graph.translator(newState)
    return newState.translate(graph)
  }
  
  private fun TranslatorState.addMethodWithBody(
    functionIndex: Int,
    function: ValueOuterClass.Function,
    method: Method,
    basicBlocks: List<ValueOuterClass.BasicBlock>
  ) {
    val newState = this
      .withCurrentMethod(method)
      .withFunctionIndex(functionIndex)

    val graph = Graph(nodes.size, newState, method, function.parameterList, basicBlocks)
    
    val locals = graph.nodes()
      .filterIsInstance<Graph.StatementNode>()
      .flatMap { it.instructions }
      .mapNotNull { instruction ->
        if (instruction.hasUnaryInstruction()) {
          val unaryInstruction = instruction.unaryInstruction
          if (unaryInstruction.hasAllocaInst()) {
            val allocaInst = unaryInstruction.allocaInst
            Attributes.Local(
              name = newState.translateValueAsIdentifier(allocaInst.to).name,
              type = translateType(allocaInst.to.type),
              modifyInBlock = false,
              isConstExpr = false
            )
          } else {
            null
          }
        } else {
          null
        }
      }

    // procs part
    val functionAttributes = Attributes(
      access = Attributes.Access.PUBLIC, // todo
      captured = emptyList(), // todo
      formals = newState.translateArgumentsAsFormals(true, function.parameterList),
      methodAnnotation = Attributes.MethodAnnotation(emptyList(), Annotations(emptyList())),
      locals = locals,
      location = Location(0, 0, Location.SourceFile(fileName)), // TODO
      procName = method,
      exceptions = emptyList(), // todo
      isAbstract = false,
      isBridgeMethod = false,
      isDefined = true,
      isSyntheticMethod = false,
      returnType = translateType(function.returnType)
    )

    val newNodes = getNodes(newState, graph)

    nodes.addAll(newNodes)

    val startNode = newNodes.single { it is Node.StartNode }
    val finishNode = newNodes.single { it is Node.ExitNode }

    val proc = Proc(
      startNode.id, finishNode.id, functionIndex, functionAttributes
    )

    procs[function.name] = proc
  }
  
  private fun TranslatorState.addFakeMethod(
    functionIndex: Int,
    function: ValueOuterClass.Function,
  ) {
    val callResultName = "%1"
    val fakeFunctionName = getNameOfFunctionOrFake(function)
    val returnType = function.returnType.copy {  }
    val method = Method.RegularMethod(
      methodName = fakeFunctionName,
      parameters = translateArgumentsAsStrings(true, function.parameterList),
      className = "", // TODO
      returnType = translateType(function.returnType).toString(),
      isStatic = true
    )
    val temporaryValue = value { 
      type = returnType
      user = user { 
        derivedUser = derivedUser { 
          name = callResultName
        }
      }
    }
    val nonNullInstructions = function.parameterList.filter { it.nonNull }.map { parameter ->
      check(parameter.hasType()) { "Parameter should have type" }
      check(parameter.type.hasPointerType()) { "Parameter should have pointer type" }
      check(parameter.type.pointerType.subtypeList.size == 1) { "Parameter should be pointer with size 1" }
      val subType = parameter.type.pointerType.subtypeList[0]
      instruction { 
        unaryInstruction = unaryInstruction { 
          loadInst = loadInst {
            from = value {
              type = parameter.type
              user = user {
                derivedUser = derivedUser {
                  name = parameter.name
                }
              }
            }
            to = value {
              type = subType
              user = user { 
                derivedUser = derivedUser {
                  name = parameter.name + "_fake_name"
                }
              }
            }
          }
        }
        location = location {
          this.row = 0
          this.column = 0
          this.filename = this@addFakeMethod.fileName
        }
        type = subType
      }
    }
    val callInstruction = instruction { 
      callBase = callBase { 
        callInst = callInst {
          this.callInstClassic = callInstClassic {
            this.function = function {
              name = function.name
              this.returnType = returnType
            }
            result = temporaryValue
          }
        }
      }
      location = location { 
        this.row = 0
        this.column = 0
        this.filename = this@addFakeMethod.fileName
      }
      type = returnType
    }
    val returnInstruction = instruction {
      returnInst = returnInst {
        this.return_ = temporaryValue
      }
      location = location {
        this.row = 0
        this.column = 0
        this.filename = this@addFakeMethod.fileName
      }
      type = returnType
    }
    val basicBlocks: List<ValueOuterClass.BasicBlock> = listOf(
      basicBlock { 
        instructions.addAll(nonNullInstructions)
        instructions.add(callInstruction)
        instructions.add(returnInstruction)
        
        terminator = returnInstruction
      }
    )
    
    val newState = this
      .withCurrentMethod(method)
      .withFunctionIndex(functionIndex)

    val graph = Graph(nodes.size, newState, method, function.parameterList, basicBlocks)
    
    val attributes = Attributes(
      access = Attributes.Access.PUBLIC, // todo
      captured = emptyList(), // todo
      formals = newState.translateArgumentsAsFormals(true, function.parameterList),
      methodAnnotation = Attributes.MethodAnnotation(emptyList(), Annotations(emptyList())),
      locals = emptyList(),
      location = Location(0, 0, Location.SourceFile(fileName)), // TODO,
      procName = method,
      exceptions = emptyList(), // todo
      isAbstract = false,
      isBridgeMethod = false,
      isDefined = true,
      isSyntheticMethod = false,
      returnType = translateType(function.returnType)
    )
    
    val nodes = getNodes(newState, graph)

    val startNode = nodes.single { it is Node.StartNode }
    val finishNode = nodes.single { it is Node.ExitNode }

    this.nodes.addAll(nodes)
    
    val proc = Proc(
      startNode.id, finishNode.id, functionIndex, attributes
    )

    procs[fakeFunctionName] = proc
    classDescriptions[""]!!.typeStruct.methods.add(method)
  }
  
  private fun TranslatorState.addMethod(
    function: ValueOuterClass.Function
  ) {
    val name = function.name
    val basicBlocks = function.basicBlocksList

    val method = Method.RegularMethod(
      methodName = name,
      parameters = translateArgumentsAsStrings(true, function.parameterList),
      className = "", // TODO
      returnType = translateType(function.returnType).toString(),
      isStatic = true
    )
    if (function.parameterList.any { it.nonNull }) {
      addFakeMethod(procs.size, function)
    }
    
    if (basicBlocks.isNotEmpty()) {
      addMethodWithBody(procs.size, function, method, basicBlocks)
    }
    
    classDescriptions[""]!!.typeStruct.methods.add(method)
  }
  
  fun translateModule(fileDatas: List<FileData>): TranslatorState {
    check(fileDatas.isNotEmpty()) { "fileDatas must not be empty" }
    var translatorState = TranslatorState(fileDatas[0].fileName, fileDatas[0].fileText)
    
    fileDatas.forEachIndexed { index, fileData ->
      println("[${(index + 1).toString().padStart(fileDatas.size.toString().length, ' ')}/${fileDatas.size}] File ${fileData.fileName}")
      translatorState = translatorState.withFileNameAndText(fileData.fileName, fileData.fileText)
      val functions = fileData.module.functionsList

      translatorState.classDescriptions[""] = ClassDescription.ClassDescriptionContainer(
        typeName = TypeName.CsuTypeName(csuKind = "Class", name = ""),
        typeStruct = ClassDescription.ClassDescriptionContainer.TypeStructContainer()
      )

      fileData.module.structList.forEach { type ->
        check(type.hasStructType()) { "Type is not structure type, but it is in struct list: $type" }
        val structType = type.structType
        translatorState.addStruct(structType)
      }

      try {
        functions.forEach { function ->
          translatorState.addMethod(function)
        }
      } catch (e: Exception) {
        e.printStackTrace()
        println()
        println()
        println()
      }
    }

    return translatorState
  }
}