package ru.kuznetsov.translator

import org.llvm.*
import proto.type.TypeOuterClass
import proto.value.ValueOuterClass.BasicBlock
import ru.kuznetsov.translator.location.LocationContainer
import ru.kuznetsov.translator.type.Type.translateType

data class TranslatorState(
  // prestate
  var fileName: String,
  var fileText: String,
  
  // Global state
  val classDescriptions: ClassDescriptionsContainer = mutableMapOf(),
  val procs: MutableMap<String, Proc> = mutableMapOf(),
  val nodes: MutableList<Node> = mutableListOf(),

  // Minor state
  val functionIndex: Int = -1,
  val currentMethod: Method? = null,
  val currentBranch: Boolean? = null,
  val currentType: Type? = null,
  val currentUnsafeType: TypeOuterClass.Type? = null,
  val currentLocation: LocationContainer? = null,
  
  val context: Map<String, Any> = mapOf()
) {
  fun withFileNameAndText(fileName: String, fileText: String) = copy(fileName = fileName, fileText = fileText)
  
  fun withFunctionIndex(functionIndex: Int): TranslatorState {
    return this.copy(functionIndex = functionIndex)
  }
  
  fun withCurrentMethod(currentMethod: Method): TranslatorState {
    return this.copy(currentMethod = currentMethod)
  }
  
  fun withCurrentUnsafeType(currentUnsafeType: TypeOuterClass.Type): TranslatorState {
    return this.copy(currentUnsafeType = currentUnsafeType, currentType = translateType(currentUnsafeType))
  }
  
  fun withCurrentBranch(currentBranch: Boolean): TranslatorState {
    return this.copy(currentBranch = currentBranch)
  }
  
  fun withCurrentLocation(location: LocationContainer): TranslatorState = copy(currentLocation = location)
  
  fun <T : Any> withContext(key: Key<T>, value: T): TranslatorState = copy(context = context + (key.name to value))
  
  @Suppress("UNCHECKED_CAST")
  fun <T : Any> getContext(key: Key<T>): T = context[key.name] as T
  @Suppress("UNCHECKED_CAST")
  fun <T : Any> getContextNullable(key: Key<T>): T? = context[key.name] as T?

  data class Key<T>(val name: String)
  
  companion object {
    val CURRENT_BLOCK = Key<BasicBlock>("BASIC_BLOCK")
  }
}