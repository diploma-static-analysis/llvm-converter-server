package ru.kuznetsov.translator.graph

import proto.value.ValueOuterClass
import proto.value.ValueOuterClass.Instruction
import ru.kuznetsov.translator.TranslatorState

abstract class AbstractGraphChanger(val graphTranslatorContainer: GraphTranslatorContainer) {
  fun Graph.StatementNode.terminator(): ValueOuterClass.Instruction? {
    return this.instructions.lastOrNull()
  }
  
  fun translate(translatorState: TranslatorState, graph: Graph): Boolean {
    return graph.nodes().any { translateNode(translatorState, graph, it) }
  }
  
  abstract fun translateNode(translatorState: TranslatorState, graph: Graph, node: Graph.Node): Boolean

  abstract fun isInstructionAffected(instruction: Instruction): Boolean
}