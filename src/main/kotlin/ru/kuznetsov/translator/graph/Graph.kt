package ru.kuznetsov.translator.graph

import org.llvm.*
import proto.value.ValueOuterClass.*
import proto.value.ValueOuterClass.Instruction
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

class Graph(startId: Int, translatorState: TranslatorState, method: Method, parameters: List<Argument>, basicBlocks: List<BasicBlock>) {
  
  private val fileName: String = translatorState.fileName
  
  private val namesToBlocks = basicBlocks.associateBy { it.name }
  private val namesToNodes = mutableMapOf<String, Node>()
  private val idsToNodes = mutableMapOf<Int, Node>()
  private var nodeIt = startId
  private val startNode: StartNode
  private val finishNode: FinishNode
  
  fun addNode(node: StatementNode) {
    check(!namesToNodes.containsKey(node.name))
    check(!idsToNodes.containsKey(node.id))
    namesToNodes[node.name] = node
    idsToNodes[node.id] = node
  }
  
  fun addNode(node: ExceptionNode) {
    check(!namesToNodes.containsKey(node.name))
    check(!idsToNodes.containsKey(node.id))
    namesToNodes[node.name] = node
    idsToNodes[node.id] = node
  }
  
  fun addEdge(from: Node, to: Node): Boolean {
    if (from.nextNodes.contains(to.id) || to.previousNodes.contains(from.id)) return false
    return from.nextNodes.add(to.id) && to.previousNodes.add(from.id)
  }
  
  fun addEdge(from: Int, to: Int): Boolean {
    val fromNode = idsToNodes[from]!!
    val toNode = idsToNodes[to]!!
    return addEdge(fromNode, toNode)
  }

  fun addEdgeToEndNode(node: Node): Boolean {
    return addEdge(node.id, finishNode.id)
  }
  
  fun getOrCreateStatementNode(name: String): StatementNode {
    val node = namesToNodes[name] as StatementNode?
    if (node != null) return node

    val block = namesToBlocks[name]!!
    return createNewStatementNode(block.name, block, mutableListOf(*block.instructionsList.toTypedArray()))
  }
  
  fun createNewStatementNode(name: String, basicBlock: BasicBlock, instructions: MutableList<Instruction>): StatementNode {
    val statementNode = StatementNode(
      nodeIt,
      name,
      basicBlock,
      instructions.toMutableList(),
      null
    )
    nodeIt++
    addNode(statementNode)
    return statementNode
  }
  
  fun getOrCreateExceptionNode(block: BasicBlock): ExceptionNode {
    val exceptionNode = ExceptionNode(
      id = nodeIt,
      name = "${block.name}-exception",
      basicBlock = block,
      instructions = mutableListOf(block.instructionsList.last()),
    )
    nodeIt++
    addNode(exceptionNode)
    return exceptionNode  
  }
  
  fun createBranchNode(
    block: BasicBlock,
    ifTrueBranch: Boolean,
    nameOfContinuation: String,
    value: String,
    condition: Instruction
  ): PruneNode {
    val pruneNode = PruneNode(
      nodeIt,
      value = value,
      ifTrueBranch = ifTrueBranch,
      basicBlock = block,
      condition = condition
    )
    nodeIt++

    idsToNodes[pruneNode.id] = pruneNode
    val nextNode = getOrCreateStatementNode(nameOfContinuation)
    addEdge(pruneNode, nextNode)
    return pruneNode
  }
  
  fun translator(translatorState: TranslatorState) {
    val graphTranslatorContainer = GraphTranslatorContainer()
    val translators = mutableListOf(
      GraphReturnTranslator(graphTranslatorContainer),
      GraphBranchTranslator(graphTranslatorContainer),
      GraphResumeTranslator(graphTranslatorContainer),
      GraphUnreachableTranslator(graphTranslatorContainer),
      GraphInvokeTranslator(graphTranslatorContainer),
      GraphSwitchTranslator(graphTranslatorContainer),
      GraphPhiNodeTranslator(graphTranslatorContainer)
    )
    graphTranslatorContainer.translators.addAll(translators)
    namesToBlocks.values.forEach { blocks ->
      val terminator = blocks.terminator
      
      check(graphTranslatorContainer.isInstructionAffected(terminator)) {
        "Last instruction in ${blocks.name} is not affected: $terminator"
      }
    }
    var isChanged = true
    while (isChanged) {
      isChanged = false
      translators.forEach {
        isChanged = isChanged || it.translate(translatorState, this)
      }
    }
    backPropagation()
  }
  
  init {
    // copy parameters from variables to identifiers
    val startInstructions = parameters.map { param ->
      val normalIdentifier = Identifier(
        kind = Identifier.Kind.NORMAL,
        name = param.name,
        stamp = 0
      )
      org.llvm.Instruction.Load(
        location = Location(0, 0, Location.SourceFile(fileName)), // TODO
        identifier = normalIdentifier,
        expression = Expression.LVariableExpression(
          pvar = PVariable.LocalVariable(
            name = param.name,
            procName = method
          )
        ),
        type = translatorState.translateType(param.type)
      )
    }
    
    startNode = StartNode(nodeIt, startInstructions, null)
    nodeIt++
    finishNode = FinishNode(nodeIt)
    nodeIt++
    idsToNodes[startNode.id] = startNode
    idsToNodes[finishNode.id] = finishNode
    if (basicBlocks.isEmpty()) {
      addEdge(startNode.id, finishNode.id)
    } else {
      val firstNode = getOrCreateStatementNode(basicBlocks[0].name)
      addEdge(startNode.id, firstNode.id)
    }
  }
  
  private fun backPropagation() {
    finishNode.exitNode = finishNode.id
    var isChanged = true
    while (isChanged) {
      isChanged = false
      for (node in nodes()) {
        for (nextNodeId in node.nextNodes) {
          val nextNode = idsToNodes[nextNodeId]!!
          if (nextNode.exitNode != null && node.exitNode == null) {
            isChanged = true
            node.exitNode = nextNode.exitNode
          }
        }
      }
    }
    finishNode.exitNode = null
  }
  
  fun nodes(): List<Node> = idsToNodes.values.toList()

  sealed class Node(
    val id: Int,
    var exitNode: Int? = null,
    val previousNodes: MutableList<Int> = mutableListOf(),
    val nextNodes: MutableList<Int> = mutableListOf()
  )
  
  class StartNode(
    id: Int,
    val instructions: List<org.llvm.Instruction>,
    exitNode: Int? = null,
    nextNodes: MutableList<Int> = mutableListOf(),
  ): Node(id, exitNode, mutableListOf(), nextNodes)
  
  class FinishNode(
    id: Int,
    previousNodes: MutableList<Int> = mutableListOf()
  ): Node(id, null, previousNodes, mutableListOf())
  
  class PruneNode(
    id: Int,
    exitNode: Int? = null,
    val basicBlock: BasicBlock,
    previousNodes: MutableList<Int> = mutableListOf(),
    nextNodes: MutableList<Int> = mutableListOf(),
    val value: String,
    val ifTrueBranch: Boolean,
    val condition: Instruction
  ): Node(id, exitNode, previousNodes, nextNodes)
  
  open class StatementNode(
    id: Int,
    val name: String,
    val basicBlock: BasicBlock,
    val instructions: MutableList<Instruction>,
    exitNode: Int? = null,
    previousNodes: MutableList<Int> = mutableListOf(),
    nextNodes: MutableList<Int> = mutableListOf()
  ) : Node(id, exitNode, previousNodes, nextNodes)
  
  class ExceptionNode(
    id: Int, 
    name: String, 
    basicBlock: BasicBlock,
    instructions: MutableList<Instruction>, 
    exitNode: Int? = null, 
    previousNodes: MutableList<Int> = mutableListOf(),
    nextNodes: MutableList<Int> = mutableListOf()
  ) : StatementNode(id, name, basicBlock, instructions, exitNode, previousNodes, nextNodes)
}