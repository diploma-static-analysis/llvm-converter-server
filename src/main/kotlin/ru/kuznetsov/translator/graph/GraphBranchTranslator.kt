package ru.kuznetsov.translator.graph

import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState

class GraphBranchTranslator(graphTranslatorContainer: GraphTranslatorContainer) : AbstractGraphChanger(graphTranslatorContainer) {
  override fun translateNode(translatorState: TranslatorState, graph: Graph, node: Graph.Node): Boolean {
    if (node is Graph.StatementNode) {
      val terminator = node.terminator() ?: return false
      if (terminator.hasBranchInst()) {
        val branchInst = terminator.branchInst

        if (branchInst.hasConditionalBranchInst()) {
          val conditionInst = branchInst.conditionalBranchInst

          val trueNode = graph.createBranchNode(node.basicBlock, true, conditionInst.basicBlockTrueBranch, conditionInst.condition.name, terminator)
          val falseNode = graph.createBranchNode(node.basicBlock, false, conditionInst.basicBlockFalseBranch, conditionInst.condition.name, terminator)
          graph.addEdge(node, trueNode)
          graph.addEdge(node, falseNode)
        } else if (branchInst.hasUnconditionalBranchInst()) {
          val nextNode = graph.getOrCreateStatementNode(branchInst.unconditionalBranchInst.basicBlockName)
          graph.addEdge(node, nextNode)
        } else {
          throw IllegalStateException("Branch must be or conditional or unconditional")
        }
        node.instructions.removeLast()
        return true
      }
    }
    return false
  }

  override fun isInstructionAffected(instruction: ValueOuterClass.Instruction): Boolean {
    return instruction.hasBranchInst()
  }
}