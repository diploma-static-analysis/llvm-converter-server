package ru.kuznetsov.translator.graph

import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState

class GraphInvokeTranslator(graphTranslatorContainer: GraphTranslatorContainer) : AbstractGraphChanger(graphTranslatorContainer) {
  override fun translateNode(translatorState: TranslatorState, graph: Graph, node: Graph.Node): Boolean {
    if (node is Graph.StatementNode && node !is Graph.ExceptionNode) {
      val terminator = node.terminator() ?: return false
      if (terminator.hasCallBase() && terminator.callBase.hasInvokeInst()) {
        val callBase = terminator.callBase
        val invokeInst = callBase.invokeInst
        val block = node.basicBlock
        
        val exceptionNode = graph.getOrCreateExceptionNode(block)

        graph.addEdge(node, exceptionNode)

        val trueNode = graph.createBranchNode(block, true, invokeInst.normalBranch, "exception${block.name}", terminator)
        val falseNode = graph.createBranchNode(block, false, invokeInst.unwindBranch, "exception${block.name}", terminator)
        graph.addEdge(exceptionNode, trueNode)
        graph.addEdge(exceptionNode, falseNode)
        node.instructions.removeLast()
        return true
      }
    }
    return false
  }

  override fun isInstructionAffected(instruction: ValueOuterClass.Instruction): Boolean {
    return instruction.hasCallBase() && instruction.callBase.hasInvokeInst()
  }


}