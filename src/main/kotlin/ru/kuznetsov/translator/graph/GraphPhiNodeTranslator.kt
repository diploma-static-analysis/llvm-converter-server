package ru.kuznetsov.translator.graph

import proto.location.LocationOuterClass
import proto.type.TypeKt.pointerType
import proto.type.type
import proto.value.*
import proto.value.ValueOuterClass.Instruction
import proto.value.ValueOuterClass.Instruction.PHINode
import proto.value.ValueOuterClass.Instruction.PHINode.IncomingValue
import ru.kuznetsov.translator.TranslatorState

class GraphPhiNodeTranslator(graphTranslatorContainer: GraphTranslatorContainer) : AbstractGraphChanger(graphTranslatorContainer) {
  
  private fun createStorePlusLoad(
    phiNode: PHINode,
    instructionText: LocationOuterClass.InstructionText,
    currentIncomingValue: IncomingValue
  ): List<Instruction> {
    val type = phiNode.type
    val resultValue = phiNode.result
    val temporaryValue = resultValue.copy { 
      if (hasName()) {
        name += "_phi_value"
      }
      if (hasUser() && user.hasDerivedUser() && user.derivedUser.hasName()) {
        user = user.copy { 
          derivedUser = derivedUser.copy {
            name += "_phi_value"
          }
        }
      }
    }
    return listOf(
      instruction {
        storeInst = storeInst {
          from = currentIncomingValue.value
          to = temporaryValue
        }
        this.type = type { 
          pointerType = pointerType {
            subtype.add(type)
          }
        }
        
        this.instructionText = instructionText
      },
      instruction {
        unaryInstruction = unaryInstruction {
          loadInst = loadInst {
            from = temporaryValue
            to = resultValue
          }
        }
        this.type = type
        this.instructionText = instructionText
      }
    )
  }

  override fun translateNode(translatorState: TranslatorState, graph: Graph, node: Graph.Node): Boolean {
    var isChanged = false
    if (node is Graph.StatementNode) {
      while (node.instructions.firstOrNull()?.hasPHINode() == true) {
        isChanged = true
        val instruction = node.instructions.first()
        val phiNode = instruction.phiNode
        for (incomingValue in phiNode.incomingValueList) {
          val instructions = createStorePlusLoad(phiNode, instruction.instructionText, incomingValue)
          val previousNode = graph.getOrCreateStatementNode(incomingValue.name)
          if (previousNode.instructions.isNotEmpty()) {
            if (!graphTranslatorContainer.isInstructionAffected(previousNode.instructions.last())) {
              previousNode.instructions.addAll(instructions)
            } else {
              var id = previousNode.instructions.size - 1
              while (id > 0 && graphTranslatorContainer.isInstructionAffected(previousNode.instructions[id])) {
                id--
              }
              previousNode.instructions.addAll(id, instructions)
            }
          } else {
            previousNode.instructions.addAll(instructions)
          }
        }
        node.instructions.removeAt(0)
      }
      check(node.instructions.all { !it.hasPHINode() }) { "Node still have PHINode after clean PHINodes" }
    }
    return isChanged
  }

  override fun isInstructionAffected(instruction: Instruction): Boolean {
    return false
  }
}