package ru.kuznetsov.translator.graph

import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState

class GraphResumeTranslator(graphTranslatorContainer: GraphTranslatorContainer) : AbstractGraphChanger(graphTranslatorContainer) {
  override fun translateNode(translatorState: TranslatorState, graph: Graph, node: Graph.Node): Boolean {
    if (node is Graph.StatementNode) {
      val terminator = node.terminator() ?: return false
      if (terminator.hasResumeInst()) {
        return graph.addEdgeToEndNode(node)
      }
    }
    return false
  }

  override fun isInstructionAffected(instruction: ValueOuterClass.Instruction): Boolean {
    return instruction.hasResumeInst()
  }
}