package ru.kuznetsov.translator.graph

import proto.location.copy
import proto.type.TypeKt.integerType
import proto.type.TypeKt.pointerType
import proto.type.TypeKt.voidType
import proto.type.copy
import proto.type.type
import proto.value.*
import proto.value.InstructionKt.BranchInstKt.conditionalBranchInst
import proto.value.InstructionKt.branchInst
import proto.value.InstructionKt.cmpInst
import proto.value.ValueOuterClass.ConstantInt
import ru.kuznetsov.translator.TranslatorState

class GraphSwitchTranslator(graphTranslatorContainer: GraphTranslatorContainer) : AbstractGraphChanger(graphTranslatorContainer) {
  private fun createConstantVariable(instructionText: String, compareInt: ConstantInt, toName: String): List<ValueOuterClass.Instruction> {
    val type = type {
      integerType = integerType { 
        size = 32
      }
    }
    val fakeType = type {
      pointerType = pointerType {
        this.subtype.add(type)
      }
    }
    val locationInstructionText = proto.location.instructionText {
      inst = instructionText
    }
    val result = value {
      this.user = user {
        this.derivedUser = derivedUser { 
          this.name = toName
        }
      }
      this.type = type
    }
    val fakeResult = value {
      this.user = user {
        this.derivedUser = derivedUser {
          this.name = toName + "_fake"
        }
      }
      this.type = fakeType
    }
    return listOf(instruction { 
      storeInst = storeInst { 
        from = value { 
          this.user = user {
            this.constant = constant {
              this.constantData = constantData { 
                this.constantInt = compareInt
              }
            }
          }
          this.type = fakeType
        }
        to = fakeResult
      }
      this.type = fakeType
      this.instructionText = locationInstructionText
    }, instruction { 
      unaryInstruction = unaryInstruction { 
        loadInst = loadInst {
          from = fakeResult
          to = result
        }
      }
      this.type = type
      this.instructionText = locationInstructionText
    })
  }
  
  private fun createVariable(instructionText: String, fromValue: ValueOuterClass.Value, toName: String, compareIntName: String): ValueOuterClass.Instruction {
    val booleanType = type { 
      integerType = integerType { 
        size = 1
      }
    }
    val locationInstructionText = proto.location.instructionText { 
      inst = instructionText
    }
    val result = value {
      this.type = booleanType.copy {  }
      this.instructionText = locationInstructionText.copy {  }
      this.user = user {
        this.derivedUser = derivedUser {
          this.name = toName
        }
      }
    }
    val constantInt = value {
      this.user = user { 
        this.derivedUser = derivedUser {
          this.name = compareIntName
        }
      }
      this.type = type {
        this.integerType = integerType {
          this.size = 32
        }
      }
    }
    return instruction {
      cmpInst = cmpInst {
        isIntPredicate = true
        this.result = result
        this.left = fromValue
        this.type = ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_EQ
        this.right = constantInt
      }
      this.type = booleanType.copy {  }
      this.instructionText = locationInstructionText.copy {  }
    }
  }
  
  private fun createBranch(conditionalName: String, trueName: String, falseName: String, instructionText: String): ValueOuterClass.Instruction {
    val locationInstructionText = proto.location.instructionText {
      this.inst = instructionText
    }
    val booleanType = type {
      integerType = integerType {
        size = 1
      }
    }
    return instruction {
      this.branchInst = branchInst {
        this.conditionalBranchInst = conditionalBranchInst {
          this.condition = value {
            this.type = booleanType.copy {  }
            this.instructionText = locationInstructionText
            this.user = user {
              this.derivedUser = derivedUser {
                this.name = conditionalName
              }
            }
          }
          this.basicBlockTrueBranch = trueName
          this.basicBlockFalseBranch = falseName
        }
      }
      this.type = type {
        this.voidType = voidType { }
      }
      this.instructionText = locationInstructionText
    }
  }
  
  override fun translateNode(translatorState: TranslatorState, graph: Graph, node: Graph.Node): Boolean {
    if (node is Graph.StatementNode) {
      val terminator = node.terminator() ?: return false
      if (terminator.hasSwitchInst()) {
        val switchInst = terminator.switchInst
        val firstCase = switchInst.branchList[0]
        if (switchInst.branchList.size > 1) {
          val newSwitchInst = switchInst.copy { 
            this.branch.clear()
            this.branch.addAll(switchInst.branchList.drop(1))
          }
          val newInstructionWithSwitch = terminator.copy { 
            this.switchInst = newSwitchInst
          }
          val newNodeName = node.name.substringBefore("-") + "-case-[${firstCase.condition.value}]"
          val constantName = newNodeName + "_constant"
          val newVariableName = newNodeName + "_${firstCase.condition.value}"
          
          val instructions = listOf(
            *createConstantVariable(terminator.instructionText.inst, firstCase.condition, constantName).toTypedArray(),
            createVariable(terminator.instructionText.inst, switchInst.switchValue, newVariableName, constantName),
            createBranch(newVariableName, firstCase.label, newNodeName, terminator.instructionText.inst)
          )
          
          val newNode = graph.createNewStatementNode(
            newNodeName,
            basicBlock {
              this.name = newNodeName
              this.instructions.add(newInstructionWithSwitch)
              this.terminator = newInstructionWithSwitch
            },
            mutableListOf(newInstructionWithSwitch)
          )
          graph.addEdge(node, newNode)
          node.instructions.removeLast()
          node.instructions.addAll(instructions)
          return true
        } else {
          val newNodeName = node.name.substringBefore("-") + "-case-[${firstCase.condition.value}]"
          val constantName = newNodeName + "_constant"
          val newVariableName = newNodeName + "_${firstCase.condition.value}"

          val instructions = listOf(
            *createConstantVariable(terminator.instructionText.inst, firstCase.condition, constantName).toTypedArray(),
            createVariable(terminator.instructionText.inst, switchInst.switchValue, newVariableName, constantName),
            createBranch(newVariableName, firstCase.label, switchInst.labelDefault, terminator.instructionText.inst)
          )
          val defaultBranchNode = graph.getOrCreateStatementNode(switchInst.labelDefault)
          graph.addEdge(node, defaultBranchNode)
          node.instructions.removeLast()
          node.instructions.addAll(instructions)
          return true
        }
      }
    }
    return false
  }

  override fun isInstructionAffected(instruction: ValueOuterClass.Instruction): Boolean {
    return instruction.hasSwitchInst()
  }
}