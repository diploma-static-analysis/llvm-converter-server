package ru.kuznetsov.translator.graph

import org.llvm.IfKind
import org.llvm.Instruction
import org.llvm.Location
import org.llvm.Node
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.instruction.Instruction.translateInstructionAsInstruction
import ru.kuznetsov.translator.value.user.instruction.Instruction.translateInstructionAsStatementKind

object GraphTranslator {
  fun TranslatorState.translate(graph: Graph): List<Node> {
    val graphNodes = graph.nodes()
    return graphNodes.map { graphNode ->
      when (graphNode) {
        is Graph.StartNode -> Node.StartNode(
          instructions = graphNode.instructions,
          procId = functionIndex,
          id = graphNode.id,
          exitNodes = listOf(graphNode.exitNode!!),
          location = Location(0, 0, Location.SourceFile(fileName)), // TODO
          previousIds = graphNode.previousNodes.toList(),
          successIds = graphNode.nextNodes.toList()
        )
        is Graph.FinishNode -> Node.ExitNode(
          instructions = emptyList(),
          procId = functionIndex,
          id = graphNode.id,
          exitNodes = listOf(),
          location = Location(0, 0, Location.SourceFile(fileName)), // TODO
          previousIds = graphNode.previousNodes.toList(),
          successIds = graphNode.nextNodes.toList()
        )
        is Graph.PruneNode -> Node.PruneNode(
          instructions = 
            this
              .withCurrentBranch(graphNode.ifTrueBranch)
              .withContext(TranslatorState.CURRENT_BLOCK, graphNode.basicBlock)
              .translateInstructionAsInstruction(graphNode.condition),
          procId = functionIndex,
          id = graphNode.id,
          exitNodes = listOf(graphNode.exitNode!!),
          location = Location(0, 0, Location.SourceFile(fileName)), // TODO
          previousIds = graphNode.previousNodes.toList(),
          successIds = graphNode.nextNodes.toList(),
          trueBranch = graphNode.ifTrueBranch,
          ifKind = IfKind.IK_IF,
          pruneKind = if (graphNode.ifTrueBranch) Node.PruneNode.Kind.TRUE_BRANCH else Node.PruneNode.Kind.FALSE_BRANCH
        )
        is Graph.StatementNode -> {
          val instructions = graphNode.instructions.flatMap { 
            this
              .withContext(TranslatorState.CURRENT_BLOCK, graphNode.basicBlock)
              .translateInstructionAsInstruction(it)
          }
          val statementNodeKind: Node.StatementNode.Kind = if (instructions.isEmpty()) {
            Node.StatementNode.Kind.SKIP
          } else {
            val lastInstruction = graphNode.instructions.last()
            translateInstructionAsStatementKind(lastInstruction)
          }
          Node.StatementNode(
            instructions = instructions,
            procId = functionIndex,
            id = graphNode.id,
            exitNodes = listOf(graphNode.exitNode!!),
            location = Location(0, 0, Location.SourceFile(fileName)), // TODO
            previousIds = graphNode.previousNodes.toList(),
            successIds = graphNode.nextNodes.toList(),
            "A",
            statementNodeKind
          )
        }
      }
    }
  }
}