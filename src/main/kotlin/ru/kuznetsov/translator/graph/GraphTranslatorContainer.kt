package ru.kuznetsov.translator.graph

import proto.value.ValueOuterClass.Instruction

class GraphTranslatorContainer(
  val translators: MutableList<AbstractGraphChanger> = mutableListOf()
) {
  fun isInstructionAffected(instruction: Instruction): Boolean = translators.any { it.isInstructionAffected(instruction) }
}