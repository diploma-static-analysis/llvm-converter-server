package ru.kuznetsov.translator.location

import org.llvm.Location
import proto.location.LocationOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.utils.Utils

data class LocationContainer(
  val fileName: String,
  val line: Int,
  val column: Int
) {
  fun toLocation(): Location {
    return Location(line, column, Location.SourceFile(fileName))
  }
  
  companion object {
    fun TranslatorState.createLocation(location: LocationOuterClass.Location): LocationContainer {
      return LocationContainer(
        fileName = fileName,
        line = location.row,
        column = location.column
      )
    }
    
    fun TranslatorState.createLocation(location: LocationOuterClass.InstructionText): LocationContainer {
      val line = Utils.findLineNumber(fileText, location.inst)
      val column = Utils.findFirstNotWhiteSpaceOfLine(fileText, line)
      return LocationContainer(
        fileName = fileName,
        line = line + 1,
        column = column + 1
      )
    }
  }
}