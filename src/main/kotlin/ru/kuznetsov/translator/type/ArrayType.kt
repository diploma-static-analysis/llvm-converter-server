package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

object ArrayType {
  fun TranslatorState.translateArrayTypeToType(arrayType: TypeOuterClass.Type.ArrayType): Type {
    return Type.Array(
      contentType = translateType(arrayType.subtype)
    )
  }
}