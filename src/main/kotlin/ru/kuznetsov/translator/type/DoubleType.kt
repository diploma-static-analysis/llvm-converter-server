package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass

object DoubleType {
  fun translateDoubleTypeToType(doubleType: TypeOuterClass.Type.DoubleType): Type {
    return Type.Float(
      kind = "FDouble"
    )
  }
}