package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass

object FunctionType {
  fun translateFunctionTypeToFunType(functionType: TypeOuterClass.Type.FunctionType): Type {
    return Type.Fun()
  }
}