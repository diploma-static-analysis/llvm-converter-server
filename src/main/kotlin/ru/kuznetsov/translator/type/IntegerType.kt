package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass

object IntegerType {
  fun translateIntegerTypeToType(arrayType: TypeOuterClass.Type.IntegerType): Type {
    val kind: Type.Int.Kind = when (arrayType.size) {
      1 -> Type.Int.Kind.I_BOOL
      8 -> Type.Int.Kind.I_CHAR
      4 -> Type.Int.Kind.I_SHORT // todo
      16 -> Type.Int.Kind.I_SHORT
      32 -> Type.Int.Kind.I_INT
      64 -> Type.Int.Kind.I_LONG
      128 -> Type.Int.Kind.I_LONG_LONG
      else -> throw IllegalStateException("Size incompatible with type: ${arrayType.size}")
    }
    return Type.Int(
      kind = kind
    )
  }
}