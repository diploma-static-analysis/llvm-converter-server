package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

object PointerType {
  fun TranslatorState.translatePointerTypeToType(pointerType: TypeOuterClass.Type.PointerType): Type {
    check(pointerType.subtypeCount < 2)
    if (pointerType.subtypeCount == 0) {
      return Type.Void()
    }
    return Type.Ptr(
      kind = Type.Ptr.Kind.PK_POINTER,
      type = translateType(pointerType.subtypeList[0])
    )
  }
}