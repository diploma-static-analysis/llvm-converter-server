package ru.kuznetsov.translator.type

import org.llvm.*
import org.llvm.Type
import proto.type.TypeOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

object StructType {
  fun TranslatorState.addStruct(structType: TypeOuterClass.Type.StructType) {
    val typeStructContainer = ClassDescription.ClassDescriptionContainer.TypeStructContainer()
    val structName = getStructName(structType)
    val classStructContainer = ClassDescription.ClassDescriptionContainer(
      typeName = TypeName.CsuTypeName(csuKind = "Class", name = structName),
      typeStruct = typeStructContainer
    )
    structType.typeList.forEachIndexed { index, subType ->
      typeStructContainer.instanceFields.add(
        Field.InstanceField(
        fieldName = "field_$index",
        type = translateType(subType),
        annotation = Annotations(emptyList())
      ))
    }
    classDescriptions[structName] = classStructContainer
  }
  
  private fun TranslatorState.getStructName(structType: TypeOuterClass.Type.StructType): String {
    return if (structType.isLiteral) {
      val methodName = when (val method = this.currentMethod) {
        is Method.RegularMethod -> method.methodName
        is Method.SpecialMethod -> method.methodName
        null -> "empty_method"
        else -> throw IllegalArgumentException("Unsupported struct type ${method.javaClass.name}")
      }
      val blockName = getContextNullable(TranslatorState.CURRENT_BLOCK)?.name ?: "empty_block"
      "anonymous-llvm-structure-$methodName-$blockName" 
    } else {
      structType.name
    }
  }
  
  fun TranslatorState.translateStructTypeToType(structType: TypeOuterClass.Type.StructType): Type {
    val name = getStructName(structType)
    if (classDescriptions[name] == null) {
      addStruct(structType)
    }
    return Type.Struct(
      structName = name
    )
  }
}