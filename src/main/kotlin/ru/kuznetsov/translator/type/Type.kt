package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.ArrayType.translateArrayTypeToType
import ru.kuznetsov.translator.type.DoubleType.translateDoubleTypeToType
import ru.kuznetsov.translator.type.FunctionType.translateFunctionTypeToFunType
import ru.kuznetsov.translator.type.IntegerType.translateIntegerTypeToType
import ru.kuznetsov.translator.type.PointerType.translatePointerTypeToType
import ru.kuznetsov.translator.type.StructType.translateStructTypeToType
import ru.kuznetsov.translator.type.VectorType.translateVectorTypeToType
import ru.kuznetsov.translator.type.VoidType.translateVoidTypeToType

object Type {
  fun TranslatorState.translateType(type: TypeOuterClass.Type): Type {
    if (type.hasArrayType()) {
      return translateArrayTypeToType(type.arrayType)
    }
    if (type.hasFunctionType()) {
      return translateFunctionTypeToFunType(type.functionType)
    }
    if (type.hasIntegerType()) {
      return translateIntegerTypeToType(type.integerType)
    }
    if (type.hasDoubleType()) {
      return translateDoubleTypeToType(type.doubleType)
    }
    if (type.hasFloatType()) {
      return Type.Float("FFloat")
    }
    if (type.hasPointerType()) {
      return translatePointerTypeToType(type.pointerType)
    }
    if (type.hasStructType()) {
      return translateStructTypeToType(type.structType)
    }
    if (type.hasTargetExtType()) {
      throw IllegalStateException("Can't parse TargetExtType")
    }
    if (type.hasTypedPointerType()) {
      throw IllegalStateException("Can't parse TypedPointerType")
    }
    if (type.hasVectorType()) {
      return translateVectorTypeToType(type.vectorType)
    }
    if (type.hasVoidType()) {
      return translateVoidTypeToType(type.voidType)
    }
    if (type.hasMetadataType()) {
      return Type.Void()
    }
    throw IllegalStateException("Can't find candidate for Type: $type")
  }
}