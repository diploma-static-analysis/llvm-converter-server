package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

object VectorType {
  fun TranslatorState.translateVectorTypeToType(vectorType: TypeOuterClass.Type.VectorType): Type {
    return Type.Array(
      contentType = translateType(vectorType.type)
    )
  }
}