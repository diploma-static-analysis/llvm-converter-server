package ru.kuznetsov.translator.type

import org.llvm.Type
import proto.type.TypeOuterClass

object VoidType {
  fun translateVoidTypeToType(voidType: TypeOuterClass.Type.VoidType): Type {
    return Type.Void()
  }
}