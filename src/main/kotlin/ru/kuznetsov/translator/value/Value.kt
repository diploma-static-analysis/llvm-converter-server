package ru.kuznetsov.translator.value

import org.llvm.Expression
import org.llvm.Identifier
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.location.LocationContainer.Companion.createLocation
import ru.kuznetsov.translator.value.argument.Argument.translateArgument
import ru.kuznetsov.translator.value.user.User.translateUser

object Value {
  fun TranslatorState.translateValueAsIdentifier(value: ValueOuterClass.Value): Identifier {
    if (value.hasName()) {
      return Identifier(
        kind = Identifier.Kind.NORMAL,
        name = value.name,
        stamp = 0
      )
    }
    if (value.hasUser()) {
      val user = value.user
      if (user.hasDerivedUser()) {
        val derivedUser = user.derivedUser
        if (derivedUser.hasName()) {
          return Identifier(
            kind = Identifier.Kind.NORMAL,
            name = derivedUser.name,
            stamp = 0
          )
        }
      }
    }
    throw IllegalArgumentException("Identifier should be ID: $value")
  }
  
  fun TranslatorState.translateValueAsExpression(value: ValueOuterClass.Value): Expression {
    val location = if (this.currentLocation != null) {
      currentLocation 
    } else if (value.hasLocation()) {
      this.createLocation(value.location)
    } else if (value.hasInstructionText()) {
      this.createLocation(value.instructionText)
    } else {
      throw IllegalStateException("Can't detect value location")
    }
    val newState = this
      .withCurrentUnsafeType(value.type)
      .withCurrentLocation(location)
    kotlin.runCatching { translateValueAsIdentifier(value) }.getOrNull()?.let { identifier: Identifier -> 
      return Expression.VariableExpression(
        identifier = identifier
      )
    }
    if (value.hasArgument()) {
      return newState
        .translateArgument(value.argument)
    }
    if (value.hasBasicBlock()) {
      TODO()
    }
    if (value.hasInlineAsm()) {
      TODO()
    }
    if (value.hasMetadataAsValue()) {
      TODO()
    }
    if (value.hasUser()) {
      return newState.translateUser(value.user)
    }
    throw IllegalStateException("Failed to parse Value: $value")
  }
}