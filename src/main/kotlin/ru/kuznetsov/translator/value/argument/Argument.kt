package ru.kuznetsov.translator.value.argument

import org.llvm.Attributes
import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Type
import proto.type.TypeKt.voidType
import proto.type.type
import proto.value.ValueOuterClass
import proto.value.argument
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

object Argument {
  fun TranslatorState.translateArgument(argument: ValueOuterClass.Argument): Expression {
    return Expression.VariableExpression(
      identifier = Identifier(
        Identifier.Kind.NORMAL,
        argument.name,
        0
      )
    )
  }
  
  private fun List<ValueOuterClass.Argument>.pre(isStatic: Boolean): List<ValueOuterClass.Argument> {
    return (if (isStatic) emptyList() else listOf(argument { this.name = "self"; this.type = type { this.voidType = voidType { } } })) + this
  }
  
  fun TranslatorState.translateArgumentsAsFormals(isStatic: Boolean, arguments: List<ValueOuterClass.Argument>): List<Attributes.Formal> {
    return arguments.pre(isStatic).map { Attributes.Formal(it.name, translateType(it.type)) }
  }
  
  fun TranslatorState.translateArgumentsAsStrings(isStatic: Boolean, arguments: List<ValueOuterClass.Argument>): List<String> {
    return translateArgumentsAsFormals(isStatic, arguments).map { it.name }
  }
}