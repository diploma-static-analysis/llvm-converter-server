package ru.kuznetsov.translator.value.user

import org.llvm.Expression
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.constant.Constant.translateConstant
import ru.kuznetsov.translator.value.user.derivedUser.DerivedUser.translateDerivedUser

object User {
  fun TranslatorState.translateUser(user: ValueOuterClass.User): Expression {
    if (user.hasConstant()) {
      return translateConstant(user.constant)
    }
    if (user.hasDerivedUser()) {
      return translateDerivedUser(user.derivedUser)
    }
    if (user.hasInstruction()) {
      TODO()
    }
    if (user.hasOperator()) {
      TODO()
    }
    throw IllegalStateException("Failed to parse User: $user")
  }
}