package ru.kuznetsov.translator.value.user.constant

import org.llvm.Expression
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.constant.constantData.ConstantData.translateConstantData
import ru.kuznetsov.translator.value.user.instruction.Instruction.translateInstructionAsExpression

object Constant {
  fun TranslatorState.translateConstant(constant: ValueOuterClass.Constant): Expression {
    if (constant.hasBlockAddress()) {
      throw IllegalStateException("Can't parse BlockAddress")
    }
    if (constant.hasConstantAggregate()) {
      throw IllegalStateException("Can't parse ConstantAggregate")
    }
    if (constant.hasConstantData()) {
      return translateConstantData(constant.constantData)
    }
    if (constant.hasConstantExpr()) {
      return translateInstructionAsExpression(constant.constantExpr.subInstruction)
    }
    if (constant.hasDsoLocalEquivalent()) {
      throw IllegalStateException("Can't parse DSOLocalEquivalent")
    }
    if (constant.hasGlobalValue()) {
      throw IllegalStateException("Can't parse GlobalValue")
    }
    if (constant.hasNoCFIValue()) {
      throw IllegalStateException("Can't parse NoCFIValue")
    }
    throw IllegalStateException("Can't find candidate for Constant: $constant")
  }
}