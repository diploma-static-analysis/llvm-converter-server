package ru.kuznetsov.translator.value.user.constant.constantData

import org.llvm.Expression
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType

object ConstantAggregateZero {
  fun TranslatorState.translateConstantAggregateZero(constantAggregateZero: ValueOuterClass.ConstantAggregateZero): Expression {
    val type = translateType(constantAggregateZero.type)
    return when (type) {
      is Type.Array -> Expression.ConstExpression.ClassConst(constValue = "Array")
      is Type.Float -> Expression.ConstExpression.FloatConst(constValue = 0.0f)
      is Type.Fun -> TODO()
      is Type.Int -> Expression.ConstExpression.IntConst(constValue = Expression.ConstExpression.IntConst.SubValue(value = 0, isPointer = false))
      is Type.Ptr -> TODO()
      is Type.Struct -> TODO()
      is Type.Var -> TODO()
      is Type.Void -> TODO()
    }
  }
}