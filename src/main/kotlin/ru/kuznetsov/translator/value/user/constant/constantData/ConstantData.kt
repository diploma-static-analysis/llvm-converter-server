package ru.kuznetsov.translator.value.user.constant.constantData

import org.llvm.Expression
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.constant.constantData.ConstantAggregateZero.translateConstantAggregateZero
import ru.kuznetsov.translator.value.user.constant.constantData.ConstantDataSequential.translateConstantDataSequential
import ru.kuznetsov.translator.value.user.constant.constantData.ConstantFP.translateConstantFPToExpression
import ru.kuznetsov.translator.value.user.constant.constantData.ConstantInt.translateConstantInt
import ru.kuznetsov.translator.value.user.constant.constantData.ConstantPointerNull.translateConstantPointerNullToExpression

object ConstantData {
  fun TranslatorState.translateConstantData(constantData: ValueOuterClass.ConstantData): Expression {
    if (constantData.hasConstantAggregateZero()) {
      return translateConstantAggregateZero(constantData.constantAggregateZero)
    }
    if (constantData.hasConstantDatSequential()) {
      return translateConstantDataSequential(constantData.constantDatSequential)
    }
    if (constantData.hasConstantFP()) {
      return translateConstantFPToExpression(constantData.constantFP)
    }
    if (constantData.hasConstantInt()) {
      return translateConstantInt(constantData.constantInt)
    }
    if (constantData.hasConstantPointerNull()) {
      return translateConstantPointerNullToExpression(constantData.constantPointerNull)
    }
    if (constantData.hasConstantTargetNone()) {
      throw IllegalStateException("Can't parse ConstantTargetNone")
    }
    if (constantData.hasConstantTokenNone()) {
      throw IllegalStateException("Can't parse ConstantTokenNone")
    }
    if (constantData.hasUndefValue()) {
      return Expression.ConstExpression.ClassConst(constValue = "Class")
    }
    throw IllegalStateException("Can't find candidate for ConstantData: $constantData")
  }
}