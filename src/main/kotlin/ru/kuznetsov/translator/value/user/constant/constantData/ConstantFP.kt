package ru.kuznetsov.translator.value.user.constant.constantData

import org.llvm.Expression
import proto.value.ValueOuterClass

object ConstantFP {
  fun translateConstantFPToExpression(constantFP: ValueOuterClass.ConstantFP): Expression {
    return Expression.ConstExpression.FloatConst(
      constValue = constantFP.value.toFloat()
    )
  }
}