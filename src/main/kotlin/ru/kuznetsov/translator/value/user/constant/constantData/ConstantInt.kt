package ru.kuznetsov.translator.value.user.constant.constantData

import org.llvm.Expression
import proto.value.ValueOuterClass

object ConstantInt {
  fun translateConstantInt(constantInt: ValueOuterClass.ConstantInt): Expression {
    return Expression.ConstExpression.IntConst(
      "ConstExpression",
      "Int",
      Expression.ConstExpression.IntConst.SubValue(constantInt.value, false) // pointer is questionable
    )
  }
}