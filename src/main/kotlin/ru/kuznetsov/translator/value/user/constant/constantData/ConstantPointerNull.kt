package ru.kuznetsov.translator.value.user.constant.constantData

import org.llvm.Expression
import proto.value.ValueOuterClass

object ConstantPointerNull {
  fun translateConstantPointerNullToExpression(constantPointerNull: ValueOuterClass.ConstantPointerNull): Expression {
    return Expression.ConstExpression.IntConst(
      constValue = Expression.ConstExpression.IntConst.SubValue(
        value = 0,
        isPointer = true
      )
    )
  }
}