package ru.kuznetsov.translator.value.user.derivedUser

import org.llvm.Expression
import org.llvm.Identifier
import proto.value.ValueOuterClass

object DerivedUser {
  fun translateDerivedUser(derivedUser: ValueOuterClass.DerivedUser): Expression {
    if (derivedUser.hasMemoryAccess()) {
      throw IllegalStateException("Can't parse DerivedUser")
    } 
    if (derivedUser.hasName()) {
      return Expression.VariableExpression(
        "VarExpression",
        Identifier(
          Identifier.Kind.NORMAL,
          derivedUser.name,
          0
        )
      )
    }
    throw IllegalStateException("Can't find candidate for DerivedUser: $derivedUser")
  }
}