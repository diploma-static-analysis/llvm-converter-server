package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object BinaryOperator {
  private fun translateBinaryOperatorType(binaryOperatorType: ValueOuterClass.BinaryOperator.BinaryOperatorType): Expression.BinaryOperationExpression.Operator {
    return when (binaryOperatorType) {
      ValueOuterClass.BinaryOperator.BinaryOperatorType.ADD -> Expression.BinaryOperationExpression.Operator.PLUSPI
      ValueOuterClass.BinaryOperator.BinaryOperatorType.SUB -> Expression.BinaryOperationExpression.Operator.MINUSPI
      ValueOuterClass.BinaryOperator.BinaryOperatorType.F_ADD -> Expression.BinaryOperationExpression.Operator.PLUSPI
      ValueOuterClass.BinaryOperator.BinaryOperatorType.F_DIV -> Expression.BinaryOperationExpression.Operator.DIVF
      ValueOuterClass.BinaryOperator.BinaryOperatorType.F_SUB -> Expression.BinaryOperationExpression.Operator.MINUSPI
      ValueOuterClass.BinaryOperator.BinaryOperatorType.MUL -> Expression.BinaryOperationExpression.Operator.MULT
      ValueOuterClass.BinaryOperator.BinaryOperatorType.AND -> Expression.BinaryOperationExpression.Operator.BAND
      ValueOuterClass.BinaryOperator.BinaryOperatorType.S_DIV -> Expression.BinaryOperationExpression.Operator.DIV
      ValueOuterClass.BinaryOperator.BinaryOperatorType.S_REM -> Expression.BinaryOperationExpression.Operator.MOD
      ValueOuterClass.BinaryOperator.BinaryOperatorType.SHL -> Expression.BinaryOperationExpression.Operator.SHIFTLT
      ValueOuterClass.BinaryOperator.BinaryOperatorType.A_SHR -> Expression.BinaryOperationExpression.Operator.SHIFTRT
      ValueOuterClass.BinaryOperator.BinaryOperatorType.L_SHR -> Expression.BinaryOperationExpression.Operator.SHIFTRT
      ValueOuterClass.BinaryOperator.BinaryOperatorType.OR -> Expression.BinaryOperationExpression.Operator.LOR
      ValueOuterClass.BinaryOperator.BinaryOperatorType.XOR -> Expression.BinaryOperationExpression.Operator.BXOR
      ValueOuterClass.BinaryOperator.BinaryOperatorType.U_DIV -> Expression.BinaryOperationExpression.Operator.DIV
      ValueOuterClass.BinaryOperator.BinaryOperatorType.U_REM -> Expression.BinaryOperationExpression.Operator.MOD
      ValueOuterClass.BinaryOperator.BinaryOperatorType.F_MUL -> Expression.BinaryOperationExpression.Operator.MULT
      else -> throw IllegalStateException("Unknown operator type: $binaryOperatorType")
    }
  }
  
  fun TranslatorState.translateBinaryOperatorAsInstruction(binaryOperator: ValueOuterClass.BinaryOperator): List<Instruction> {
    val operatorType = translateBinaryOperatorType(binaryOperator.operatorType)
    val resultId = translateValueAsIdentifier(binaryOperator.result)
    val resultType = translateType(binaryOperator.result.type)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    val location = currentLocation!!.toLocation()
    
    return listOf(
      Instruction.Store(
        location = location,
        lvalue = Expression.VariableExpression(
          identifier = temporaryId
        ),
        rvalue = Expression.BinaryOperationExpression(
          operator = operatorType,
          left = translateValueAsExpression(binaryOperator.l),
          right = translateValueAsExpression(binaryOperator.r),
        ),
        type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = resultType), 
      ),
      Instruction.Load(
        location = location,
        identifier = resultId,
        expression = Expression.VariableExpression(
          identifier = temporaryId
        ),
        type = resultType,
      ),
    )
  }
}