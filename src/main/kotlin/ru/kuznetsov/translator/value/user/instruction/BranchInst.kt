package ru.kuznetsov.translator.value.user.instruction

import org.llvm.*
import org.llvm.Instruction
import proto.value.ValueOuterClass
import proto.value.ValueOuterClass.Instruction.BranchInst.ConditionalBranchInst
import proto.value.ValueOuterClass.Instruction.BranchInst.UnconditionalBranchInst
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.Value.translateValueAsExpression

object BranchInst {
  fun TranslatorState.translateBranchInstAsInstruction(branchInst: ValueOuterClass.Instruction.BranchInst): List<Instruction> {
    if (branchInst.hasUnconditionalBranchInst()) {
      return translateUnconditionalBranchInstAsInstruction(branchInst.unconditionalBranchInst)
    } else if (branchInst.hasConditionalBranchInst()) {
      return translateConditionalBranchInstAsInstruction(branchInst.conditionalBranchInst)
    }
    throw IllegalStateException("Can't translate branch insts")
    
  }
  
  private fun TranslatorState.translateUnconditionalBranchInstAsInstruction(unconditionalBranchInst: UnconditionalBranchInst): List<Instruction> {
    return emptyList()
  }

  private fun TranslatorState.translateConditionalBranchInstAsInstruction(conditionalBranchInst: ConditionalBranchInst): List<Instruction> {
    val currentBranch = currentBranch!!
    return listOf(
      Instruction.Prune(
        location = currentLocation!!.toLocation(),
        condition = if (currentBranch) 
          translateValueAsExpression(conditionalBranchInst.condition)
        else Expression.UnaryOperationExpression(
          operator = Expression.UnaryOperationExpression.Operator.LNOT,
          expression = translateValueAsExpression(conditionalBranchInst.condition),
          type = Type.Int(kind = Type.Int.Kind.I_BOOL)
        ),
        trueBranch = currentBranch,
        ifKind = IfKind.IK_IF
      )
    )
  }
}