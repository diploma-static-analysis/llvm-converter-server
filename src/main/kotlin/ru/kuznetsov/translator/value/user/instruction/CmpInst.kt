package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object CmpInst {
  fun TranslatorState.translateCmpInstAsInstruction(cmpInst: ValueOuterClass.Instruction.CmpInst): List<Instruction> {
    val inferOperator = when (val llvmOperator = cmpInst.type) {
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_EQ -> Expression.BinaryOperationExpression.Operator.EQ
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_NE -> Expression.BinaryOperationExpression.Operator.NE
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_UGT -> Expression.BinaryOperationExpression.Operator.GT
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_ULT -> Expression.BinaryOperationExpression.Operator.LT
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_SGE -> Expression.BinaryOperationExpression.Operator.GE
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_SGT -> Expression.BinaryOperationExpression.Operator.GT
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_SLE -> Expression.BinaryOperationExpression.Operator.LE
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_SLT -> Expression.BinaryOperationExpression.Operator.LT
      ValueOuterClass.Instruction.CmpInst.CmpType.FCMP_OLT -> Expression.BinaryOperationExpression.Operator.LT
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_ULE -> Expression.BinaryOperationExpression.Operator.LE
      ValueOuterClass.Instruction.CmpInst.CmpType.ICMP_UGE -> Expression.BinaryOperationExpression.Operator.GE
      else -> throw IllegalStateException("Can't parse operator $llvmOperator")
    }
    
    val resultId = translateValueAsIdentifier(cmpInst.result)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    
    return listOf(
      Instruction.Store(
        location = currentLocation!!.toLocation(),
        lvalue = Expression.VariableExpression(
          identifier = temporaryId
        ),
        rvalue = Expression.BinaryOperationExpression(
          operator = inferOperator,
          left = translateValueAsExpression(cmpInst.left),
          right = translateValueAsExpression(cmpInst.right),
        ),
        type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = translateType(cmpInst.result.type)), // TODO look to type
      ),
      Instruction.Load(
        location = currentLocation.toLocation(),
        identifier = resultId,
        expression = Expression.VariableExpression(
          identifier = temporaryId
        ),
        type = translateType(cmpInst.result.type),
      )
    )
  }
}