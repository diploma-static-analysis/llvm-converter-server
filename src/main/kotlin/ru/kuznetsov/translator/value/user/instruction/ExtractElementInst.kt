package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object ExtractElementInst {
  fun TranslatorState.translateExtractElementInst(extractElementInst: ValueOuterClass.Instruction.ExtractElementInst): List<Instruction> {
    val type = translateType(extractElementInst.type)
    val fakeType = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = type)
    val result = translateValueAsIdentifier(extractElementInst.`return`)
    val fakeResult = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = result.name,
      stamp = 1
    )
    val location = currentLocation!!.toLocation()
    return listOf(
      Instruction.Store(
        location = location,
        lvalue = Expression.VariableExpression(
          identifier = fakeResult
        ),
        rvalue = Expression.LIndexExpression(
          array = translateValueAsExpression(extractElementInst.target),
          index = translateValueAsExpression(extractElementInst.index)
        ),
        type = fakeType
      ),
      Instruction.Load(
        location = location,
        identifier = result,
        expression = Expression.VariableExpression(
          identifier = fakeResult
        ),
        type = type,
      )
    )
  }
}