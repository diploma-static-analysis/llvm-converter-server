package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object GetElementPtrInst {
  fun TranslatorState.translateSubExpression(type: Type, operands: Iterator<ValueOuterClass.Value>, target: Expression): Expression {
    if (!operands.hasNext()) {
      return target
    }
    val currentOperand = operands.next()
    when (type) {
      is Type.Ptr -> {
        check(currentOperand.hasUser()) { "Ptr type should have value as int = 0, got $currentOperand" }
        /*
        TODO this is array call, how to check?
        
        val user = currentOperand.user
        check(user.hasConstant()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val constant = user.constant
        check(constant.hasConstantData()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val constantData = constant.constantData
        check(constantData.hasConstantInt()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val constantInt = constantData.constantInt
        check(constantInt.value == 0L) { "Ptr type should have value as int = 0, got $type" }*/
        return translateSubExpression(type.type, operands, target) // TODO if ptr is not first
      }
      is Type.Struct -> {
        check(currentOperand.hasUser()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val user = currentOperand.user
        check(user.hasConstant()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val constant = user.constant
        check(constant.hasConstantData()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val constantData = constant.constantData
        check(constantData.hasConstantInt()) { "Ptr type should have value as int = 0, got $currentOperand" }
        val constantInt = constantData.constantInt
        
        val classDescription = classDescriptions[type.structName]!!
        val subtype = classDescription.typeStruct.instanceFields[constantInt.value.toInt()].type
        
        return Expression.LFieldExpression(
          identifier = Expression.LFieldExpression.Identifier("field_${constantInt.value}"),
          expression = translateSubExpression(subtype, operands, target),
          type = type
        )
      }
      is Type.Array -> {
        return Expression.LIndexExpression(
          array = translateSubExpression(type.contentType, operands, target),
          index = translateValueAsExpression(currentOperand)
        )
      }
      else -> throw IllegalStateException("Can't parse type $type")
    }
  }
  
  fun TranslatorState.translateGetElementPtrInstToInstruction(getElementPtrInst: ValueOuterClass.Instruction.GetElementPtrInst): List<Instruction> {
    val resultId = translateValueAsIdentifier(getElementPtrInst.result)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    val resultType = translateType(getElementPtrInst.result.type)
    return listOf(Instruction.Store(
      location = currentLocation!!.toLocation(),
      lvalue = Expression.VariableExpression(identifier = temporaryId),
      rvalue = translateGetElementPtrInstToExpression(getElementPtrInst),
      type = Type.Ptr(
        kind = Type.Ptr.Kind.PK_POINTER,
        type = resultType
      )
    ), Instruction.Load(
      location = currentLocation.toLocation(),
      identifier = resultId,
      expression = Expression.VariableExpression(identifier = temporaryId),
      type = resultType
    ))
  }
  
  fun TranslatorState.translateGetElementPtrInstToExpression(getElementPtrInst: ValueOuterClass.Instruction.GetElementPtrInst): Expression {
    val type = translateType(getElementPtrInst.targetType)
    val operands = getElementPtrInst.operandList
    return translateSubExpression(type, operands.iterator(), translateValueAsExpression(getElementPtrInst.target))
  }
}