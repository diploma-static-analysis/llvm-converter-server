package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object InsertElementInst {
  fun TranslatorState.translateInsertElementInstToInstruction(insertElementInst: ValueOuterClass.Instruction.InsertElementInst): List<Instruction> {
    val array = kotlin.runCatching { translateValueAsIdentifier(insertElementInst.source) }.getOrElse { return emptyList() }
    val copyOfArray = translateValueAsIdentifier(insertElementInst.result)
    val temporaryArray = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = array.name,
      stamp = 1
    )
    val arrayType = translateType(insertElementInst.type)
    val temporaryArrayType = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = arrayType)
    
    val location = currentLocation!!.toLocation()
    
    return listOf(
      Instruction.Store(
        location = location,
        lvalue = Expression.VariableExpression(identifier = temporaryArray),
        rvalue = translateValueAsExpression(insertElementInst.source),
        type = temporaryArrayType
      ),
      Instruction.Load(
        location = location,
        identifier = copyOfArray,
        expression = Expression.VariableExpression(identifier = temporaryArray),
        type = arrayType
      ),
      Instruction.Store(
        location = location,
        lvalue = Expression.LIndexExpression(
          array = Expression.VariableExpression(identifier = copyOfArray),
          index = translateValueAsExpression(insertElementInst.index)
        ),
        rvalue = translateValueAsExpression(insertElementInst.element),
        type = translateType(insertElementInst.element.type)
      )
    )
  }
}