package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier
import ru.kuznetsov.translator.value.user.instruction.GetElementPtrInst.translateSubExpression

object InsertValueInst {
  private fun TranslatorState.translateInsertValueInstToExpression(insertValueInst: ValueOuterClass.Instruction.InsertValueInst, target: Expression): Expression {
    val type = translateType(insertValueInst.targetType)
    val operands = insertValueInst.operandList
    return translateSubExpression(type, operands.iterator(), target)
  }
  
  fun TranslatorState.translateInsertValueInstToInstruction(insertValueInst: ValueOuterClass.Instruction.InsertValueInst): List<Instruction> {
    val resultId = translateValueAsIdentifier(insertValueInst.result)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    val resultType = translateType(insertValueInst.targetType)
    return listOf(Instruction.Store(
      location = currentLocation!!.toLocation(),
      lvalue = Expression.VariableExpression(identifier = temporaryId),
      rvalue = translateValueAsExpression(insertValueInst.result),
      type = Type.Ptr(
        kind = Type.Ptr.Kind.PK_POINTER,
        type = resultType
      )
    ), Instruction.Load(
      location = currentLocation.toLocation(),
      identifier = resultId,
      expression = Expression.VariableExpression(identifier = temporaryId),
      type = resultType
    ), Instruction.Store(
      location = currentLocation.toLocation(),
      lvalue = translateInsertValueInstToExpression(insertValueInst, Expression.VariableExpression(identifier = resultId)),
      rvalue = translateValueAsExpression(insertValueInst.value),
      type = translateType(insertValueInst.value.type)
    ))
  }
}