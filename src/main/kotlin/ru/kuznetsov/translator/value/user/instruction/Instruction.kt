package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Instruction
import org.llvm.Node
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.location.LocationContainer.Companion.createLocation
import ru.kuznetsov.translator.value.user.instruction.BinaryOperator.translateBinaryOperatorAsInstruction
import ru.kuznetsov.translator.value.user.instruction.BranchInst.translateBranchInstAsInstruction
import ru.kuznetsov.translator.value.user.instruction.CmpInst.translateCmpInstAsInstruction
import ru.kuznetsov.translator.value.user.instruction.ExtractElementInst.translateExtractElementInst
import ru.kuznetsov.translator.value.user.instruction.GetElementPtrInst.translateGetElementPtrInstToExpression
import ru.kuznetsov.translator.value.user.instruction.GetElementPtrInst.translateGetElementPtrInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.InsertElementInst.translateInsertElementInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.InsertValueInst.translateInsertValueInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.LandingPadInst.translateLandingPadInstructionToInstruction
import ru.kuznetsov.translator.value.user.instruction.ReturnInst.translateReturnInstAsInstruction
import ru.kuznetsov.translator.value.user.instruction.SelectInst.translateSelectInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.ShuffleVectorInst.translateShuffleVectorInst
import ru.kuznetsov.translator.value.user.instruction.StoreInst.translateStoreInstAsInstruction
import ru.kuznetsov.translator.value.user.instruction.callBase.CallBase.translateCallBaseToExpression
import ru.kuznetsov.translator.value.user.instruction.unaryinst.UnaryInst.translateUnaryInstAsExpression
import ru.kuznetsov.translator.value.user.instruction.unaryinst.UnaryInst.translateUnaryInstAsInstruction

object Instruction {
  fun TranslatorState.translateInstructionAsInstruction(instruction: ValueOuterClass.Instruction): List<Instruction> {
    val location = if (instruction.hasLocation()) {
      this.createLocation(instruction.location)
    } else if (instruction.hasInstructionText()) {
      this.createLocation(instruction.instructionText)
    } else {
      throw IllegalStateException("Can't detect value location")
    }

    try {
      return this
        .withCurrentUnsafeType(instruction.type)
        .withCurrentLocation(location)
        .translateInstructionAsInstructionWithState(instruction)
    } catch (e: Exception) {
      throw Exception("While parsing instruction: $instruction", e)
    }
  }
  
  private fun TranslatorState.translateInstructionAsInstructionWithState(instruction: ValueOuterClass.Instruction): List<Instruction> {
    if (instruction.hasAtomicCmpXchgInst()) {
      throw IllegalStateException("Can't parse AtomicCmpXchginst")
    }
    if (instruction.hasAtomicRMWInst()) {
      throw IllegalStateException("Can't parse AtomicRMWInst")
    }
    if (instruction.hasBinaryOperator()) {
      return translateBinaryOperatorAsInstruction(instruction.binaryOperator)
    }
    if (instruction.hasBranchInst()) {
      return translateBranchInstAsInstruction(instruction.branchInst)
    }
    if (instruction.hasCallBase()) {
      return translateCallBaseToExpression(instruction.callBase)
    }
    if (instruction.hasCatchReturnInst()) {
      throw IllegalStateException("Can't parse CatchReturninst")
    }
    if (instruction.hasCatchSwitchInst()) {
      throw IllegalStateException("Can't parse CatchSwitchinst")
    }
    if (instruction.hasCleanupReturnInst()) {
      throw IllegalStateException("Can't parse CleanupReturninst")
    }
    if (instruction.hasCmpInst()) {
      return translateCmpInstAsInstruction(instruction.cmpInst)
    }
    if (instruction.hasExtractElementInst()) {
      return translateExtractElementInst(instruction.extractElementInst)
    }
    if (instruction.hasFenceInst()) {
      throw IllegalStateException("Can't parse Fenceinst")
    }
    if (instruction.hasFuncletPadInst()) {
      throw IllegalStateException("Can't parse FuncletPadinst")
    }
    if (instruction.hasGetElementPtrInst()) {
      return translateGetElementPtrInstToInstruction(instruction.getElementPtrInst)
    }
    if (instruction.hasIndirectBrInst()) {
      throw IllegalStateException("Can't parse IndirectBrinst")
    }
    if (instruction.hasInsertElementInst()) {
      return translateInsertElementInstToInstruction(instruction.insertElementInst)
    }
    if (instruction.hasInsertValueInst()) {
      return translateInsertValueInstToInstruction(instruction.insertValueInst)
    }
    if (instruction.hasLandingPadInst()) {
      return translateLandingPadInstructionToInstruction(instruction.landingPadInst)
    }
    if (instruction.hasPHINode()) {
      throw IllegalStateException("Can't parse PHINode")
    }
    if (instruction.hasResumeInst()) {
      return emptyList() // todo
    }
    if (instruction.hasReturnInst()) {
      return translateReturnInstAsInstruction(instruction.returnInst)
    }
    if (instruction.hasSelectInst()) {
      return translateSelectInstToInstruction(instruction.selectInst)
    }
    if (instruction.hasShuffleVectorInst()) {
      return translateShuffleVectorInst(instruction.shuffleVectorInst)
    }
    if (instruction.hasStoreInst()) {
      return translateStoreInstAsInstruction(instruction.storeInst)
    }
    if (instruction.hasSwitchInst()) {
      throw IllegalStateException("Can't parse Switchinst")
    }
    if (instruction.hasUnaryInstruction()) {
      return translateUnaryInstAsInstruction(instruction.unaryInstruction)
    }
    if (instruction.hasUnreachableInst()) {
      return emptyList() // todo
    }
    throw IllegalStateException("Can't find candidate for CallBase: $instruction")
  }

  fun TranslatorState.translateInstructionAsStatementKind(instruction: ValueOuterClass.Instruction): Node.StatementNode.Kind {
    if (instruction.hasAtomicCmpXchgInst()) {
      throw IllegalStateException("Can't parse AtomicCmpXchginst")
    }
    if (instruction.hasAtomicRMWInst()) {
      throw IllegalStateException("Can't parse AtomicRMWInst")
    }
    if (instruction.hasBinaryOperator()) {
      return Node.StatementNode.Kind.BINARY_OPERATOR_STMT
    }
    if (instruction.hasBranchInst()) {
      throw IllegalStateException("Can't parse Branchinst")
    }
    if (instruction.hasCallBase()) {
      return Node.StatementNode.Kind.CALL
    }
    if (instruction.hasCatchReturnInst()) {
      throw IllegalStateException("Can't parse CatchReturninst")
    }
    if (instruction.hasCatchSwitchInst()) {
      throw IllegalStateException("Can't parse CatchSwitchinst")
    }
    if (instruction.hasCleanupReturnInst()) {
      throw IllegalStateException("Can't parse CleanupReturninst")
    }
    if (instruction.hasCmpInst()) {
      return Node.StatementNode.Kind.BINARY_OPERATOR_STMT
    }
    if (instruction.hasExtractElementInst()) {
      return Node.StatementNode.Kind.UNARY_OPERATOR
    }
    if (instruction.hasFenceInst()) {
      throw IllegalStateException("Can't parse Fenceinst")
    }
    if (instruction.hasFuncletPadInst()) {
      throw IllegalStateException("Can't parse FuncletPadinst")
    }
    if (instruction.hasGetElementPtrInst()) {
      return Node.StatementNode.Kind.UNARY_OPERATOR
    }
    if (instruction.hasIndirectBrInst()) {
      throw IllegalStateException("Can't parse IndirectBrinst")
    }
    if (instruction.hasInsertElementInst()) {
      throw IllegalStateException("Can't parse InsertElementinst")
    }
    if (instruction.hasInsertValueInst()) {
      throw IllegalStateException("Can't parse InsertValueinst")
    }
    if (instruction.hasLandingPadInst()) {
      throw IllegalStateException("Can't parse LandingPadinst")
    }
    if (instruction.hasPHINode()) {
      throw IllegalStateException("Can't parse PHINode")
    }
    if (instruction.hasResumeInst()) {
      return Node.StatementNode.Kind.EXCEPTION_HANDLER // TODO
    }
    if (instruction.hasReturnInst()) {
      return Node.StatementNode.Kind.RETURN_STMT
    }
    if (instruction.hasSelectInst()) {
      return Node.StatementNode.Kind.UNARY_OPERATOR
    }
    if (instruction.hasShuffleVectorInst()) {
      throw IllegalStateException("Can't parse ShuffleVectorinst")
    }
    if (instruction.hasStoreInst()) {
      return Node.StatementNode.Kind.BINARY_OPERATOR_STMT // TODO
    }
    if (instruction.hasSwitchInst()) {
      throw IllegalStateException("Can't parse Switchinst")
    }
    if (instruction.hasUnaryInstruction()) {
      return Node.StatementNode.Kind.UNARY_OPERATOR
    }
    if (instruction.hasUnreachableInst()) {
      return Node.StatementNode.Kind.SKIP
    }
    throw IllegalStateException("Can't find candidate for CallBase: $instruction")
  }

  fun TranslatorState.translateInstructionAsExpression(instruction: ValueOuterClass.Instruction): Expression {
    val location = this.currentLocation
      ?: if (instruction.hasLocation()) {
        this.createLocation(instruction.location)
      } else if (instruction.hasInstructionText()) {
        this.createLocation(instruction.instructionText)
      } else {
        throw IllegalStateException("Can't detect value location")
      }

    return this
      .withCurrentUnsafeType(instruction.type)
      .withCurrentLocation(location)
      .translateInstructionAsExpressionWithState(instruction)
  }
  
  private fun TranslatorState.translateInstructionAsExpressionWithState(instruction: ValueOuterClass.Instruction): Expression {
    if (instruction.hasUnaryInstruction()) {
      return translateUnaryInstAsExpression(instruction.unaryInstruction)
    }
    if (instruction.hasGetElementPtrInst()) {
      return translateGetElementPtrInstToExpression(instruction.getElementPtrInst)
    }
    throw IllegalStateException("Can't parse Expression: $instruction")
  }
}