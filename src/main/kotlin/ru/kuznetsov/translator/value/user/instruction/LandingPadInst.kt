package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Identifier
import org.llvm.Instruction
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier
import ru.kuznetsov.translator.value.user.constant.Constant.translateConstant

object LandingPadInst {
  fun TranslatorState.translateLandingPadInstructionToInstruction(landingPadInst: ValueOuterClass.Instruction.LandingPadInst): List<Instruction> {
    val clauseList = landingPadInst.clauseList
    if (clauseList.isEmpty()) return emptyList()
    if (clauseList[0].type == ValueOuterClass.Instruction.LandingPadInst.LandingPadClause.Type.CLEANUP) return emptyList()
    clauseList.forEach { clause ->
      when (clause.type) {
        ValueOuterClass.Instruction.LandingPadInst.LandingPadClause.Type.CLEANUP ->
          throw IllegalStateException()
        ValueOuterClass.Instruction.LandingPadInst.LandingPadClause.Type.FILTER ->
          TODO()
        ValueOuterClass.Instruction.LandingPadInst.LandingPadClause.Type.CATCH -> {
          return listOf(Instruction.Load(
            location = currentLocation!!.toLocation(),
            identifier = translateValueAsIdentifier(landingPadInst.result),
            expression = translateConstant(clause.constant),
            type = translateType(landingPadInst.type)
          ))
        }
        else -> throw IllegalStateException()
      }
    }
    throw IllegalStateException()
  }
}