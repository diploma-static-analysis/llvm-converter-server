package ru.kuznetsov.translator.value.user.instruction

import org.llvm.*
import org.llvm.Instruction
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression

object ReturnInst {
  fun TranslatorState.translateReturnInstAsInstruction(returnInst: ValueOuterClass.ReturnInst): List<Instruction> {
    if (returnInst.`return`.hasType()) {
      return listOf(Instruction.Store(
        "Store",
        currentLocation!!.toLocation(),
        Expression.LVariableExpression(
          "LvarExpression",
          PVariable.LocalVariable(
            "return",
            PVariable.Kind.LOCALVARIABLE,
            currentMethod!!
          )
        ),
        translateValueAsExpression(returnInst.`return`),
        translateType(returnInst.`return`.type)
      ))
    } else {
      return listOf()
    }
  }
}