package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object SelectInst {
  fun TranslatorState.translateSelectInstToInstruction(selectInst: ValueOuterClass.Instruction.SelectInst): List<Instruction> {
    val resultId = translateValueAsIdentifier(selectInst.result)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    return listOf(
      Instruction.Store(
        location = currentLocation!!.toLocation(),
        lvalue = Expression.VariableExpression(
          identifier = temporaryId
        ),
        rvalue = translateValueAsExpression(selectInst.trueValue), // TODO here if
        type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = translateType(selectInst.result.type)),
      ),
      Instruction.Load(
        location = currentLocation.toLocation(),
        identifier = resultId,
        expression = Expression.VariableExpression(
          identifier = temporaryId
        ),
        type = translateType(selectInst.result.type),
      )
    )
  }
}