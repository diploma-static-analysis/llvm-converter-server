package ru.kuznetsov.translator.value.user.instruction

import org.llvm.Expression
import org.llvm.Instruction
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object StoreInst {
  fun TranslatorState.translateStoreInstAsInstruction(storeInst: ValueOuterClass.StoreInst): List<Instruction> {
    return listOf(Instruction.Store(
      location = currentLocation!!.toLocation(),
      lvalue = Expression.VariableExpression(
        "VarExpression",
        kotlin.runCatching { translateValueAsIdentifier(storeInst.to) }.getOrNull() ?: return emptyList()
      ),
      rvalue = translateValueAsExpression(storeInst.from),
      type = translateType(storeInst.from.type)
    ))
  }
}