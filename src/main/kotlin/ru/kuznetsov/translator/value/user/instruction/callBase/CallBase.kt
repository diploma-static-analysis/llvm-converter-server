package ru.kuznetsov.translator.value.user.instruction.callBase

import org.llvm.Instruction
import org.llvm.Location
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.instruction.callBase.CallInst.translateCallInstToExpression
import ru.kuznetsov.translator.value.user.instruction.callBase.InvokeInst.translateInvokeInstToInstruction

object CallBase {
  fun TranslatorState.translateCallBaseToExpression(callBase: ValueOuterClass.CallBase): List<Instruction> {
    if (callBase.hasCallBrInst()) {
      throw IllegalStateException("Can't parse CallBrInst")
    }
    if (callBase.hasCallInst()) {
      return translateCallInstToExpression(callBase.callInst)
    }
    if (callBase.hasCgStatePointInst()) {
      throw IllegalStateException("Can't parse CGStatepointInst")
    }
    if (callBase.hasInvokeInst()) {
      return translateInvokeInstToInstruction(callBase.invokeInst)
    }
    throw IllegalStateException("Can't find candidate for CallBase: $callBase")
  }
}