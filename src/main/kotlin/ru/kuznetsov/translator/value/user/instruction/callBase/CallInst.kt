package ru.kuznetsov.translator.value.user.instruction.callBase

import org.llvm.Expression
import org.llvm.Instruction
import org.llvm.Method
import proto.value.ValueOuterClass
import proto.value.ValueOuterClass.CallInst.CallInstClassic
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier
import ru.kuznetsov.translator.value.argument.Argument.translateArgumentsAsStrings
import ru.kuznetsov.translator.value.user.instruction.callBase.binaryOp.BinaryOpIntrinsic.translateBinaryOpIntrinsicToInstruction

object CallInst {
  fun getNameOfFunctionOrFake(function: ValueOuterClass.Function): String {
    return if (function.parameterList.any { it.nonNull }) {
      function.name + "_nonnull_call" // todo solve conflicts
    } else {
      function.name
    }
  }
  
  fun TranslatorState.translateClassicCall(callInstClassic: CallInstClassic): List<Instruction> {
    val returnedExpression: Expression = if (callInstClassic.hasFunction()) {
      val parameters = translateArgumentsAsStrings(true, callInstClassic.function.parameterList)
      val returnType = translateType(callInstClassic.function.returnType).toString()
      Expression.ConstExpression.FunctionConst(
        constValue = Method.RegularMethod(
          methodName = getNameOfFunctionOrFake(callInstClassic.function),
          parameters = parameters,
          className = "",
          returnType = returnType,
          isStatic = true // TODO
        )
      )
    } else if (callInstClassic.hasCalledFunction()) {
      val name = kotlin.runCatching { translateValueAsIdentifier(callInstClassic.calledFunction).name }
        .getOrElse { callInstClassic.calledFunction.argument.name }
      if (name.isBlank()) throw IllegalArgumentException("Can't take name from called function")
      Expression.ConstExpression.FunctionConst(
        constValue = Method.RegularMethod(
          methodName = name,
          parameters = listOf(),
          className = "",
          returnType = "void",
          isStatic = true
        )
      )
    } else {
      throw IllegalStateException("Either called function or function must present")
    }
    return listOf(Instruction.Call(
      location = currentLocation!!.toLocation(),
      returnValue = translateValueAsIdentifier(callInstClassic.result),
      returnType = translateType(callInstClassic.result.type),
      functionExpression = returnedExpression,
      arguments = callInstClassic.argumentsList.map { Instruction.Call.Argument(translateValueAsExpression(it), translateType(it.type)) },
      flags = Instruction.Call.Flags(
        virtual = false,
        isObjectBlock = false
      )
    ))
  }
  
  fun TranslatorState.translateCallInstToExpression(callBase: ValueOuterClass.CallInst): List<Instruction> {
    if (callBase.hasCallInstClassic()) {
      return translateClassicCall(callBase.callInstClassic)
    }
    if (callBase.hasIntrinsicInst()) {
      val intrinsicInst = callBase.intrinsicInst
      if (intrinsicInst.hasInstrProfInstBase()) {
        return emptyList()
      }
      if (intrinsicInst.hasDbgInfoIntrinsic()) {
        return emptyList()
      }
      if (intrinsicInst.hasBinaryOpIntrinsic()) {
        return translateBinaryOpIntrinsicToInstruction(intrinsicInst.binaryOpIntrinsic)
      }
      throw IllegalStateException("Can't parse intrinsic: $intrinsicInst")
    }
    throw IllegalStateException("Can't parse: $callBase")
  }
}