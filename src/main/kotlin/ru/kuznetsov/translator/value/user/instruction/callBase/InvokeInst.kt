package ru.kuznetsov.translator.value.user.instruction.callBase

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Method
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.argument.Argument.translateArgumentsAsStrings
import ru.kuznetsov.translator.value.user.instruction.callBase.CallInst.getNameOfFunctionOrFake

object InvokeInst {
  fun TranslatorState.translateInvokeInstToInstruction(invokeInst: ValueOuterClass.InvokeInst): List<Instruction> {
    val currentBlock: ValueOuterClass.BasicBlock = getContext(TranslatorState.CURRENT_BLOCK)
    return listOf(
      Instruction.Call(
        location = currentLocation!!.toLocation(),
        returnValue = Identifier(
          kind = Identifier.Kind.NORMAL,
          name = "exception%${currentBlock.name}",
          stamp = 0
        ),
        returnType = translateType(invokeInst.result.type),
        functionExpression = Expression.ConstExpression.FunctionConst(
          constValue = Method.RegularMethod(
            methodName = getNameOfFunctionOrFake(invokeInst.function),
            parameters = translateArgumentsAsStrings(true, invokeInst.function.parameterList),
            className = "",
            returnType = translateType(invokeInst.function.returnType).toString(),
            isStatic = true // TODO
          )
        ),
        arguments = invokeInst.argumentsList.map { Instruction.Call.Argument(translateValueAsExpression(it), translateType(it.type)) },
        flags = Instruction.Call.Flags(
          virtual = false,
          isObjectBlock = false
        )
      )
    )
  }
}