package ru.kuznetsov.translator.value.user.instruction.callBase.binaryOp

import org.llvm.Instruction
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.instruction.callBase.binaryOp.SaturatingInst.translateSaturatingInstToInstruction

object BinaryOpIntrinsic {
  fun TranslatorState.translateBinaryOpIntrinsicToInstruction(binaryOpIntrinsic: ValueOuterClass.CallInst.IntrinsicInst.BinaryOpIntrinsic): List<Instruction> {
    if (binaryOpIntrinsic.hasSaturatingInst()) {
      return translateSaturatingInstToInstruction(binaryOpIntrinsic.saturatingInst)
    }
    throw IllegalArgumentException("Failed to parse binary op: $binaryOpIntrinsic")
  }
}