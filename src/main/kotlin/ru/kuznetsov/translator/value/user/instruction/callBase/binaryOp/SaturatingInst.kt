package ru.kuznetsov.translator.value.user.instruction.callBase.binaryOp

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object SaturatingInst {
  fun TranslatorState.translateSaturatingInstToInstruction(saturatingInst: ValueOuterClass.CallInst.IntrinsicInst.BinaryOpIntrinsic.SaturatingInst): List<Instruction> {
    val inferOperator = when (saturatingInst.operation) {
      ValueOuterClass.CallInst.IntrinsicInst.BinaryOpIntrinsic.SaturatingInst.Operation.U_ADD -> Expression.BinaryOperationExpression.Operator.PLUSPI
      ValueOuterClass.CallInst.IntrinsicInst.BinaryOpIntrinsic.SaturatingInst.Operation.S_ADD -> Expression.BinaryOperationExpression.Operator.PLUSPI
      ValueOuterClass.CallInst.IntrinsicInst.BinaryOpIntrinsic.SaturatingInst.Operation.U_SUB -> Expression.BinaryOperationExpression.Operator.MINUSPI
      ValueOuterClass.CallInst.IntrinsicInst.BinaryOpIntrinsic.SaturatingInst.Operation.S_SUB -> Expression.BinaryOperationExpression.Operator.MINUSPI
      else -> throw IllegalArgumentException("Can't find operation: ${saturatingInst.operation}")
    }
    
    val resultId = translateValueAsIdentifier(saturatingInst.result)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    val type = translateType(saturatingInst.result.type)

    return listOf(
      Instruction.Store(
        location = currentLocation!!.toLocation(),
        lvalue = Expression.VariableExpression(
          identifier = temporaryId
        ),
        rvalue = Expression.BinaryOperationExpression(
          operator = inferOperator,
          left = translateValueAsExpression(saturatingInst.l),
          right = translateValueAsExpression(saturatingInst.r),
        ),
        type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = type),
      ),
      Instruction.Load(
        location = currentLocation.toLocation(),
        identifier = resultId,
        expression = Expression.VariableExpression(
          identifier = temporaryId
        ),
        type = type,
      )
    )
  }
}