package ru.kuznetsov.translator.value.user.instruction.unaryinst

import org.llvm.Expression
import org.llvm.Instruction
import org.llvm.Method
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object AllocaInst {
  fun TranslatorState.translateAllocaInstToInstruction(allocaInst: ValueOuterClass.AllocaInst): List<Instruction> {
    if (currentType is Type.Ptr) {
      return when (val subtype = currentType.type) {
        is Type.Struct -> {
          listOf(
            Instruction.Call(
              location = currentLocation!!.toLocation(),
              returnValue = translateValueAsIdentifier(allocaInst.to),
              returnType = subtype,
              functionExpression = Expression.ConstExpression.FunctionConst(
                constValue = Method.RegularMethod(
                  methodName = "__new",
                  parameters = listOf(),
                  className = "BuiltIn",
                  returnType = "",
                  isStatic = false
                )
              ),
              arguments = listOf(
                Instruction.Call.Argument(
                  expression = Expression.SizeOfExpression(
                    kind = Expression.SizeOfExpression.Kind.EXACT,
                    dynamicLength = null,
                    type = subtype
                  ),
                  type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = subtype)
                )
              ),
              flags = Instruction.Call.Flags(
                virtual = false,
                isObjectBlock = false
              )
            )
          )
        }

        is Type.Array -> {
          check(currentUnsafeType!!.hasPointerType()) { "Must be pointer: $currentUnsafeType" }
          val subUnsafeType = currentUnsafeType.pointerType.subtypeList[0]
          val count : Int = if (subUnsafeType.hasArrayType()) {
            subUnsafeType.arrayType.count
          } else if (subUnsafeType.hasVectorType()) {
            subUnsafeType.vectorType.size
          } else {
            throw IllegalArgumentException("Must by array in pointer: $subUnsafeType")
          }
          listOf(
            Instruction.Call(
              location = currentLocation!!.toLocation(),
              returnValue = translateValueAsIdentifier(allocaInst.to),
              returnType = subtype,
              functionExpression = Expression.ConstExpression.FunctionConst(
                constValue = Method.RegularMethod(
                  methodName = "__new_array",
                  parameters = listOf(),
                  className = "BuiltIn",
                  returnType = "",
                  isStatic = false
                )
              ),
              arguments = listOf(
                Instruction.Call.Argument(
                  expression = Expression.SizeOfExpression(
                    kind = Expression.SizeOfExpression.Kind.EXACT,
                    dynamicLength = Expression.ConstExpression.IntConst(
                      constValue = Expression.ConstExpression.IntConst.SubValue(
                        value = count.toLong(),
                        isPointer = false
                      )
                    ),
                    type = subtype
                  ),
                  type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = subtype)
                )
              ),
              flags = Instruction.Call.Flags(
                virtual = false,
                isObjectBlock = false
              )
            )
          )
        }

        else -> listOf()
      }
    } else {
      return listOf()
    }
  }
}