package ru.kuznetsov.translator.value.user.instruction.unaryinst

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier
import ru.kuznetsov.translator.value.user.instruction.GetElementPtrInst.translateSubExpression

object ExtractValueInst {
  fun TranslatorState.translateExtractValueInstToExpression(extractValueInst: ValueOuterClass.UnaryInstruction.ExtractValueInst): Expression {
    val type = translateType(extractValueInst.targetType)
    val operands = extractValueInst.operandList
    return translateSubExpression(type, operands.iterator(), translateValueAsExpression(extractValueInst.target))
  }
  
  fun TranslatorState.translateExtractValueInstToInstruction(extractValueInst: ValueOuterClass.UnaryInstruction.ExtractValueInst): List<Instruction> {
    val resultId = translateValueAsIdentifier(extractValueInst.result)
    val temporaryId = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = resultId.name,
      stamp = 1
    )
    val resultType = translateType(extractValueInst.result.type)
    return listOf(Instruction.Store(
      location = currentLocation!!.toLocation(),
      lvalue = Expression.VariableExpression(identifier = temporaryId),
      rvalue = translateExtractValueInstToExpression(extractValueInst),
      type = Type.Ptr(
        kind = Type.Ptr.Kind.PK_POINTER,
        type = resultType
      )
    ), Instruction.Load(
      location = currentLocation.toLocation(),
      identifier = resultId,
      expression = Expression.VariableExpression(identifier = temporaryId),
      type = resultType
    ))
  }
}