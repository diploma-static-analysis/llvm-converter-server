package ru.kuznetsov.translator.value.user.instruction.unaryinst

import org.llvm.Instruction
import org.llvm.Location
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object LoadInst {
  fun TranslatorState.translateLoadInstToInstruction(loadInst: ValueOuterClass.LoadInst): List<Instruction> {
    return listOf(
      Instruction.Load(
        location = currentLocation!!.toLocation(),
        identifier = translateValueAsIdentifier(loadInst.to),
        expression = translateValueAsExpression(loadInst.from),
        type = translateType(loadInst.to.type)
      )
    )
  }
}