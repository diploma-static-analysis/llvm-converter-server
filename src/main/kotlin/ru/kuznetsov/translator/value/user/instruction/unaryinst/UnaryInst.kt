package ru.kuznetsov.translator.value.user.instruction.unaryinst

import org.llvm.Expression
import org.llvm.Instruction
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.value.user.instruction.unaryinst.AllocaInst.translateAllocaInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.unaryinst.ExtractValueInst.translateExtractValueInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.unaryinst.LoadInst.translateLoadInstToInstruction
import ru.kuznetsov.translator.value.user.instruction.unaryinst.castinst.CastInst.translateCastInstToExpression
import ru.kuznetsov.translator.value.user.instruction.unaryinst.castinst.CastInst.translateCastInstToInstruction

object UnaryInst {
  fun TranslatorState.translateUnaryInstAsInstruction(unaryInst: ValueOuterClass.UnaryInstruction): List<Instruction> {
    if (unaryInst.hasLoadInst()) {
      return translateLoadInstToInstruction(unaryInst.loadInst)
    }
    if (unaryInst.hasAllocaInst()) {
      return translateAllocaInstToInstruction(unaryInst.allocaInst)
    }
    if (unaryInst.hasCastInst()) {
      return translateCastInstToInstruction(unaryInst.castInst)
    }
    if (unaryInst.hasExtractValueInst()) {
      return translateExtractValueInstToInstruction(unaryInst.extractValueInst)
    }
    if (unaryInst.hasFreezeInst()) {
      return emptyList()
    }
    throw IllegalStateException("Failed to parse unaryInst: $unaryInst")
  }
  
  fun TranslatorState.translateUnaryInstAsExpression(unaryInst: ValueOuterClass.UnaryInstruction): Expression {
    if (unaryInst.hasCastInst()) {
      return translateCastInstToExpression(unaryInst.castInst)
    }
    throw IllegalStateException("Failed to parse unaryInst: $unaryInst")
  }
}