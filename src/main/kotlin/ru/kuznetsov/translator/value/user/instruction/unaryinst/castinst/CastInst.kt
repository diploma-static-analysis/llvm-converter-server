package ru.kuznetsov.translator.value.user.instruction.unaryinst.castinst

import org.llvm.Expression
import org.llvm.Identifier
import org.llvm.Instruction
import org.llvm.Type
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.type.Type.translateType
import ru.kuznetsov.translator.value.Value.translateValueAsExpression
import ru.kuznetsov.translator.value.Value.translateValueAsIdentifier

object CastInst {
  fun TranslatorState.translateCastInstToInstruction(castInst: ValueOuterClass.CastInst): List<Instruction> {
    val identifier = translateValueAsIdentifier(castInst.to)
    val temporaryIdentifier = Identifier(
      kind = Identifier.Kind.NORMAL,
      name = identifier.name,
      stamp = 1
    )
    val resultType = translateType(castInst.toType)
    return listOf(
      Instruction.Store(
        location = currentLocation!!.toLocation(),
        lvalue = Expression.VariableExpression(
          identifier = temporaryIdentifier
        ),
        rvalue = translateCastInstToExpression(castInst),
        type = Type.Ptr(kind = Type.Ptr.Kind.PK_POINTER, type = resultType),
      ),
      Instruction.Load(
        location = currentLocation.toLocation(),
        identifier = identifier,
        expression = Expression.VariableExpression(
          identifier = temporaryIdentifier
        ),
        type = resultType,
      )
    )
  }
  
  fun TranslatorState.translateCastInstToExpression(castInst: ValueOuterClass.CastInst): Expression {
    return Expression.CastExpression(
      type = translateType(castInst.toType),
      expression = translateValueAsExpression(castInst.from)
    )
  }
}