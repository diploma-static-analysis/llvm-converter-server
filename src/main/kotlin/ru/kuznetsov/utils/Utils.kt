package ru.kuznetsov.utils

import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit

object Utils {
  fun String.runCommand(workingDir: File): String? {
    println("Starting $this")
    try {
      val parts = this.split("\\s".toRegex())
      val proc = ProcessBuilder(*parts.toTypedArray())
        .apply {
          val env = this.environment()
          env["DOTNET_ROOT"] = "/home/vladislavkuznetsov/.dotnet/"
        }
        .directory(workingDir)
        .redirectOutput(ProcessBuilder.Redirect.PIPE)
        .redirectError(ProcessBuilder.Redirect.PIPE)
        .start()

      proc.waitFor(60, TimeUnit.MINUTES)
      val errorOutput = proc.errorStream.bufferedReader().readText()
      println(errorOutput)
      if (proc.exitValue() != 0) {
        println(proc.inputStream.bufferedReader().readText())
        throw Exception("Command failed with code ${proc.exitValue()}")
      }
      return proc.inputStream.bufferedReader().readText()
    } catch(e: IOException) {
      e.printStackTrace()
      return null
    }
  }

  fun findLineNumber(text: String, target: String): Int {
    val lineTarget = target.split("\n")[0]
    val lines = text.split("\n")
    lines.forEachIndexed { index, line ->
      if (line.contains(lineTarget)) {
        return index
      }
    }
    return -1
  }
  
  fun findFirstNotWhiteSpaceOfLine(text: String, lineNumber: Int): Int {
    if (lineNumber == -1) return -1 // TODO
    val lines = text.split("\n")
    val line = lines[lineNumber]
    line.forEachIndexed { index, c ->
      if (!c.isWhitespace()) {
        return index
      }
    }
    return -1
  }
}