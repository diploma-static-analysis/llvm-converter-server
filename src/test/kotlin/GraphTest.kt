import com.google.protobuf.TextFormat
import org.llvm.Method
import proto.value.*
import proto.value.InstructionKt.BranchInstKt.conditionalBranchInst
import proto.value.InstructionKt.BranchInstKt.unconditionalBranchInst
import proto.value.InstructionKt.branchInst
import ru.kuznetsov.translator.TranslatorState
import ru.kuznetsov.translator.graph.Graph
import kotlin.io.path.Path
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class GraphTest {
  
  private fun (BasicBlockKt.Dsl).withReturn(name: String) {
    val instruction = instruction {
      returnInst = returnInst {
        return_ = value {
          this.name = name
        }
      }
    }
    instructions.add(instruction)
    terminator = instruction
  }
  
  fun (BasicBlockKt.Dsl).withUnconditionalBranch(name: String) {
    val instruction = instruction {
      branchInst = branchInst {
        unconditionalBranchInst = unconditionalBranchInst {
          basicBlockName = name
        }
      }
    }
    instructions.add(instruction)
    terminator = instruction
  }
  
  fun (BasicBlockKt.Dsl).withConditionalBranch(value: String, ifTrue: String, ifFalse: String) {
    val instruction = instruction {
      branchInst = branchInst {
        conditionalBranchInst = conditionalBranchInst {
          condition = value {
            name = value
          }
          basicBlockTrueBranch = ifTrue
          basicBlockFalseBranch = ifFalse
        }
      }
    }
    instructions.add(instruction)
    terminator = instruction
  }
  
  private fun getModuleFromFile(filename: String): ValueOuterClass.Module {
    val builder = ValueOuterClass.Module.newBuilder()
    TextFormat.getParser().merge(GraphTest::class.java.getResourceAsStream("/${Path("graph", filename, "input.protoraw")}")!!.bufferedReader().readText(), builder)
    return builder.build()
  }

  private fun List<Pair<Int,Int>>.sorted() : List<Pair<Int,Int>> =
    sortedWith(
      java.util.Comparator<Pair<Int, Int>> { a, b -> a.first.compareTo(b.first) }.thenBy { it.second }
    )
  
  private fun Graph.testGraph() {
    val graphNodes = this.nodes()
    val fromToEdges = graphNodes.flatMap { node -> node.nextNodes.map { Pair(node.id, it) } }
    val toFromEdges = graphNodes.flatMap { node -> node.previousNodes.map { Pair(it, node.id) } }
    assertEquals(fromToEdges, fromToEdges.distinct())
    assertEquals(toFromEdges, toFromEdges.distinct())
    assertEquals(toFromEdges.sorted(), fromToEdges.sorted())
  }
  
  private fun Graph.assertGraph(nodes: List<Pair<String, String>>, fromStart: List<String>, toEnd: List<String>) {
    testGraph()
    val graphNodes = this.nodes()
    val nodesByName = graphNodes.filterIsInstance<Graph.StatementNode>().associateBy { it.name }
    val nodesById = graphNodes.associateBy { it.id }
    val startNode = graphNodes.single { it is Graph.StartNode }
    val finishNode = graphNodes.single { it is Graph.FinishNode }
    
    fun List<Int>.asNodes(): List<Graph.Node> = map { nodesById[it]!! }
    
    assertEquals((nodes.map { it.first } + nodes.map { it.second }).sorted().distinct(), nodesByName.keys.sorted())
    nodesByName.keys.forEach { nodeName ->
      val node = nodesByName[nodeName] as Graph.StatementNode
      val graphFrom = node.nextNodes.asNodes().map { if (it is Graph.PruneNode) it.nextNodes.asNodes().single() else it }.filterIsInstance<Graph.StatementNode>().map { it.name }
      val graphTo = node.previousNodes.asNodes().map { if (it is Graph.PruneNode) it.previousNodes.asNodes().single() else it }.filterIsInstance<Graph.StatementNode>().map { it.name }

      assertEquals(nodes.filter { it.second == nodeName }.map { it.first }.sorted(), graphTo.sorted(), "On check previous node with name ${node.name}")
      assertEquals(nodes.filter { it.first == nodeName }.map { it.second }.sorted(), graphFrom.sorted(), "On check next node with name ${node.name}")
      
      if (fromStart.contains(node.name)) {
        assertTrue("Node $nodeName should contain start node") { node.previousNodes.contains(startNode.id) }
      } else {
        assertFalse("Node $nodeName shouldn't contain start node") { node.previousNodes.contains(startNode.id) }
      }
      if (toEnd.contains(node.name)) {
        assertTrue("Node $nodeName should contain end node") { node.nextNodes.contains(finishNode.id) }
      } else {
        assertFalse("Node $nodeName shouldn't contain end node") { node.nextNodes.contains(finishNode.id) }
      }
    }
    nodesById.values.forEach { assertTrue("Exit node should be exit node: node ${it.id}") { it.exitNode == finishNode.id || it.id == finishNode.id } }
  }
  
  @Test
  fun `simple graph test`() {
    val basicBlock = basicBlock {
      withReturn("0")
    }
    val method = Method.RegularMethod(
      "",
      emptyList(),
      "",
      null,
      true
    )
    val translatorState = TranslatorState("test.txt", basicBlock.toString())
    val graph = Graph(0, translatorState, method, emptyList(),  listOf(basicBlock))
    val nodes = graph.nodes()
    assertEquals(3, nodes.size)
  }
  
  @Test
  fun `fibb graph test`() {
    val module = getModuleFromFile("fibb")
    val blocks = module.functionsList[0].basicBlocksList
    val method = Method.RegularMethod(
      "",
      emptyList(),
      "",
      null,
      true
    )
    val translatorState = TranslatorState("fibb/fibb.ll", module.toString())
    val graph = Graph(0, translatorState, method, emptyList(), blocks)
    val fromStart = listOf("%0")
    val toEnd = listOf("%28")
    val graphEdges = listOf(
      "%0" to "%7",
      "%7" to "%11",
      "%7" to "%28",
      "%11" to "%14",
      "%11" to "%16",
      "%14" to "%22",
      "%16" to "%22",
      "%22" to "%25",
      "%25" to "%7"
    )
    graph.translator(translatorState)
    graph.assertGraph(graphEdges, fromStart, toEnd)
  }
}