import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.google.protobuf.TextFormat
import org.llvm.Configuration
import org.skyscreamer.jsonassert.JSONAssert
import proto.value.ValueOuterClass
import ru.kuznetsov.translator.FileData
import ru.kuznetsov.translator.Module
import ru.kuznetsov.utils.Utils.runCommand
import java.nio.file.Files
import kotlin.io.path.Path
import kotlin.io.path.name
import kotlin.io.path.outputStream
import kotlin.io.path.readText
import kotlin.test.Test

class TranslatorTest {

  fun doTest(name: String) {
    val testDirectory = Path(TranslatorTest::class.java.getResource("/translator")!!.path, name)
    val input = testDirectory.resolve("input.protoraw")
    val responseBuilder = ValueOuterClass.Module.newBuilder()
    TextFormat.getParser().merge(input.readText(), responseBuilder)
    val response = responseBuilder.build()
    val objectMapper = ObjectMapper()
      .setDefaultPrettyPrinter(DefaultPrettyPrinter())
      .registerKotlinModule()

    val tempPath = Files.createTempDirectory("llvm-converter-test-$name")
    val tenvPath = tempPath.resolve("output-tenv-example.json")
    val cfgPath = tempPath.resolve("output-cfg-example.json")
    
    val writer = objectMapper.writerWithDefaultPrettyPrinter().with(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
    val state = Module.translateModule(listOf(FileData("test.txt", testDirectory.resolve("program.ll").readText(), response)))
    writer.writeValue(tenvPath.outputStream(), state.classDescriptions.map { it.value.toClassDescription() })
    writer.writeValue(cfgPath.outputStream(), Configuration(state.procs, state.nodes))
    
    val command = "/home/vladislavkuznetsov/Vl/infer/infer/infer/bin/infer run " +
            "--biabduction " +
            "-g " +
            "--procedures-summary-json " +
            "--debug " +
            "--sarif " +
            "--pmd-xml " +
            "--cfg-json ${cfgPath.name} " +
            "--tenv-json ${tenvPath.name}"

    command.runCommand(tempPath.toFile())!!
    
    val reportJson = tempPath.resolve("infer-out").resolve("report.json").readText()
    try {
      JSONAssert.assertEquals(testDirectory.resolve("gold.json").readText(), reportJson, true)
    } catch (e: AssertionError) {
      JSONAssert.assertEquals(testDirectory.resolve("golden-tenv.json").readText(), tenvPath.readText(), true)
      JSONAssert.assertEquals(testDirectory.resolve("golden-cfg.json").readText(), cfgPath.readText(), true)
      throw e
    }
  }
  
  @Test
  fun `empty main`() = doTest("empty_main")
  @Test
  fun `get argument from function not_ok`() = doTest("get_argument_from_function_not_ok")
  @Test
  fun `get argument from function ok`() = doTest("get_argument_from_function_ok")
  @Test
  fun `get straight from function`() = doTest("get_straight_from_function")
  @Test
  fun `if with true branch dereference`() = doTest("if_with_true_branch_dereference")
  @Test
  fun `if with true branch ok`() = doTest("if_with_true_branch_ok")
  @Test
  fun `straight dereference null`() = doTest("straight_dereference_null")
  @Test
  fun `straight dereference null deep`() = doTest("straight_dereference_null_deep")
  @Test
  fun `straight dereference null deep with dereference`() = doTest("straight_dereference_null_deep_with_dereference")
  @Test
  fun sum() = doTest("sum")
  @Test
  fun `simple ok struct`() = doTest("simple_ok_struct")
  @Test
  fun `null pointer by field struct`() = doTest("null_pointer_by_field_struct")
  @Test
  fun `chain field null structure`() = doTest("chain_field_null_struct")
  @Test
  fun `return struct with null from function`() = doTest("return_struct_with_null_from_function")
  @Test
  fun `return struct with ok field`() = doTest("return_struct_with_ok_field")
  @Test
  fun `return struct with not null with if`() = doTest("return_struct_with_not_null_with_if")
  @Test
  fun `return struct with null with if`() = doTest("return_struct_with_null_with_if")
  @Test
  fun `calling function in string`() = doTest("calling_function_in_string")
  @Test
  fun `big test crypto fail`() = doTest("big_test_crypto_fail")
  @Test
  fun `big test crypto ok`() = doTest("big_test_crypto_ok")
}