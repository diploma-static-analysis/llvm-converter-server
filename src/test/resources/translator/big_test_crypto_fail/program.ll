; ModuleID = 'program.cpp'
source_filename = "program.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::map" = type { %"class.std::_Rb_tree" }
%"class.std::_Rb_tree" = type { %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl" }
%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl" = type { %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_header" }
%"struct.std::_Rb_tree_key_compare" = type { %"struct.std::less" }
%"struct.std::less" = type { i8 }
%"struct.std::_Rb_tree_header" = type { %"struct.std::_Rb_tree_node_base", i64 }
%"struct.std::_Rb_tree_node_base" = type { i32, %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }
%"struct.std::pair" = type { i8, double }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i64 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], %struct.__locale_struct*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%"struct.std::piecewise_construct_t" = type { i8 }
%"class.std::initializer_list" = type { %"struct.std::pair"*, i64 }
%"class.std::allocator.0" = type { i8 }
%"class.std::__new_allocator.1" = type { i8 }
%"struct.std::pair.3" = type { i8, double }
%"struct.std::_Rb_tree_const_iterator" = type { %"struct.std::_Rb_tree_node_base"* }
%class.anon = type { i8 }
%"struct.__gnu_cxx::__ops::_Iter_comp_iter" = type { %class.anon }
%"struct.std::_Rb_tree_node" = type { %"struct.std::_Rb_tree_node_base", %"struct.__gnu_cxx::__aligned_membuf" }
%"struct.__gnu_cxx::__aligned_membuf" = type { [16 x i8] }
%"class.std::__pair_base.4" = type { i8 }
%"class.std::basic_ifstream" = type { %"class.std::basic_istream.base", %"class.std::basic_filebuf", %"class.std::basic_ios" }
%"class.std::basic_istream.base" = type { i32 (...)**, i64 }
%"class.std::basic_filebuf" = type { %"class.std::basic_streambuf", %union.pthread_mutex_t, %"class.std::__basic_file", i32, %struct.__mbstate_t, %struct.__mbstate_t, %struct.__mbstate_t, i8*, i64, i8, i8, i8, i8, i8*, i8*, i8, %"class.std::codecvt"*, i8*, i64, i8*, i8* }
%union.pthread_mutex_t = type { %struct.__pthread_mutex_s }
%struct.__pthread_mutex_s = type { i32, i32, i32, i32, i32, i16, i16, %struct.__pthread_internal_list }
%struct.__pthread_internal_list = type { %struct.__pthread_internal_list*, %struct.__pthread_internal_list* }
%"class.std::__basic_file" = type <{ %struct._IO_FILE*, i8, [7 x i8] }>
%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, %struct._IO_codecvt*, %struct._IO_wide_data*, %struct._IO_FILE*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type opaque
%struct._IO_codecvt = type opaque
%struct._IO_wide_data = type opaque
%struct.__mbstate_t = type { i32, %union.anon }
%union.anon = type { i32 }
%"class.std::codecvt" = type { %"class.std::__codecvt_abstract_base.base", %struct.__locale_struct* }
%"class.std::__codecvt_abstract_base.base" = type { %"class.std::locale::facet.base" }
%"class.std::__cxx11::basic_string" = type { %"struct.std::__cxx11::basic_string<char>::_Alloc_hider", i64, %union.anon.9 }
%"struct.std::__cxx11::basic_string<char>::_Alloc_hider" = type { i8* }
%union.anon.9 = type { i64, [8 x i8] }
%"class.std::map.10" = type { %"class.std::_Rb_tree.11" }
%"class.std::_Rb_tree.11" = type { %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl" }
%"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl" = type { %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_header" }
%"class.std::basic_istream" = type { i32 (...)**, i64, %"class.std::basic_ios" }
%"struct.std::_Rb_tree_iterator" = type { %"struct.std::_Rb_tree_node_base"* }
%"class.std::tuple" = type { %"struct.std::_Tuple_impl" }
%"struct.std::_Tuple_impl" = type { %"struct.std::_Head_base" }
%"struct.std::_Head_base" = type { i8* }
%"class.std::tuple.23" = type { i8 }
%"class.std::tuple.25" = type { %"struct.std::_Tuple_impl.26" }
%"struct.std::_Tuple_impl.26" = type { %"struct.std::_Head_base.27" }
%"struct.std::_Head_base.27" = type { i8* }
%"class.std::allocator" = type { i8 }
%"class.std::__new_allocator" = type { i8 }
%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node" = type { %"class.std::_Rb_tree"* }
%"class.std::allocator.12" = type { i8 }
%"class.std::__new_allocator.13" = type { i8 }
%"struct.std::_Rb_tree_node.15" = type <{ %"struct.std::_Rb_tree_node_base", %"struct.__gnu_cxx::__aligned_membuf.16", [6 x i8] }>
%"struct.__gnu_cxx::__aligned_membuf.16" = type { [2 x i8] }
%"struct.std::pair.17" = type { i8, i8 }
%"struct.std::pair.20" = type { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }
%"struct.std::_Select1st" = type { i8 }
%"class.std::__pair_base.21" = type { i8 }
%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node" = type { %"class.std::_Rb_tree"*, %"struct.std::_Rb_tree_node"* }
%"struct.std::_Index_tuple" = type { i8 }
%"struct.std::_Index_tuple.24" = type { i8 }
%"class.std::__pair_base" = type { i8 }

$_ZNSaISt4pairIKcdEEC2Ev = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2ESt16initializer_listIS4_ERKS1_RKS5_ = comdat any

$_ZNSaISt4pairIKcdEED2Ev = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev = comdat any

$_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE5beginEv = comdat any

$_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv = comdat any

$_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEEdeEv = comdat any

$_ZNSt4pairIcdEC2IKcdLb1EEERKS_IT_T0_E = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2ERKS6_ = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2Ev = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixERS3_ = comdat any

$_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEEC2Ev = comdat any

$_ZSt3absd = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixEOc = comdat any

$_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEED2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev = comdat any

$__clang_call_terminate = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE15_M_destroy_nodeEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE7destroyIS3_EEvRS5_PT_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv = comdat any

$_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE7destroyIS3_EEvPT_ = comdat any

$_ZN9__gnu_cxx16__aligned_membufISt4pairIKcdEE6_M_ptrEv = comdat any

$_ZN9__gnu_cxx16__aligned_membufISt4pairIKcdEE7_M_addrEv = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE10deallocateERS5_PS4_m = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE10deallocateEPS4_m = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev = comdat any

$_ZSteqRKSt23_Rb_tree_const_iteratorISt4pairIKcdEES5_ = comdat any

$_ZStneRKSt23_Rb_tree_const_iteratorISt4pairIKcdEES5_ = comdat any

$_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEppEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2ERKS8_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2ERKSA_ = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_rootEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyERKS8_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_rootEv = comdat any

$_ZN9__gnu_cxx14__alloc_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEES5_E17_S_select_on_copyERKS6_ = comdat any

$_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev = comdat any

$_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2ERKS1_ = comdat any

$_ZNSt15_Rb_tree_headerC2Ev = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE37select_on_container_copy_constructionERKS5_ = comdat any

$_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS4_ = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS5_ = comdat any

$_ZNSt15_Rb_tree_header8_M_resetEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeC2ERS8_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ERKS8_RT0_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_PSt18_Rb_tree_node_baseRT0_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_leftmostEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_M_clone_nodeILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_RT0_ = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeclIRKS2_EEPSt13_Rb_tree_nodeIS2_EOT_ = comdat any

$_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt13_Rb_tree_nodeIS2_EDpOT_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_get_nodeEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKS2_EEEvPSt13_Rb_tree_nodeIS2_EDpOT_ = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE8allocateERS5_m = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE8allocateEmPKv = comdat any

$_ZNKSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE11_M_max_sizeEv = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKS3_EEEvRS5_PT_DpOT0_ = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKS3_EEEvPT_DpOT0_ = comdat any

$_ZNSt18_Rb_tree_node_base10_S_minimumEPS_ = comdat any

$_ZNSt18_Rb_tree_node_base10_S_maximumEPS_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2Ev = comdat any

$_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev = comdat any

$_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2Ev = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2Ev = comdat any

$_ZNSaISt13_Rb_tree_nodeISt4pairIKccEEEC2Ev = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEEC2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE15_M_destroy_nodeEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKccEEEE7destroyIS3_EEvRS5_PT_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv = comdat any

$_ZNSt13_Rb_tree_nodeISt4pairIKccEE9_M_valptrEv = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEE7destroyIS3_EEvPT_ = comdat any

$_ZN9__gnu_cxx16__aligned_membufISt4pairIKccEE6_M_ptrEv = comdat any

$_ZN9__gnu_cxx16__aligned_membufISt4pairIKccEE7_M_addrEv = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKccEEEE10deallocateERS5_PS4_m = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEE10deallocateEPS4_m = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv = comdat any

$_ZNSaISt13_Rb_tree_nodeISt4pairIKccEEED2Ev = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEED2Ev = comdat any

$_ZNSt15__new_allocatorISt4pairIKcdEEC2Ev = comdat any

$_ZNSt15__new_allocatorISt4pairIKcdEED2Ev = comdat any

$_ZNSaISt4pairIKcdEEC2ERKS2_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2ERKS6_RKS7_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_insert_range_uniqueIPKS2_EENSt9enable_ifIXsr17__same_value_typeIT_EE5valueEvE4typeESD_SD_ = comdat any

$_ZNKSt16initializer_listISt4pairIKcdEE5beginEv = comdat any

$_ZNKSt16initializer_listISt4pairIKcdEE3endEv = comdat any

$_ZNSt15__new_allocatorISt4pairIKcdEEC2ERKS3_ = comdat any

$_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2IS2_EERKSaIT_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2ERKS6_OSaISt13_Rb_tree_nodeIS2_EE = comdat any

$_ZSt4moveIRSaISt13_Rb_tree_nodeISt4pairIKcdEEEEONSt16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_insert_unique_IRKS2_NS8_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EOT_RT0_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv = comdat any

$_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2ERKSt17_Rb_tree_iteratorIS2_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_ = comdat any

$_ZNKSt10_Select1stISt4pairIKcdEEclERKS2_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_M_insert_IRKS2_NS8_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS2_EPSt18_Rb_tree_node_baseSG_OT_RT0_ = comdat any

$_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base = comdat any

$_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEE13_M_const_castEv = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE4sizeEv = comdat any

$_ZNKSt4lessIcEclERKcS2_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base = comdat any

$_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE24_M_get_insert_unique_posERS1_ = comdat any

$_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRS1_S4_Lb1EEEOT_OT0_ = comdat any

$_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEmmEv = comdat any

$_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEppEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNKSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv = comdat any

$_ZNK9__gnu_cxx16__aligned_membufISt4pairIKcdEE6_M_ptrEv = comdat any

$_ZNK9__gnu_cxx16__aligned_membufISt4pairIKcdEE7_M_addrEv = comdat any

$_ZSteqRKSt17_Rb_tree_iteratorISt4pairIKcdEES5_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE5beginEv = comdat any

$_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRPSt13_Rb_tree_nodeIS_IKcdEERS1_Lb1EEEOT_OT0_ = comdat any

$_ZSt7forwardIRPSt13_Rb_tree_nodeISt4pairIKcdEEEOT_RNSt16remove_referenceIS7_E4typeE = comdat any

$_ZSt7forwardIRPSt18_Rb_tree_node_baseEOT_RNSt16remove_referenceIS3_E4typeE = comdat any

$_ZNKSt16initializer_listISt4pairIKcdEE4sizeEv = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE5beginEv = comdat any

$_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2EPKSt18_Rb_tree_node_base = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE11lower_boundERS3_ = comdat any

$_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv = comdat any

$_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE8key_compEv = comdat any

$_ZNKSt17_Rb_tree_iteratorISt4pairIKcdEEdeEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_ = comdat any

$_ZNSt5tupleIJRKcEEC2ILb1ELb1EEES1_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11lower_boundERS1_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_lower_boundEPSt13_Rb_tree_nodeIS2_EPSt18_Rb_tree_node_baseRS1_ = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8key_compEv = comdat any

$_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE = comdat any

$_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE = comdat any

$_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeC2IJRKSt21piecewise_construct_tSt5tupleIJRS1_EESE_IJEEEEERS8_DpOT_ = comdat any

$_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node6_M_keyEv = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node9_M_insertES0_IPSt18_Rb_tree_node_baseSB_E = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeD2Ev = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEEPSt13_Rb_tree_nodeIS2_EDpOT_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEEvPSt13_Rb_tree_nodeIS2_EDpOT_ = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJRS2_EESB_IJEEEEEvRS5_PT_DpOT0_ = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJRS2_EESA_IJEEEEEvPT_DpOT0_ = comdat any

$_ZNSt5tupleIJRKcEEC2EOS2_ = comdat any

$_ZNSt4pairIKcdEC2IJRS0_EJEEESt21piecewise_construct_tSt5tupleIJDpT_EES5_IJDpT0_EE = comdat any

$_ZNSt11_Tuple_implILm0EJRKcEEC2EOS2_ = comdat any

$_ZNSt4pairIKcdEC2IJRS0_EJLm0EEJEJEEERSt5tupleIJDpT_EERS4_IJDpT1_EESt12_Index_tupleIJXspT0_EEESD_IJXspT2_EEE = comdat any

$_ZSt7forwardIRKcEOT_RNSt16remove_referenceIS2_E4typeE = comdat any

$_ZSt3getILm0EJRKcEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS6_ = comdat any

$_ZSt12__get_helperILm0ERKcJEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE = comdat any

$_ZNSt11_Tuple_implILm0EJRKcEE7_M_headERS2_ = comdat any

$_ZNSt10_Head_baseILm0ERKcLb0EE7_M_headERS2_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E = comdat any

$_ZNSt11_Tuple_implILm0EJRKcEEC2ES1_ = comdat any

$_ZNSt10_Head_baseILm0ERKcLb0EEC2ES1_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_ = comdat any

$_ZSt16forward_as_tupleIJcEESt5tupleIJDpOT_EES3_ = comdat any

$_ZSt4moveIRcEONSt16remove_referenceIT_E4typeEOS2_ = comdat any

$_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeC2IJRKSt21piecewise_construct_tSt5tupleIJOcEESE_IJEEEEERS8_DpOT_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEEPSt13_Rb_tree_nodeIS2_EDpOT_ = comdat any

$_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEEvPSt13_Rb_tree_nodeIS2_EDpOT_ = comdat any

$_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJOcEESB_IJEEEEEvRS5_PT_DpOT0_ = comdat any

$_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJOcEESA_IJEEEEEvPT_DpOT0_ = comdat any

$_ZNSt5tupleIJOcEEC2EOS1_ = comdat any

$_ZNSt4pairIKcdEC2IJOcEJEEESt21piecewise_construct_tSt5tupleIJDpT_EES5_IJDpT0_EE = comdat any

$_ZNSt11_Tuple_implILm0EJOcEEC2EOS1_ = comdat any

$_ZNSt4pairIKcdEC2IJOcEJLm0EEJEJEEERSt5tupleIJDpT_EERS4_IJDpT1_EESt12_Index_tupleIJXspT0_EEESD_IJXspT2_EEE = comdat any

$_ZSt7forwardIOcEOT_RNSt16remove_referenceIS1_E4typeE = comdat any

$_ZSt3getILm0EJOcEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS5_ = comdat any

$_ZSt12__get_helperILm0EOcJEERT0_RSt11_Tuple_implIXT_EJS1_DpT1_EE = comdat any

$_ZNSt11_Tuple_implILm0EJOcEE7_M_headERS1_ = comdat any

$_ZNSt10_Head_baseILm0EOcLb0EE7_M_headERS1_ = comdat any

$_ZSt7forwardIcEOT_RNSt16remove_referenceIS0_E4typeE = comdat any

$_ZNSt5tupleIJOcEEC2IJcELb1ELb1EEEDpOT_ = comdat any

$_ZNSt11_Tuple_implILm0EJOcEEC2IcEEOT_ = comdat any

$_ZNSt10_Head_baseILm0EOcLb0EEC2IcEEOT_ = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external hidden global i8
@frequency = dso_local global %"class.std::map" zeroinitializer, align 8
@constinit = private constant [26 x %"struct.std::pair"] [%"struct.std::pair" { i8 97, double 8.167000e-02 }, %"struct.std::pair" { i8 98, double 1.492000e-02 }, %"struct.std::pair" { i8 99, double 2.782000e-02 }, %"struct.std::pair" { i8 100, double 4.253000e-02 }, %"struct.std::pair" { i8 101, double 1.270200e-01 }, %"struct.std::pair" { i8 102, double 2.228000e-02 }, %"struct.std::pair" { i8 103, double 2.015000e-02 }, %"struct.std::pair" { i8 104, double 6.094000e-02 }, %"struct.std::pair" { i8 105, double 6.966000e-02 }, %"struct.std::pair" { i8 106, double 1.530000e-03 }, %"struct.std::pair" { i8 107, double 7.720000e-03 }, %"struct.std::pair" { i8 108, double 4.025000e-02 }, %"struct.std::pair" { i8 109, double 2.406000e-02 }, %"struct.std::pair" { i8 110, double 6.749000e-02 }, %"struct.std::pair" { i8 111, double 7.507000e-02 }, %"struct.std::pair" { i8 112, double 1.929000e-02 }, %"struct.std::pair" { i8 113, double 0x3F4F212D77318FC5 }, %"struct.std::pair" { i8 114, double 5.987000e-02 }, %"struct.std::pair" { i8 115, double 6.327000e-02 }, %"struct.std::pair" { i8 116, double 9.056000e-02 }, %"struct.std::pair" { i8 117, double 2.758000e-02 }, %"struct.std::pair" { i8 118, double 9.780000e-03 }, %"struct.std::pair" { i8 119, double 2.360000e-02 }, %"struct.std::pair" { i8 120, double 1.500000e-03 }, %"struct.std::pair" { i8 121, double 1.975000e-02 }, %"struct.std::pair" { i8 122, double 7.400000e-04 }], align 8
@.str = private unnamed_addr constant [11 x i8] c"020.cipher\00", align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str.2 = private unnamed_addr constant [13 x i8] c"Length key: \00", align 1
@.str.3 = private unnamed_addr constant [8 x i8] c"\0AWord: \00", align 1
@.str.4 = private unnamed_addr constant [3 x i8] c"\0A\0A\00", align 1
@_ZStL19piecewise_construct = internal constant %"struct.std::piecewise_construct_t" undef, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_program.cpp, i8* null }]

; Function Attrs: noinline uwtable
define internal void @__cxx_global_var_init() #0 section ".text.startup" {
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* noundef nonnull align 1 dereferenceable(1) @_ZStL8__ioinit)
  %1 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init", %"class.std::ios_base::Init"* @_ZStL8__ioinit, i32 0, i32 0), i8* @__dso_handle) #3
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* noundef nonnull align 1 dereferenceable(1)) unnamed_addr #1

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* noundef nonnull align 1 dereferenceable(1)) unnamed_addr #2

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define dso_local noundef signext i8 @_Z7getStepci(i8 noundef signext %0, i32 noundef %1) #4 {
  %3 = alloca i8, align 1
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store i8 %0, i8* %3, align 1
  store i32 %1, i32* %4, align 4
  %6 = load i8, i8* %3, align 1
  %7 = sext i8 %6 to i32
  %8 = load i32, i32* %4, align 4
  %9 = add nsw i32 %7, %8
  store i32 %9, i32* %5, align 4
  br label %10

10:                                               ; preds = %13, %2
  %11 = load i32, i32* %5, align 4
  %12 = icmp sgt i32 %11, 122
  br i1 %12, label %13, label %16

13:                                               ; preds = %10
  %14 = load i32, i32* %5, align 4
  %15 = sub nsw i32 %14, 26
  store i32 %15, i32* %5, align 4
  br label %10, !llvm.loop !6

16:                                               ; preds = %10
  %17 = load i32, i32* %5, align 4
  %18 = trunc i32 %17 to i8
  ret i8 %18
}

; Function Attrs: noinline uwtable
define internal void @__cxx_global_var_init.1() #0 section ".text.startup" personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %1 = alloca %"class.std::initializer_list", align 8
  %2 = alloca [26 x %"struct.std::pair"], align 8
  %3 = alloca %"struct.std::less", align 1
  %4 = alloca %"class.std::allocator.0", align 1
  %5 = alloca i8*, align 8
  %6 = alloca i32, align 4
  %7 = getelementptr inbounds [26 x %"struct.std::pair"], [26 x %"struct.std::pair"]* %2, i64 0, i64 0
  %8 = bitcast [26 x %"struct.std::pair"]* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %8, i8* align 8 getelementptr inbounds ([26 x %"struct.std::pair"], [26 x %"struct.std::pair"]* @constinit, i32 0, i32 0, i32 0), i64 416, i1 false)
  %9 = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %1, i32 0, i32 0
  %10 = getelementptr inbounds [26 x %"struct.std::pair"], [26 x %"struct.std::pair"]* %2, i64 0, i64 0
  store %"struct.std::pair"* %10, %"struct.std::pair"** %9, align 8
  %11 = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %1, i32 0, i32 1
  store i64 26, i64* %11, align 8
  call void @_ZNSaISt4pairIKcdEEC2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %4) #3
  %12 = bitcast %"class.std::initializer_list"* %1 to { %"struct.std::pair"*, i64 }*
  %13 = getelementptr inbounds { %"struct.std::pair"*, i64 }, { %"struct.std::pair"*, i64 }* %12, i32 0, i32 0
  %14 = load %"struct.std::pair"*, %"struct.std::pair"** %13, align 8
  %15 = getelementptr inbounds { %"struct.std::pair"*, i64 }, { %"struct.std::pair"*, i64 }* %12, i32 0, i32 1
  %16 = load i64, i64* %15, align 8
  invoke void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2ESt16initializer_listIS4_ERKS1_RKS5_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) @frequency, %"struct.std::pair"* %14, i64 %16, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %3, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %4)
          to label %17 unwind label %19

17:                                               ; preds = %0
  call void @_ZNSaISt4pairIKcdEED2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %4) #3
  %18 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::map"*)* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::map", %"class.std::map"* @frequency, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0), i8* @__dso_handle) #3
  ret void

19:                                               ; preds = %0
  %20 = landingpad { i8*, i32 }
          cleanup
  %21 = extractvalue { i8*, i32 } %20, 0
  store i8* %21, i8** %5, align 8
  %22 = extractvalue { i8*, i32 } %20, 1
  store i32 %22, i32* %6, align 4
  call void @_ZNSaISt4pairIKcdEED2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %4) #3
  br label %23

23:                                               ; preds = %19
  %24 = load i8*, i8** %5, align 8
  %25 = load i32, i32* %6, align 4
  %26 = insertvalue { i8*, i32 } undef, i8* %24, 0
  %27 = insertvalue { i8*, i32 } %26, i32 %25, 1
  resume { i8*, i32 } %27
}

; Function Attrs: argmemonly nofree nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #5

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt4pairIKcdEEC2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::allocator.0"*, align 8
  store %"class.std::allocator.0"* %0, %"class.std::allocator.0"** %2, align 8
  %3 = load %"class.std::allocator.0"*, %"class.std::allocator.0"** %2, align 8
  %4 = bitcast %"class.std::allocator.0"* %3 to %"class.std::__new_allocator.1"*
  call void @_ZNSt15__new_allocatorISt4pairIKcdEEC2Ev(%"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2ESt16initializer_listIS4_ERKS1_RKS5_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::pair"* %1, i64 %2, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %3, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %4) unnamed_addr #7 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %6 = alloca %"class.std::initializer_list", align 8
  %7 = alloca %"class.std::map"*, align 8
  %8 = alloca %"struct.std::less"*, align 8
  %9 = alloca %"class.std::allocator.0"*, align 8
  %10 = alloca %"class.std::allocator.0", align 1
  %11 = alloca i8*, align 8
  %12 = alloca i32, align 4
  %13 = bitcast %"class.std::initializer_list"* %6 to { %"struct.std::pair"*, i64 }*
  %14 = getelementptr inbounds { %"struct.std::pair"*, i64 }, { %"struct.std::pair"*, i64 }* %13, i32 0, i32 0
  store %"struct.std::pair"* %1, %"struct.std::pair"** %14, align 8
  %15 = getelementptr inbounds { %"struct.std::pair"*, i64 }, { %"struct.std::pair"*, i64 }* %13, i32 0, i32 1
  store i64 %2, i64* %15, align 8
  store %"class.std::map"* %0, %"class.std::map"** %7, align 8
  store %"struct.std::less"* %3, %"struct.std::less"** %8, align 8
  store %"class.std::allocator.0"* %4, %"class.std::allocator.0"** %9, align 8
  %16 = load %"class.std::map"*, %"class.std::map"** %7, align 8
  %17 = getelementptr inbounds %"class.std::map", %"class.std::map"* %16, i32 0, i32 0
  %18 = load %"struct.std::less"*, %"struct.std::less"** %8, align 8
  %19 = load %"class.std::allocator.0"*, %"class.std::allocator.0"** %9, align 8
  call void @_ZNSaISt4pairIKcdEEC2ERKS2_(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %10, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %19) #3
  invoke void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2ERKS6_RKS7_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %18, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %10)
          to label %20 unwind label %25

20:                                               ; preds = %5
  call void @_ZNSaISt4pairIKcdEED2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %10) #3
  %21 = getelementptr inbounds %"class.std::map", %"class.std::map"* %16, i32 0, i32 0
  %22 = call noundef %"struct.std::pair"* @_ZNKSt16initializer_listISt4pairIKcdEE5beginEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %6) #3
  %23 = call noundef %"struct.std::pair"* @_ZNKSt16initializer_listISt4pairIKcdEE3endEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %6) #3
  invoke void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_insert_range_uniqueIPKS2_EENSt9enable_ifIXsr17__same_value_typeIT_EE5valueEvE4typeESD_SD_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %21, %"struct.std::pair"* noundef %22, %"struct.std::pair"* noundef %23)
          to label %24 unwind label %29

24:                                               ; preds = %20
  ret void

25:                                               ; preds = %5
  %26 = landingpad { i8*, i32 }
          cleanup
  %27 = extractvalue { i8*, i32 } %26, 0
  store i8* %27, i8** %11, align 8
  %28 = extractvalue { i8*, i32 } %26, 1
  store i32 %28, i32* %12, align 4
  call void @_ZNSaISt4pairIKcdEED2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %10) #3
  br label %33

29:                                               ; preds = %20
  %30 = landingpad { i8*, i32 }
          cleanup
  %31 = extractvalue { i8*, i32 } %30, 0
  store i8* %31, i8** %11, align 8
  %32 = extractvalue { i8*, i32 } %30, 1
  store i32 %32, i32* %12, align 4
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  br label %33

33:                                               ; preds = %29, %25
  %34 = load i8*, i8** %11, align 8
  %35 = load i32, i32* %12, align 4
  %36 = insertvalue { i8*, i32 } undef, i8* %34, 0
  %37 = insertvalue { i8*, i32 } %36, i32 %35, 1
  resume { i8*, i32 } %37
}

declare i32 @__gxx_personality_v0(...)

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt4pairIKcdEED2Ev(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::allocator.0"*, align 8
  store %"class.std::allocator.0"* %0, %"class.std::allocator.0"** %2, align 8
  %3 = load %"class.std::allocator.0"*, %"class.std::allocator.0"** %2, align 8
  %4 = bitcast %"class.std::allocator.0"* %3 to %"class.std::__new_allocator.1"*
  call void @_ZNSt15__new_allocatorISt4pairIKcdEED2Ev(%"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::map"*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %2, align 8
  %3 = load %"class.std::map"*, %"class.std::map"** %2, align 8
  %4 = getelementptr inbounds %"class.std::map", %"class.std::map"* %3, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %4) #3
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define dso_local { i8, double } @_Z7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) #8 {
  %2 = alloca %"struct.std::pair.3", align 8
  %3 = alloca %"class.std::map"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %5 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %6 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %7 = alloca %class.anon, align 1
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  %8 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %9 = call %"struct.std::_Rb_tree_node_base"* @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE5beginEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %8) #3
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %9, %"struct.std::_Rb_tree_node_base"** %10, align 8
  %11 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %12 = call %"struct.std::_Rb_tree_node_base"* @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11) #3
  %13 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %6, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %12, %"struct.std::_Rb_tree_node_base"** %13, align 8
  %14 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  %15 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %14, align 8
  %16 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %6, i32 0, i32 0
  %17 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %16, align 8
  %18 = call %"struct.std::_Rb_tree_node_base"* @"_ZSt11max_elementISt23_Rb_tree_const_iteratorISt4pairIKcdEEZ7get_maxRKSt3mapIcdSt4lessIcESaIS3_EEE3$_0ET_SD_SD_T0_"(%"struct.std::_Rb_tree_node_base"* %15, %"struct.std::_Rb_tree_node_base"* %17)
  %19 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %4, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %18, %"struct.std::_Rb_tree_node_base"** %19, align 8
  %20 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %4) #3
  call void @_ZNSt4pairIcdEC2IKcdLb1EEERKS_IT_T0_E(%"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %2, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %20)
  %21 = bitcast %"struct.std::pair.3"* %2 to { i8, double }*
  %22 = load { i8, double }, { i8, double }* %21, align 8
  ret { i8, double } %22
}

; Function Attrs: mustprogress noinline optnone uwtable
define internal %"struct.std::_Rb_tree_node_base"* @"_ZSt11max_elementISt23_Rb_tree_const_iteratorISt4pairIKcdEEZ7get_maxRKSt3mapIcdSt4lessIcESaIS3_EEE3$_0ET_SD_SD_T0_"(%"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"* %1) #8 {
  %3 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %4 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %5 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %6 = alloca %class.anon, align 1
  %7 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %8 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %9 = alloca %"struct.__gnu_cxx::__ops::_Iter_comp_iter", align 1
  %10 = alloca %class.anon, align 1
  %11 = alloca %"struct.__gnu_cxx::__ops::_Iter_comp_iter", align 1
  %12 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %4, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %12, align 8
  %13 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %13, align 8
  %14 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  %15 = bitcast %"struct.std::_Rb_tree_const_iterator"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %14, i8* align 8 %15, i64 8, i1 false)
  %16 = bitcast %"struct.std::_Rb_tree_const_iterator"* %8 to i8*
  %17 = bitcast %"struct.std::_Rb_tree_const_iterator"* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %16, i8* align 8 %17, i64 8, i1 false)
  call void @"_ZN9__gnu_cxx5__ops16__iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EENS0_15_Iter_comp_iterIT_EESE_"()
  %18 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %7, i32 0, i32 0
  %19 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %18, align 8
  %20 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %8, i32 0, i32 0
  %21 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %20, align 8
  %22 = call %"struct.std::_Rb_tree_node_base"* @"_ZSt13__max_elementISt23_Rb_tree_const_iteratorISt4pairIKcdEEN9__gnu_cxx5__ops15_Iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaIS3_EEE3$_0EEET_SH_SH_T0_"(%"struct.std::_Rb_tree_node_base"* %19, %"struct.std::_Rb_tree_node_base"* %21)
  %23 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %3, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %22, %"struct.std::_Rb_tree_node_base"** %23, align 8
  %24 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %3, i32 0, i32 0
  %25 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %24, align 8
  ret %"struct.std::_Rb_tree_node_base"* %25
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE5beginEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %3 = alloca %"class.std::map"*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  %4 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %5 = getelementptr inbounds %"class.std::map", %"class.std::map"* %4, i32 0, i32 0
  %6 = call %"struct.std::_Rb_tree_node_base"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE5beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %2, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %2, i32 0, i32 0
  %9 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  ret %"struct.std::_Rb_tree_node_base"* %9
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %3 = alloca %"class.std::map"*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  %4 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %5 = getelementptr inbounds %"class.std::map", %"class.std::map"* %4, i32 0, i32 0
  %6 = call %"struct.std::_Rb_tree_node_base"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %2, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %2, i32 0, i32 0
  %9 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  ret %"struct.std::_Rb_tree_node_base"* %9
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %3, i32 0, i32 0
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %5 to %"struct.std::_Rb_tree_node"*
  %7 = call noundef %"struct.std::pair"* @_ZNKSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %6)
  ret %"struct.std::pair"* %7
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIcdEC2IKcdLb1EEERKS_IT_T0_E(%"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %0, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::pair.3"*, align 8
  %4 = alloca %"struct.std::pair"*, align 8
  store %"struct.std::pair.3"* %0, %"struct.std::pair.3"** %3, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %4, align 8
  %5 = load %"struct.std::pair.3"*, %"struct.std::pair.3"** %3, align 8
  %6 = bitcast %"struct.std::pair.3"* %5 to %"class.std::__pair_base.4"*
  %7 = getelementptr inbounds %"struct.std::pair.3", %"struct.std::pair.3"* %5, i32 0, i32 0
  %8 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  %9 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %8, i32 0, i32 0
  %10 = load i8, i8* %9, align 8
  store i8 %10, i8* %7, align 8
  %11 = getelementptr inbounds %"struct.std::pair.3", %"struct.std::pair.3"* %5, i32 0, i32 1
  %12 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  %13 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %12, i32 0, i32 1
  %14 = load double, double* %13, align 8
  store double %14, double* %11, align 8
  ret void
}

; Function Attrs: mustprogress noinline norecurse optnone uwtable
define dso_local noundef i32 @main() #9 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %1 = alloca i32, align 4
  %2 = alloca %"class.std::basic_ifstream", align 8
  %3 = alloca i8*, align 8
  %4 = alloca i32, align 4
  %5 = alloca %"class.std::__cxx11::basic_string", align 8
  %6 = alloca i32, align 4
  %7 = alloca %"class.std::__cxx11::basic_string", align 8
  %8 = alloca i32*, align 8
  %9 = alloca i32, align 4
  %10 = alloca %"class.std::map", align 8
  %11 = alloca %"class.std::map", align 8
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca %"class.std::map.10", align 8
  %15 = alloca i8, align 1
  %16 = alloca double, align 8
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca double, align 8
  %20 = alloca i8, align 1
  %21 = alloca i8, align 1
  %22 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1Ev(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256) %2)
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256) %2, i8* noundef getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i64 0, i64 0), i32 noundef 8)
          to label %23 unwind label %63

23:                                               ; preds = %0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5) #3
  %24 = bitcast %"class.std::basic_ifstream"* %2 to %"class.std::basic_istream"*
  %25 = invoke noundef nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* noundef nonnull align 8 dereferenceable(16) %24, %"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5)
          to label %26 unwind label %67

26:                                               ; preds = %23
  store i32 8, i32* %6, align 4
  %27 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* noundef getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i64 0, i64 0))
          to label %28 unwind label %67

28:                                               ; preds = %26
  %29 = load i32, i32* %6, align 4
  %30 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) %27, i32 noundef %29)
          to label %31 unwind label %67

31:                                               ; preds = %28
  %32 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) %30, i8 noundef signext 10)
          to label %33 unwind label %67

33:                                               ; preds = %31
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %7, %"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5)
          to label %34 unwind label %67

34:                                               ; preds = %33
  store i32* null, i32** %8, align 8
  store i32 0, i32* %9, align 4
  br label %35

35:                                               ; preds = %172, %34
  %36 = load i32, i32* %9, align 4
  %37 = load i32, i32* %6, align 4
  %38 = icmp slt i32 %36, %37
  br i1 %38, label %39, label %176

39:                                               ; preds = %35
  invoke void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2ERKS6_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %10, %"class.std::map"* noundef nonnull align 8 dereferenceable(48) @frequency)
          to label %40 unwind label %71

40:                                               ; preds = %39
  call void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11) #3
  store i32 0, i32* %12, align 4
  br label %41

41:                                               ; preds = %59, %40
  %42 = load i32, i32* %12, align 4
  %43 = load i32, i32* %9, align 4
  %44 = add nsw i32 %42, %43
  %45 = sext i32 %44 to i64
  %46 = call noundef i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5) #3
  %47 = icmp ult i64 %45, %46
  br i1 %47, label %48, label %79

48:                                               ; preds = %41
  %49 = load i32, i32* %12, align 4
  %50 = load i32, i32* %9, align 4
  %51 = add nsw i32 %49, %50
  %52 = sext i32 %51 to i64
  %53 = invoke noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEixEm(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5, i64 noundef %52)
          to label %54 unwind label %75

54:                                               ; preds = %48
  %55 = invoke noundef nonnull align 8 dereferenceable(8) double* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11, i8* noundef nonnull align 1 dereferenceable(1) %53)
          to label %56 unwind label %75

56:                                               ; preds = %54
  %57 = load double, double* %55, align 8
  %58 = fadd double %57, 1.000000e+00
  store double %58, double* %55, align 8
  br label %59

59:                                               ; preds = %56
  %60 = load i32, i32* %6, align 4
  %61 = load i32, i32* %12, align 4
  %62 = add nsw i32 %61, %60
  store i32 %62, i32* %12, align 4
  br label %41, !llvm.loop !8

63:                                               ; preds = %0
  %64 = landingpad { i8*, i32 }
          cleanup
  %65 = extractvalue { i8*, i32 } %64, 0
  store i8* %65, i8** %3, align 8
  %66 = extractvalue { i8*, i32 } %64, 1
  store i32 %66, i32* %4, align 4
  br label %186

67:                                               ; preds = %33, %31, %28, %26, %23
  %68 = landingpad { i8*, i32 }
          cleanup
  %69 = extractvalue { i8*, i32 } %68, 0
  store i8* %69, i8** %3, align 8
  %70 = extractvalue { i8*, i32 } %68, 1
  store i32 %70, i32* %4, align 4
  br label %185

71:                                               ; preds = %180, %178, %176, %39
  %72 = landingpad { i8*, i32 }
          cleanup
  %73 = extractvalue { i8*, i32 } %72, 0
  store i8* %73, i8** %3, align 8
  %74 = extractvalue { i8*, i32 } %72, 1
  store i32 %74, i32* %4, align 4
  br label %184

75:                                               ; preds = %54, %48
  %76 = landingpad { i8*, i32 }
          cleanup
  %77 = extractvalue { i8*, i32 } %76, 0
  store i8* %77, i8** %3, align 8
  %78 = extractvalue { i8*, i32 } %76, 1
  store i32 %78, i32* %4, align 4
  br label %175

79:                                               ; preds = %41
  %80 = load i32*, i32** %8, align 8
  %81 = load i32, i32* %80, align 4
  store i32 %81, i32* %13, align 4
  call void @_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEEC2Ev(%"class.std::map.10"* noundef nonnull align 8 dereferenceable(48) %14) #3
  store i8 97, i8* %15, align 1
  br label %82

82:                                               ; preds = %93, %79
  %83 = load i8, i8* %15, align 1
  %84 = sext i8 %83 to i32
  %85 = icmp sle i32 %84, 122
  br i1 %85, label %86, label %100

86:                                               ; preds = %82
  %87 = call noundef i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5) #3
  %88 = uitofp i64 %87 to double
  %89 = invoke noundef nonnull align 8 dereferenceable(8) double* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11, i8* noundef nonnull align 1 dereferenceable(1) %15)
          to label %90 unwind label %96

90:                                               ; preds = %86
  %91 = load double, double* %89, align 8
  %92 = fdiv double %91, %88
  store double %92, double* %89, align 8
  br label %93

93:                                               ; preds = %90
  %94 = load i8, i8* %15, align 1
  %95 = add i8 %94, 1
  store i8 %95, i8* %15, align 1
  br label %82, !llvm.loop !9

96:                                               ; preds = %166, %152, %146, %117, %111, %109, %86
  %97 = landingpad { i8*, i32 }
          cleanup
  %98 = extractvalue { i8*, i32 } %97, 0
  store i8* %98, i8** %3, align 8
  %99 = extractvalue { i8*, i32 } %97, 1
  store i32 %99, i32* %4, align 4
  call void @_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEED2Ev(%"class.std::map.10"* noundef nonnull align 8 dereferenceable(48) %14) #3
  br label %175

100:                                              ; preds = %82
  store double 1.000000e+08, double* %16, align 8
  store i32 0, i32* %17, align 4
  store i32 0, i32* %18, align 4
  br label %101

101:                                              ; preds = %135, %100
  %102 = load i32, i32* %18, align 4
  %103 = icmp slt i32 %102, 26
  br i1 %103, label %104, label %138

104:                                              ; preds = %101
  store double 0.000000e+00, double* %19, align 8
  store i8 97, i8* %20, align 1
  br label %105

105:                                              ; preds = %124, %104
  %106 = load i8, i8* %20, align 1
  %107 = sext i8 %106 to i32
  %108 = icmp sle i32 %107, 122
  br i1 %108, label %109, label %127

109:                                              ; preds = %105
  %110 = invoke noundef nonnull align 8 dereferenceable(8) double* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11, i8* noundef nonnull align 1 dereferenceable(1) %20)
          to label %111 unwind label %96

111:                                              ; preds = %109
  %112 = load double, double* %110, align 8
  %113 = load i8, i8* %20, align 1
  %114 = load i32, i32* %18, align 4
  %115 = call noundef signext i8 @_Z7getStepci(i8 noundef signext %113, i32 noundef %114)
  store i8 %115, i8* %21, align 1
  %116 = invoke noundef nonnull align 8 dereferenceable(8) double* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixEOc(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) @frequency, i8* noundef nonnull align 1 dereferenceable(1) %21)
          to label %117 unwind label %96

117:                                              ; preds = %111
  %118 = load double, double* %116, align 8
  %119 = fsub double %112, %118
  %120 = invoke noundef double @_ZSt3absd(double noundef %119)
          to label %121 unwind label %96

121:                                              ; preds = %117
  %122 = load double, double* %19, align 8
  %123 = fadd double %122, %120
  store double %123, double* %19, align 8
  br label %124

124:                                              ; preds = %121
  %125 = load i8, i8* %20, align 1
  %126 = add i8 %125, 1
  store i8 %126, i8* %20, align 1
  br label %105, !llvm.loop !10

127:                                              ; preds = %105
  %128 = load double, double* %19, align 8
  %129 = load double, double* %16, align 8
  %130 = fcmp olt double %128, %129
  br i1 %130, label %131, label %134

131:                                              ; preds = %127
  %132 = load double, double* %19, align 8
  store double %132, double* %16, align 8
  %133 = load i32, i32* %18, align 4
  store i32 %133, i32* %17, align 4
  br label %134

134:                                              ; preds = %131, %127
  br label %135

135:                                              ; preds = %134
  %136 = load i32, i32* %18, align 4
  %137 = add nsw i32 %136, 1
  store i32 %137, i32* %18, align 4
  br label %101, !llvm.loop !11

138:                                              ; preds = %101
  store i32 0, i32* %22, align 4
  br label %139

139:                                              ; preds = %162, %138
  %140 = load i32, i32* %22, align 4
  %141 = load i32, i32* %9, align 4
  %142 = add nsw i32 %140, %141
  %143 = sext i32 %142 to i64
  %144 = call noundef i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5) #3
  %145 = icmp ult i64 %143, %144
  br i1 %145, label %146, label %166

146:                                              ; preds = %139
  %147 = load i32, i32* %22, align 4
  %148 = load i32, i32* %9, align 4
  %149 = add nsw i32 %147, %148
  %150 = sext i32 %149 to i64
  %151 = invoke noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEixEm(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %7, i64 noundef %150)
          to label %152 unwind label %96

152:                                              ; preds = %146
  %153 = load i8, i8* %151, align 1
  %154 = load i32, i32* %17, align 4
  %155 = call noundef signext i8 @_Z7getStepci(i8 noundef signext %153, i32 noundef %154)
  %156 = load i32, i32* %22, align 4
  %157 = load i32, i32* %9, align 4
  %158 = add nsw i32 %156, %157
  %159 = sext i32 %158 to i64
  %160 = invoke noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEixEm(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %7, i64 noundef %159)
          to label %161 unwind label %96

161:                                              ; preds = %152
  store i8 %155, i8* %160, align 1
  br label %162

162:                                              ; preds = %161
  %163 = load i32, i32* %6, align 4
  %164 = load i32, i32* %22, align 4
  %165 = add nsw i32 %164, %163
  store i32 %165, i32* %22, align 4
  br label %139, !llvm.loop !12

166:                                              ; preds = %139
  %167 = load i32, i32* %17, align 4
  %168 = add nsw i32 97, %167
  %169 = trunc i32 %168 to i8
  %170 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) @_ZSt4cout, i8 noundef signext %169)
          to label %171 unwind label %96

171:                                              ; preds = %166
  call void @_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEED2Ev(%"class.std::map.10"* noundef nonnull align 8 dereferenceable(48) %14) #3
  call void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11) #3
  call void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %10) #3
  br label %172

172:                                              ; preds = %171
  %173 = load i32, i32* %9, align 4
  %174 = add nsw i32 %173, 1
  store i32 %174, i32* %9, align 4
  br label %35, !llvm.loop !13

175:                                              ; preds = %96, %75
  call void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %11) #3
  call void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEED2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %10) #3
  br label %184

176:                                              ; preds = %35
  %177 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* noundef getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i64 0, i64 0))
          to label %178 unwind label %71

178:                                              ; preds = %176
  %179 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) %177, %"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %7)
          to label %180 unwind label %71

180:                                              ; preds = %178
  %181 = invoke noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8) %179, i8* noundef getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i64 0, i64 0))
          to label %182 unwind label %71

182:                                              ; preds = %180
  store i32 0, i32* %1, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %7) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5) #3
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256) %2) #3
  %183 = load i32, i32* %1, align 4
  ret i32 %183

184:                                              ; preds = %175, %71
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %7) #3
  br label %185

185:                                              ; preds = %184, %67
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32) %5) #3
  br label %186

186:                                              ; preds = %185, %63
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256) %2) #3
  br label %187

187:                                              ; preds = %186
  %188 = load i8*, i8** %3, align 8
  %189 = load i32, i32* %4, align 4
  %190 = insertvalue { i8*, i32 } undef, i8* %188, 0
  %191 = insertvalue { i8*, i32 } %190, i32 %189, 1
  resume { i8*, i32 } %191
}

declare void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1Ev(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256)) unnamed_addr #1

declare void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256), i8* noundef, i32 noundef) #1

; Function Attrs: nounwind
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32)) unnamed_addr #2

declare noundef nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* noundef nonnull align 8 dereferenceable(16), %"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32)) #1

declare noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8), i8 noundef signext) #1

declare noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8), i8* noundef) #1

declare noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8), i32 noundef) #1

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32), %"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32)) unnamed_addr #1

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2ERKS6_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0, %"class.std::map"* noundef nonnull align 8 dereferenceable(48) %1) unnamed_addr #7 comdat align 2 {
  %3 = alloca %"class.std::map"*, align 8
  %4 = alloca %"class.std::map"*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  store %"class.std::map"* %1, %"class.std::map"** %4, align 8
  %5 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %6 = getelementptr inbounds %"class.std::map", %"class.std::map"* %5, i32 0, i32 0
  %7 = load %"class.std::map"*, %"class.std::map"** %4, align 8
  %8 = getelementptr inbounds %"class.std::map", %"class.std::map"* %7, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2ERKS8_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEC2Ev(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::map"*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %2, align 8
  %3 = load %"class.std::map"*, %"class.std::map"** %2, align 8
  %4 = getelementptr inbounds %"class.std::map", %"class.std::map"* %3, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2Ev(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %4) #3
  ret void
}

; Function Attrs: nounwind
declare noundef i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32)) #2

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) double* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) #8 comdat align 2 {
  %3 = alloca %"class.std::map"*, align 8
  %4 = alloca i8*, align 8
  %5 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %6 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %7 = alloca %"struct.std::less", align 1
  %8 = alloca %"struct.std::less", align 1
  %9 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %10 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %11 = alloca %"class.std::tuple", align 8
  %12 = alloca %"class.std::tuple.23", align 1
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  store i8* %1, i8** %4, align 8
  %13 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %14 = load i8*, i8** %4, align 8
  %15 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE11lower_boundERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %13, i8* noundef nonnull align 1 dereferenceable(1) %14)
  %16 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %15, %"struct.std::_Rb_tree_node_base"** %16, align 8
  %17 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %13) #3
  %18 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %17, %"struct.std::_Rb_tree_node_base"** %18, align 8
  %19 = call noundef zeroext i1 @_ZSteqRKSt17_Rb_tree_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %6) #3
  br i1 %19, label %25, label %20

20:                                               ; preds = %2
  call void @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE8key_compEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %13)
  %21 = load i8*, i8** %4, align 8
  %22 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt17_Rb_tree_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %23 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %22, i32 0, i32 0
  %24 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %7, i8* noundef nonnull align 1 dereferenceable(1) %21, i8* noundef nonnull align 1 dereferenceable(1) %23)
  br label %25

25:                                               ; preds = %20, %2
  %26 = phi i1 [ true, %2 ], [ %24, %20 ]
  br i1 %26, label %27, label %36

27:                                               ; preds = %25
  %28 = getelementptr inbounds %"class.std::map", %"class.std::map"* %13, i32 0, i32 0
  call void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2ERKSt17_Rb_tree_iteratorIS2_E(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %10, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %29 = load i8*, i8** %4, align 8
  call void @_ZNSt5tupleIJRKcEEC2ILb1ELb1EEES1_(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %11, i8* noundef nonnull align 1 dereferenceable(1) %29) #3
  %30 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %10, i32 0, i32 0
  %31 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %30, align 8
  %32 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %28, %"struct.std::_Rb_tree_node_base"* %31, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) @_ZStL19piecewise_construct, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %11, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %12)
  %33 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %9, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %32, %"struct.std::_Rb_tree_node_base"** %33, align 8
  %34 = bitcast %"struct.std::_Rb_tree_iterator"* %5 to i8*
  %35 = bitcast %"struct.std::_Rb_tree_iterator"* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %34, i8* align 8 %35, i64 8, i1 false)
  br label %36

36:                                               ; preds = %27, %25
  %37 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt17_Rb_tree_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %38 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %37, i32 0, i32 1
  ret double* %38
}

declare noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEixEm(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32), i64 noundef) #1

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEEC2Ev(%"class.std::map.10"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::map.10"*, align 8
  store %"class.std::map.10"* %0, %"class.std::map.10"** %2, align 8
  %3 = load %"class.std::map.10"*, %"class.std::map.10"** %2, align 8
  %4 = getelementptr inbounds %"class.std::map.10", %"class.std::map.10"* %3, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2Ev(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %4) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef double @_ZSt3absd(double noundef %0) #4 comdat {
  %2 = alloca double, align 8
  store double %0, double* %2, align 8
  %3 = load double, double* %2, align 8
  %4 = call double @llvm.fabs.f64(double %3)
  ret double %4
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) double* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEEixEOc(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) #8 comdat align 2 {
  %3 = alloca %"class.std::map"*, align 8
  %4 = alloca i8*, align 8
  %5 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %6 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %7 = alloca %"struct.std::less", align 1
  %8 = alloca %"struct.std::less", align 1
  %9 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %10 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %11 = alloca %"class.std::tuple.25", align 8
  %12 = alloca %"class.std::tuple.23", align 1
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  store i8* %1, i8** %4, align 8
  %13 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %14 = load i8*, i8** %4, align 8
  %15 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE11lower_boundERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %13, i8* noundef nonnull align 1 dereferenceable(1) %14)
  %16 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %15, %"struct.std::_Rb_tree_node_base"** %16, align 8
  %17 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %13) #3
  %18 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %17, %"struct.std::_Rb_tree_node_base"** %18, align 8
  %19 = call noundef zeroext i1 @_ZSteqRKSt17_Rb_tree_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %6) #3
  br i1 %19, label %25, label %20

20:                                               ; preds = %2
  call void @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE8key_compEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %13)
  %21 = load i8*, i8** %4, align 8
  %22 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt17_Rb_tree_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %23 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %22, i32 0, i32 0
  %24 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %7, i8* noundef nonnull align 1 dereferenceable(1) %21, i8* noundef nonnull align 1 dereferenceable(1) %23)
  br label %25

25:                                               ; preds = %20, %2
  %26 = phi i1 [ true, %2 ], [ %24, %20 ]
  br i1 %26, label %27, label %37

27:                                               ; preds = %25
  %28 = getelementptr inbounds %"class.std::map", %"class.std::map"* %13, i32 0, i32 0
  call void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2ERKSt17_Rb_tree_iteratorIS2_E(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %10, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %29 = load i8*, i8** %4, align 8
  %30 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt4moveIRcEONSt16remove_referenceIT_E4typeEOS2_(i8* noundef nonnull align 1 dereferenceable(1) %29) #3
  call void @_ZSt16forward_as_tupleIJcEESt5tupleIJDpOT_EES3_(%"class.std::tuple.25"* sret(%"class.std::tuple.25") align 8 %11, i8* noundef nonnull align 1 dereferenceable(1) %30) #3
  %31 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %10, i32 0, i32 0
  %32 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %31, align 8
  %33 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %28, %"struct.std::_Rb_tree_node_base"* %32, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) @_ZStL19piecewise_construct, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %11, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %12)
  %34 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %9, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %33, %"struct.std::_Rb_tree_node_base"** %34, align 8
  %35 = bitcast %"struct.std::_Rb_tree_iterator"* %5 to i8*
  %36 = bitcast %"struct.std::_Rb_tree_iterator"* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %35, i8* align 8 %36, i64 8, i1 false)
  br label %37

37:                                               ; preds = %27, %25
  %38 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt17_Rb_tree_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %39 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %38, i32 0, i32 1
  ret double* %39
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt3mapIccSt4lessIcESaISt4pairIKccEEED2Ev(%"class.std::map.10"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::map.10"*, align 8
  store %"class.std::map.10"* %0, %"class.std::map.10"** %2, align 8
  %3 = load %"class.std::map.10"*, %"class.std::map.10"** %2, align 8
  %4 = getelementptr inbounds %"class.std::map.10", %"class.std::map.10"* %3, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %4) #3
  ret void
}

declare noundef nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* noundef nonnull align 8 dereferenceable(8), %"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32)) #1

; Function Attrs: nounwind
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* noundef nonnull align 8 dereferenceable(32)) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* noundef nonnull align 8 dereferenceable(256)) unnamed_addr #2

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i32, align 4
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %5 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %6 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5) #3
  invoke void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5, %"struct.std::_Rb_tree_node"* noundef %6)
          to label %7 unwind label %9

7:                                                ; preds = %1
  %8 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %5, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %8) #3
  ret void

9:                                                ; preds = %1
  %10 = landingpad { i8*, i32 }
          catch i8* null
  %11 = extractvalue { i8*, i32 } %10, 0
  store i8* %11, i8** %3, align 8
  %12 = extractvalue { i8*, i32 } %10, 1
  store i32 %12, i32* %4, align 4
  %13 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %5, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %13) #3
  br label %14

14:                                               ; preds = %9
  %15 = load i8*, i8** %3, align 8
  call void @__clang_call_terminate(i8* %15) #16
  unreachable
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1) #8 comdat align 2 {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %4, align 8
  %6 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  br label %7

7:                                                ; preds = %10, %2
  %8 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  %9 = icmp ne %"struct.std::_Rb_tree_node"* %8, null
  br i1 %9, label %10, label %19

10:                                               ; preds = %7
  %11 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  %12 = bitcast %"struct.std::_Rb_tree_node"* %11 to %"struct.std::_Rb_tree_node_base"*
  %13 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %12) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6, %"struct.std::_Rb_tree_node"* noundef %13)
  %14 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  %15 = bitcast %"struct.std::_Rb_tree_node"* %14 to %"struct.std::_Rb_tree_node_base"*
  %16 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %15) #3
  store %"struct.std::_Rb_tree_node"* %16, %"struct.std::_Rb_tree_node"** %5, align 8
  %17 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6, %"struct.std::_Rb_tree_node"* noundef %17) #3
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  store %"struct.std::_Rb_tree_node"* %18, %"struct.std::_Rb_tree_node"** %4, align 8
  br label %7, !llvm.loop !14

19:                                               ; preds = %7
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = call noundef %"struct.std::_Rb_tree_node"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %3) #3
  ret %"struct.std::_Rb_tree_node"* %4
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %3 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %4 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %3 to %"class.std::allocator"*
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8* %0) #10 comdat {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #3
  call void @_ZSt9terminatev() #16
  unreachable
}

declare i8* @__cxa_begin_catch(i8*)

declare void @_ZSt9terminatev()

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %3, i32 0, i32 3
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %5 to %"struct.std::_Rb_tree_node"*
  ret %"struct.std::_Rb_tree_node"* %6
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %3, i32 0, i32 2
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %5 to %"struct.std::_Rb_tree_node"*
  ret %"struct.std::_Rb_tree_node"* %6
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1) #4 comdat align 2 {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %4, align 8
  %5 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %6 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE15_M_destroy_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5, %"struct.std::_Rb_tree_node"* noundef %6) #3
  %7 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5, %"struct.std::_Rb_tree_node"* noundef %7) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE15_M_destroy_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1) #4 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %4, align 8
  %5 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %6 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  %8 = invoke noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %7)
          to label %9 unwind label %11

9:                                                ; preds = %2
  call void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE7destroyIS3_EEvRS5_PT_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::pair"* noundef %8) #3
  %10 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  ret void

11:                                               ; preds = %2
  %12 = landingpad { i8*, i32 }
          catch i8* null
  %13 = extractvalue { i8*, i32 } %12, 0
  call void @__clang_call_terminate(i8* %13) #16
  unreachable
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1) #4 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %4, align 8
  %5 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %6 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  invoke void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE10deallocateERS5_PS4_m(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::_Rb_tree_node"* noundef %7, i64 noundef 1)
          to label %8 unwind label %9

8:                                                ; preds = %2
  ret void

9:                                                ; preds = %2
  %10 = landingpad { i8*, i32 }
          catch i8* null
  %11 = extractvalue { i8*, i32 } %10, 0
  call void @__clang_call_terminate(i8* %11) #16
  unreachable
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE7destroyIS3_EEvRS5_PT_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1) #4 comdat align 2 {
  %3 = alloca %"class.std::allocator"*, align 8
  %4 = alloca %"struct.std::pair"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %3, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %4, align 8
  %5 = load %"class.std::allocator"*, %"class.std::allocator"** %3, align 8
  %6 = bitcast %"class.std::allocator"* %5 to %"class.std::__new_allocator"*
  %7 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE7destroyIS3_EEvPT_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::pair"* noundef %7) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to %"class.std::allocator"*
  ret %"class.std::allocator"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"struct.std::_Rb_tree_node"* %0, %"struct.std::_Rb_tree_node"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node", %"struct.std::_Rb_tree_node"* %3, i32 0, i32 1
  %5 = call noundef %"struct.std::pair"* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKcdEE6_M_ptrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %4) #3
  ret %"struct.std::pair"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE7destroyIS3_EEvPT_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1) #4 comdat align 2 {
  %3 = alloca %"class.std::__new_allocator"*, align 8
  %4 = alloca %"struct.std::pair"*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %3, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %4, align 8
  %5 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %3, align 8
  %6 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair"* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKcdEE6_M_ptrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"struct.__gnu_cxx::__aligned_membuf"*, align 8
  store %"struct.__gnu_cxx::__aligned_membuf"* %0, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %3 = load %"struct.__gnu_cxx::__aligned_membuf"*, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %4 = call noundef i8* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKcdEE7_M_addrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %3) #3
  %5 = bitcast i8* %4 to %"struct.std::pair"*
  ret %"struct.std::pair"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef i8* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKcdEE7_M_addrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"struct.__gnu_cxx::__aligned_membuf"*, align 8
  store %"struct.__gnu_cxx::__aligned_membuf"* %0, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %3 = load %"struct.__gnu_cxx::__aligned_membuf"*, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %4 = getelementptr inbounds %"struct.__gnu_cxx::__aligned_membuf", %"struct.__gnu_cxx::__aligned_membuf"* %3, i32 0, i32 0
  %5 = bitcast [16 x i8]* %4 to i8*
  ret i8* %5
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE10deallocateERS5_PS4_m(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::_Rb_tree_node"* noundef %1, i64 noundef %2) #8 comdat align 2 {
  %4 = alloca %"class.std::allocator"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %6 = alloca i64, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %4, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %5, align 8
  store i64 %2, i64* %6, align 8
  %7 = load %"class.std::allocator"*, %"class.std::allocator"** %4, align 8
  %8 = bitcast %"class.std::allocator"* %7 to %"class.std::__new_allocator"*
  %9 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %10 = load i64, i64* %6, align 8
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE10deallocateEPS4_m(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %8, %"struct.std::_Rb_tree_node"* noundef %9, i64 noundef %10)
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE10deallocateEPS4_m(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::_Rb_tree_node"* noundef %1, i64 noundef %2) #4 comdat align 2 {
  %4 = alloca %"class.std::__new_allocator"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %6 = alloca i64, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %4, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %5, align 8
  store i64 %2, i64* %6, align 8
  %7 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %4, align 8
  %8 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %9 = bitcast %"struct.std::_Rb_tree_node"* %8 to i8*
  call void @_ZdlPv(i8* noundef %9) #17
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8* noundef) #11

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %8, i32 0, i32 1
  %10 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %11 = bitcast %"struct.std::_Rb_tree_node_base"* %10 to %"struct.std::_Rb_tree_node"*
  ret %"struct.std::_Rb_tree_node"* %11
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator"*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %2, align 8
  %3 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %2, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define internal %"struct.std::_Rb_tree_node_base"* @"_ZSt13__max_elementISt23_Rb_tree_const_iteratorISt4pairIKcdEEN9__gnu_cxx5__ops15_Iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaIS3_EEE3$_0EEET_SH_SH_T0_"(%"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"* %1) #8 {
  %3 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %4 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %5 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %6 = alloca %"struct.__gnu_cxx::__ops::_Iter_comp_iter", align 1
  %7 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %8 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %9 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %4, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %10, align 8
  %11 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %11, align 8
  %12 = call noundef zeroext i1 @_ZSteqRKSt23_Rb_tree_const_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %4, %"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  br i1 %12, label %13, label %16

13:                                               ; preds = %2
  %14 = bitcast %"struct.std::_Rb_tree_const_iterator"* %3 to i8*
  %15 = bitcast %"struct.std::_Rb_tree_const_iterator"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %14, i8* align 8 %15, i64 8, i1 false)
  br label %39

16:                                               ; preds = %2
  %17 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  %18 = bitcast %"struct.std::_Rb_tree_const_iterator"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %17, i8* align 8 %18, i64 8, i1 false)
  br label %19

19:                                               ; preds = %35, %16
  %20 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_const_iterator"* @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEppEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %4) #3
  %21 = call noundef zeroext i1 @_ZStneRKSt23_Rb_tree_const_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %20, %"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  br i1 %21, label %22, label %36

22:                                               ; preds = %19
  %23 = bitcast %"struct.std::_Rb_tree_const_iterator"* %8 to i8*
  %24 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %23, i8* align 8 %24, i64 8, i1 false)
  %25 = bitcast %"struct.std::_Rb_tree_const_iterator"* %9 to i8*
  %26 = bitcast %"struct.std::_Rb_tree_const_iterator"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %25, i8* align 8 %26, i64 8, i1 false)
  %27 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %8, i32 0, i32 0
  %28 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %27, align 8
  %29 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %9, i32 0, i32 0
  %30 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %29, align 8
  %31 = call noundef zeroext i1 @"_ZN9__gnu_cxx5__ops15_Iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EclISt23_Rb_tree_const_iteratorIS7_ESG_EEbT_T0_"(%"struct.__gnu_cxx::__ops::_Iter_comp_iter"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::_Rb_tree_node_base"* %28, %"struct.std::_Rb_tree_node_base"* %30)
  br i1 %31, label %32, label %35

32:                                               ; preds = %22
  %33 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  %34 = bitcast %"struct.std::_Rb_tree_const_iterator"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %33, i8* align 8 %34, i64 8, i1 false)
  br label %35

35:                                               ; preds = %32, %22
  br label %19, !llvm.loop !15

36:                                               ; preds = %19
  %37 = bitcast %"struct.std::_Rb_tree_const_iterator"* %3 to i8*
  %38 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %37, i8* align 8 %38, i64 8, i1 false)
  br label %39

39:                                               ; preds = %36, %13
  %40 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %3, i32 0, i32 0
  %41 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %40, align 8
  ret %"struct.std::_Rb_tree_node_base"* %41
}

; Function Attrs: mustprogress noinline optnone uwtable
define internal void @"_ZN9__gnu_cxx5__ops16__iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EENS0_15_Iter_comp_iterIT_EESE_"() #8 {
  %1 = alloca %"struct.__gnu_cxx::__ops::_Iter_comp_iter", align 1
  %2 = alloca %class.anon, align 1
  %3 = alloca %class.anon, align 1
  %4 = call noundef nonnull align 1 dereferenceable(1) %class.anon* @"_ZSt4moveIRZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EONSt16remove_referenceIT_E4typeEOSD_"(%class.anon* noundef nonnull align 1 dereferenceable(1) %2) #3
  call void @"_ZN9__gnu_cxx5__ops15_Iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EC2ESC_"(%"struct.__gnu_cxx::__ops::_Iter_comp_iter"* noundef nonnull align 1 dereferenceable(1) %1)
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef zeroext i1 @_ZSteqRKSt23_Rb_tree_const_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %1) #4 comdat {
  %3 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %1, %"struct.std::_Rb_tree_const_iterator"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  %7 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %6, align 8
  %8 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %4, align 8
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %8, i32 0, i32 0
  %10 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %11 = icmp eq %"struct.std::_Rb_tree_node_base"* %7, %10
  ret i1 %11
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef zeroext i1 @_ZStneRKSt23_Rb_tree_const_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %1) #4 comdat {
  %3 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %1, %"struct.std::_Rb_tree_const_iterator"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  %7 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %6, align 8
  %8 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %4, align 8
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %8, i32 0, i32 0
  %10 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %11 = icmp ne %"struct.std::_Rb_tree_node_base"* %7, %10
  ret i1 %11
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_const_iterator"* @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEppEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %3, i32 0, i32 0
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %5) #18
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %3, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %7, align 8
  ret %"struct.std::_Rb_tree_const_iterator"* %3
}

; Function Attrs: mustprogress noinline optnone uwtable
define internal noundef zeroext i1 @"_ZN9__gnu_cxx5__ops15_Iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EclISt23_Rb_tree_const_iteratorIS7_ESG_EEbT_T0_"(%"struct.__gnu_cxx::__ops::_Iter_comp_iter"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"* %2) #8 align 2 {
  %4 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %5 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %6 = alloca %"struct.__gnu_cxx::__ops::_Iter_comp_iter"*, align 8
  %7 = alloca %"struct.std::pair.3", align 8
  %8 = alloca %"struct.std::pair.3", align 8
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %4, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %2, %"struct.std::_Rb_tree_node_base"** %10, align 8
  store %"struct.__gnu_cxx::__ops::_Iter_comp_iter"* %0, %"struct.__gnu_cxx::__ops::_Iter_comp_iter"** %6, align 8
  %11 = load %"struct.__gnu_cxx::__ops::_Iter_comp_iter"*, %"struct.__gnu_cxx::__ops::_Iter_comp_iter"** %6, align 8
  %12 = getelementptr inbounds %"struct.__gnu_cxx::__ops::_Iter_comp_iter", %"struct.__gnu_cxx::__ops::_Iter_comp_iter"* %11, i32 0, i32 0
  %13 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %4) #3
  call void @_ZNSt4pairIcdEC2IKcdLb1EEERKS_IT_T0_E(%"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %7, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %13)
  %14 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  call void @_ZNSt4pairIcdEC2IKcdLb1EEERKS_IT_T0_E(%"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %8, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %14)
  %15 = call noundef zeroext i1 @"_ZZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEENK3$_0clERKS2_IcdESC_"(%class.anon* noundef nonnull align 1 dereferenceable(1) %12, %"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %7, %"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %8)
  ret i1 %15
}

; Function Attrs: nounwind readonly willreturn
declare noundef %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef) #12

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define internal noundef zeroext i1 @"_ZZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEENK3$_0clERKS2_IcdESC_"(%class.anon* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %1, %"struct.std::pair.3"* noundef nonnull align 8 dereferenceable(16) %2) #4 align 2 {
  %4 = alloca %class.anon*, align 8
  %5 = alloca %"struct.std::pair.3"*, align 8
  %6 = alloca %"struct.std::pair.3"*, align 8
  store %class.anon* %0, %class.anon** %4, align 8
  store %"struct.std::pair.3"* %1, %"struct.std::pair.3"** %5, align 8
  store %"struct.std::pair.3"* %2, %"struct.std::pair.3"** %6, align 8
  %7 = load %class.anon*, %class.anon** %4, align 8
  %8 = load %"struct.std::pair.3"*, %"struct.std::pair.3"** %5, align 8
  %9 = getelementptr inbounds %"struct.std::pair.3", %"struct.std::pair.3"* %8, i32 0, i32 1
  %10 = load double, double* %9, align 8
  %11 = load %"struct.std::pair.3"*, %"struct.std::pair.3"** %6, align 8
  %12 = getelementptr inbounds %"struct.std::pair.3", %"struct.std::pair.3"* %11, i32 0, i32 1
  %13 = load double, double* %12, align 8
  %14 = fcmp olt double %10, %13
  ret i1 %14
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define internal noundef nonnull align 1 dereferenceable(1) %class.anon* @"_ZSt4moveIRZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EONSt16remove_referenceIT_E4typeEOSD_"(%class.anon* noundef nonnull align 1 dereferenceable(1) %0) #4 {
  %2 = alloca %class.anon*, align 8
  store %class.anon* %0, %class.anon** %2, align 8
  %3 = load %class.anon*, %class.anon** %2, align 8
  ret %class.anon* %3
}

; Function Attrs: noinline nounwind optnone uwtable
define internal void @"_ZN9__gnu_cxx5__ops15_Iter_comp_iterIZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EC2ESC_"(%"struct.__gnu_cxx::__ops::_Iter_comp_iter"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 align 2 {
  %2 = alloca %class.anon, align 1
  %3 = alloca %"struct.__gnu_cxx::__ops::_Iter_comp_iter"*, align 8
  store %"struct.__gnu_cxx::__ops::_Iter_comp_iter"* %0, %"struct.__gnu_cxx::__ops::_Iter_comp_iter"** %3, align 8
  %4 = load %"struct.__gnu_cxx::__ops::_Iter_comp_iter"*, %"struct.__gnu_cxx::__ops::_Iter_comp_iter"** %3, align 8
  %5 = getelementptr inbounds %"struct.__gnu_cxx::__ops::_Iter_comp_iter", %"struct.__gnu_cxx::__ops::_Iter_comp_iter"* %4, i32 0, i32 0
  %6 = call noundef nonnull align 1 dereferenceable(1) %class.anon* @"_ZSt4moveIRZ7get_maxRKSt3mapIcdSt4lessIcESaISt4pairIKcdEEEE3$_0EONSt16remove_referenceIT_E4typeEOSD_"(%class.anon* noundef nonnull align 1 dereferenceable(1) %2) #3
  ret void
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2ERKS8_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %1) unnamed_addr #7 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca i8*, align 8
  %6 = alloca i32, align 4
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"class.std::_Rb_tree"* %1, %"class.std::_Rb_tree"** %4, align 8
  %7 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %8 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %7, i32 0, i32 0
  %9 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %10 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %9, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2ERKSA_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %8, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %10)
  %11 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %12 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_rootEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %11) #3
  %13 = icmp ne %"struct.std::_Rb_tree_node_base"* %12, null
  br i1 %13, label %14, label %24

14:                                               ; preds = %2
  %15 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %16 = invoke noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyERKS8_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %7, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %15)
          to label %17 unwind label %20

17:                                               ; preds = %14
  %18 = bitcast %"struct.std::_Rb_tree_node"* %16 to %"struct.std::_Rb_tree_node_base"*
  %19 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_rootEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %7) #3
  store %"struct.std::_Rb_tree_node_base"* %18, %"struct.std::_Rb_tree_node_base"** %19, align 8
  br label %24

20:                                               ; preds = %14
  %21 = landingpad { i8*, i32 }
          cleanup
  %22 = extractvalue { i8*, i32 } %21, 0
  store i8* %22, i8** %5, align 8
  %23 = extractvalue { i8*, i32 } %21, 1
  store i32 %23, i32* %6, align 4
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %8) #3
  br label %25

24:                                               ; preds = %17, %2
  ret void

25:                                               ; preds = %20
  %26 = load i8*, i8** %5, align 8
  %27 = load i32, i32* %6, align 4
  %28 = insertvalue { i8*, i32 } undef, i8* %26, 0
  %29 = insertvalue { i8*, i32 } %28, i32 %27, 1
  resume { i8*, i32 } %29
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2ERKSA_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %1) unnamed_addr #7 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, align 8
  %4 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, align 8
  %5 = alloca %"class.std::allocator", align 1
  %6 = alloca i8*, align 8
  %7 = alloca i32, align 4
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %3, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %1, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %4, align 8
  %8 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %3, align 8
  %9 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %8 to %"class.std::allocator"*
  %10 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %4, align 8
  %11 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %10 to %"class.std::allocator"*
  call void @_ZN9__gnu_cxx14__alloc_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEES5_E17_S_select_on_copyERKS6_(%"class.std::allocator"* sret(%"class.std::allocator") align 1 %5, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %11)
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS4_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %9, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %5) #3
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %5) #3
  %12 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %8 to %"struct.std::_Rb_tree_key_compare"*
  %13 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %4, align 8
  %14 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %13 to %"struct.std::_Rb_tree_key_compare"*
  %15 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %14, i32 0, i32 0
  invoke void @_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2ERKS1_(%"struct.std::_Rb_tree_key_compare"* noundef nonnull align 1 dereferenceable(1) %12, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %15)
          to label %16 unwind label %20

16:                                               ; preds = %2
  %17 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %8 to i8*
  %18 = getelementptr inbounds i8, i8* %17, i64 8
  %19 = bitcast i8* %18 to %"struct.std::_Rb_tree_header"*
  call void @_ZNSt15_Rb_tree_headerC2Ev(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %19) #3
  ret void

20:                                               ; preds = %2
  %21 = landingpad { i8*, i32 }
          cleanup
  %22 = extractvalue { i8*, i32 } %21, 0
  store i8* %22, i8** %6, align 8
  %23 = extractvalue { i8*, i32 } %21, 1
  store i32 %23, i32* %7, align 4
  %24 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %8 to %"class.std::allocator"*
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %24) #3
  br label %25

25:                                               ; preds = %20
  %26 = load i8*, i8** %6, align 8
  %27 = load i32, i32* %7, align 4
  %28 = insertvalue { i8*, i32 } undef, i8* %26, 0
  %29 = insertvalue { i8*, i32 } %28, i32 %27, 1
  resume { i8*, i32 } %29
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node_base"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_rootEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %8, i32 0, i32 1
  %10 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  ret %"struct.std::_Rb_tree_node_base"* %10
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyERKS8_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %1) #8 comdat align 2 {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node", align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"class.std::_Rb_tree"* %1, %"class.std::_Rb_tree"** %4, align 8
  %6 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeC2ERS8_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %5, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6)
  %7 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %8 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ERKS8_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %7, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %5)
  ret %"struct.std::_Rb_tree_node"* %8
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_rootEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %8, i32 0, i32 1
  ret %"struct.std::_Rb_tree_node_base"** %9
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZN9__gnu_cxx14__alloc_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEES5_E17_S_select_on_copyERKS6_(%"class.std::allocator"* noalias sret(%"class.std::allocator") align 1 %0, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %1) #8 comdat align 2 {
  %3 = alloca i8*, align 8
  %4 = alloca %"class.std::allocator"*, align 8
  %5 = bitcast %"class.std::allocator"* %0 to i8*
  store i8* %5, i8** %3, align 8
  store %"class.std::allocator"* %1, %"class.std::allocator"** %4, align 8
  %6 = load %"class.std::allocator"*, %"class.std::allocator"** %4, align 8
  call void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE37select_on_container_copy_constructionERKS5_(%"class.std::allocator"* sret(%"class.std::allocator") align 1 %0, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %6)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::allocator"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %2, align 8
  %3 = load %"class.std::allocator"*, %"class.std::allocator"** %2, align 8
  %4 = bitcast %"class.std::allocator"* %3 to %"class.std::__new_allocator"*
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2ERKS1_(%"struct.std::_Rb_tree_key_compare"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree_key_compare"*, align 8
  %4 = alloca %"struct.std::less"*, align 8
  store %"struct.std::_Rb_tree_key_compare"* %0, %"struct.std::_Rb_tree_key_compare"** %3, align 8
  store %"struct.std::less"* %1, %"struct.std::less"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_key_compare"*, %"struct.std::_Rb_tree_key_compare"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %5, i32 0, i32 0
  %7 = load %"struct.std::less"*, %"struct.std::less"** %4, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15_Rb_tree_headerC2Ev(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %0) unnamed_addr #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %2 = alloca %"struct.std::_Rb_tree_header"*, align 8
  store %"struct.std::_Rb_tree_header"* %0, %"struct.std::_Rb_tree_header"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_header"*, %"struct.std::_Rb_tree_header"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %5, i32 0, i32 0
  store i32 0, i32* %6, align 8
  invoke void @_ZNSt15_Rb_tree_header8_M_resetEv(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %3)
          to label %7 unwind label %8

7:                                                ; preds = %1
  ret void

8:                                                ; preds = %1
  %9 = landingpad { i8*, i32 }
          catch i8* null
  %10 = extractvalue { i8*, i32 } %9, 0
  call void @__clang_call_terminate(i8* %10) #16
  unreachable
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE37select_on_container_copy_constructionERKS5_(%"class.std::allocator"* noalias sret(%"class.std::allocator") align 1 %0, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %1) #4 comdat align 2 {
  %3 = alloca i8*, align 8
  %4 = alloca %"class.std::allocator"*, align 8
  %5 = bitcast %"class.std::allocator"* %0 to i8*
  store i8* %5, i8** %3, align 8
  store %"class.std::allocator"* %1, %"class.std::allocator"** %4, align 8
  %6 = load %"class.std::allocator"*, %"class.std::allocator"** %4, align 8
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS4_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %6) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS4_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::allocator"*, align 8
  %4 = alloca %"class.std::allocator"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %3, align 8
  store %"class.std::allocator"* %1, %"class.std::allocator"** %4, align 8
  %5 = load %"class.std::allocator"*, %"class.std::allocator"** %3, align 8
  %6 = bitcast %"class.std::allocator"* %5 to %"class.std::__new_allocator"*
  %7 = load %"class.std::allocator"*, %"class.std::allocator"** %4, align 8
  %8 = bitcast %"class.std::allocator"* %7 to %"class.std::__new_allocator"*
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS5_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %6, %"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %8) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS5_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::__new_allocator"*, align 8
  %4 = alloca %"class.std::__new_allocator"*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %3, align 8
  store %"class.std::__new_allocator"* %1, %"class.std::__new_allocator"** %4, align 8
  %5 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %3, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15_Rb_tree_header8_M_resetEv(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_header"*, align 8
  store %"struct.std::_Rb_tree_header"* %0, %"struct.std::_Rb_tree_header"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_header"*, %"struct.std::_Rb_tree_header"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %4, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %5, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %7, i32 0, i32 2
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 0
  %11 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %10, i32 0, i32 3
  store %"struct.std::_Rb_tree_node_base"* %9, %"struct.std::_Rb_tree_node_base"** %11, align 8
  %12 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %3, i32 0, i32 1
  store i64 0, i64* %12, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeC2ERS8_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %0, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %3, align 8
  store %"class.std::_Rb_tree"* %1, %"class.std::_Rb_tree"** %4, align 8
  %5 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %5, i32 0, i32 0
  %7 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  store %"class.std::_Rb_tree"* %7, %"class.std::_Rb_tree"** %6, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ERKS8_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %1, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %2) #8 comdat align 2 {
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca %"class.std::_Rb_tree"*, align 8
  %6 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store %"class.std::_Rb_tree"* %1, %"class.std::_Rb_tree"** %5, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %2, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %6, align 8
  %8 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %9 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %5, align 8
  %10 = call noundef %"struct.std::_Rb_tree_node"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %9) #3
  %11 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %8) #3
  %12 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %6, align 8
  %13 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_PSt18_Rb_tree_node_baseRT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %8, %"struct.std::_Rb_tree_node"* noundef %10, %"struct.std::_Rb_tree_node_base"* noundef %11, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %12)
  store %"struct.std::_Rb_tree_node"* %13, %"struct.std::_Rb_tree_node"** %7, align 8
  %14 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %15 = bitcast %"struct.std::_Rb_tree_node"* %14 to %"struct.std::_Rb_tree_node_base"*
  %16 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %15) #3
  %17 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_leftmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %8) #3
  store %"struct.std::_Rb_tree_node_base"* %16, %"struct.std::_Rb_tree_node_base"** %17, align 8
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %19 = bitcast %"struct.std::_Rb_tree_node"* %18 to %"struct.std::_Rb_tree_node_base"*
  %20 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %19) #3
  %21 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %8) #3
  store %"struct.std::_Rb_tree_node_base"* %20, %"struct.std::_Rb_tree_node_base"** %21, align 8
  %22 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %5, align 8
  %23 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %22, i32 0, i32 0
  %24 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %23 to i8*
  %25 = getelementptr inbounds i8, i8* %24, i64 8
  %26 = bitcast i8* %25 to %"struct.std::_Rb_tree_header"*
  %27 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %26, i32 0, i32 1
  %28 = load i64, i64* %27, align 8
  %29 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %8, i32 0, i32 0
  %30 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %29 to i8*
  %31 = getelementptr inbounds i8, i8* %30, i64 8
  %32 = bitcast i8* %31 to %"struct.std::_Rb_tree_header"*
  %33 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %32, i32 0, i32 1
  store i64 %28, i64* %33, align 8
  %34 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  ret %"struct.std::_Rb_tree_node"* %34
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_PSt18_Rb_tree_node_baseRT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1, %"struct.std::_Rb_tree_node_base"* noundef %2, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %3) #8 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %5 = alloca %"class.std::_Rb_tree"*, align 8
  %6 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %8 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %9 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %10 = alloca i8*, align 8
  %11 = alloca i32, align 4
  %12 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %5, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %6, align 8
  store %"struct.std::_Rb_tree_node_base"* %2, %"struct.std::_Rb_tree_node_base"** %7, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %3, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %8, align 8
  %13 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %5, align 8
  %14 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %15 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %8, align 8
  %16 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_M_clone_nodeILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %14, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %15)
  store %"struct.std::_Rb_tree_node"* %16, %"struct.std::_Rb_tree_node"** %9, align 8
  %17 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %19 = bitcast %"struct.std::_Rb_tree_node"* %18 to %"struct.std::_Rb_tree_node_base"*
  %20 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %19, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* %17, %"struct.std::_Rb_tree_node_base"** %20, align 8
  %21 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %22 = bitcast %"struct.std::_Rb_tree_node"* %21 to %"struct.std::_Rb_tree_node_base"*
  %23 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %22, i32 0, i32 3
  %24 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %23, align 8
  %25 = icmp ne %"struct.std::_Rb_tree_node_base"* %24, null
  br i1 %25, label %26, label %48

26:                                               ; preds = %4
  %27 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %28 = bitcast %"struct.std::_Rb_tree_node"* %27 to %"struct.std::_Rb_tree_node_base"*
  %29 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %28) #3
  %30 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %31 = bitcast %"struct.std::_Rb_tree_node"* %30 to %"struct.std::_Rb_tree_node_base"*
  %32 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %8, align 8
  %33 = invoke noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_PSt18_Rb_tree_node_baseRT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %29, %"struct.std::_Rb_tree_node_base"* noundef %31, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %32)
          to label %34 unwind label %39

34:                                               ; preds = %26
  %35 = bitcast %"struct.std::_Rb_tree_node"* %33 to %"struct.std::_Rb_tree_node_base"*
  %36 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %37 = bitcast %"struct.std::_Rb_tree_node"* %36 to %"struct.std::_Rb_tree_node_base"*
  %38 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %37, i32 0, i32 3
  store %"struct.std::_Rb_tree_node_base"* %35, %"struct.std::_Rb_tree_node_base"** %38, align 8
  br label %48

39:                                               ; preds = %75, %57, %26
  %40 = landingpad { i8*, i32 }
          catch i8* null
  %41 = extractvalue { i8*, i32 } %40, 0
  store i8* %41, i8** %10, align 8
  %42 = extractvalue { i8*, i32 } %40, 1
  store i32 %42, i32* %11, align 4
  br label %43

43:                                               ; preds = %39
  %44 = load i8*, i8** %10, align 8
  %45 = call i8* @__cxa_begin_catch(i8* %44) #3
  %46 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  invoke void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %46)
          to label %47 unwind label %95

47:                                               ; preds = %43
  invoke void @__cxa_rethrow() #19
          to label %110 unwind label %95

48:                                               ; preds = %34, %4
  %49 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %50 = bitcast %"struct.std::_Rb_tree_node"* %49 to %"struct.std::_Rb_tree_node_base"*
  store %"struct.std::_Rb_tree_node_base"* %50, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %51 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %52 = bitcast %"struct.std::_Rb_tree_node"* %51 to %"struct.std::_Rb_tree_node_base"*
  %53 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %52) #3
  store %"struct.std::_Rb_tree_node"* %53, %"struct.std::_Rb_tree_node"** %6, align 8
  br label %54

54:                                               ; preds = %88, %48
  %55 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %56 = icmp ne %"struct.std::_Rb_tree_node"* %55, null
  br i1 %56, label %57, label %94

57:                                               ; preds = %54
  %58 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %59 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %8, align 8
  %60 = invoke noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_M_clone_nodeILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %58, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %59)
          to label %61 unwind label %39

61:                                               ; preds = %57
  store %"struct.std::_Rb_tree_node"* %60, %"struct.std::_Rb_tree_node"** %12, align 8
  %62 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %12, align 8
  %63 = bitcast %"struct.std::_Rb_tree_node"* %62 to %"struct.std::_Rb_tree_node_base"*
  %64 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %65 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %64, i32 0, i32 2
  store %"struct.std::_Rb_tree_node_base"* %63, %"struct.std::_Rb_tree_node_base"** %65, align 8
  %66 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %67 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %12, align 8
  %68 = bitcast %"struct.std::_Rb_tree_node"* %67 to %"struct.std::_Rb_tree_node_base"*
  %69 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %68, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* %66, %"struct.std::_Rb_tree_node_base"** %69, align 8
  %70 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %71 = bitcast %"struct.std::_Rb_tree_node"* %70 to %"struct.std::_Rb_tree_node_base"*
  %72 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %71, i32 0, i32 3
  %73 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %72, align 8
  %74 = icmp ne %"struct.std::_Rb_tree_node_base"* %73, null
  br i1 %74, label %75, label %88

75:                                               ; preds = %61
  %76 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %77 = bitcast %"struct.std::_Rb_tree_node"* %76 to %"struct.std::_Rb_tree_node_base"*
  %78 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %77) #3
  %79 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %12, align 8
  %80 = bitcast %"struct.std::_Rb_tree_node"* %79 to %"struct.std::_Rb_tree_node_base"*
  %81 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %8, align 8
  %82 = invoke noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_M_copyILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_PSt18_Rb_tree_node_baseRT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %78, %"struct.std::_Rb_tree_node_base"* noundef %80, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %81)
          to label %83 unwind label %39

83:                                               ; preds = %75
  %84 = bitcast %"struct.std::_Rb_tree_node"* %82 to %"struct.std::_Rb_tree_node_base"*
  %85 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %12, align 8
  %86 = bitcast %"struct.std::_Rb_tree_node"* %85 to %"struct.std::_Rb_tree_node_base"*
  %87 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %86, i32 0, i32 3
  store %"struct.std::_Rb_tree_node_base"* %84, %"struct.std::_Rb_tree_node_base"** %87, align 8
  br label %88

88:                                               ; preds = %83, %61
  %89 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %12, align 8
  %90 = bitcast %"struct.std::_Rb_tree_node"* %89 to %"struct.std::_Rb_tree_node_base"*
  store %"struct.std::_Rb_tree_node_base"* %90, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %91 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %92 = bitcast %"struct.std::_Rb_tree_node"* %91 to %"struct.std::_Rb_tree_node_base"*
  %93 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %92) #3
  store %"struct.std::_Rb_tree_node"* %93, %"struct.std::_Rb_tree_node"** %6, align 8
  br label %54, !llvm.loop !16

94:                                               ; preds = %54
  br label %100

95:                                               ; preds = %47, %43
  %96 = landingpad { i8*, i32 }
          cleanup
  %97 = extractvalue { i8*, i32 } %96, 0
  store i8* %97, i8** %10, align 8
  %98 = extractvalue { i8*, i32 } %96, 1
  store i32 %98, i32* %11, align 4
  invoke void @__cxa_end_catch()
          to label %99 unwind label %107

99:                                               ; preds = %95
  br label %102

100:                                              ; preds = %94
  %101 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  ret %"struct.std::_Rb_tree_node"* %101

102:                                              ; preds = %99
  %103 = load i8*, i8** %10, align 8
  %104 = load i32, i32* %11, align 4
  %105 = insertvalue { i8*, i32 } undef, i8* %103, 0
  %106 = insertvalue { i8*, i32 } %105, i32 %104, 1
  resume { i8*, i32 } %106

107:                                              ; preds = %95
  %108 = landingpad { i8*, i32 }
          catch i8* null
  %109 = extractvalue { i8*, i32 } %108, 0
  call void @__clang_call_terminate(i8* %109) #16
  unreachable

110:                                              ; preds = %47
  unreachable
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  ret %"struct.std::_Rb_tree_node_base"* %8
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_S_minimumEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt18_Rb_tree_node_base10_S_minimumEPS_(%"struct.std::_Rb_tree_node_base"* noundef %3) #3
  ret %"struct.std::_Rb_tree_node_base"* %4
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_leftmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %8, i32 0, i32 2
  ret %"struct.std::_Rb_tree_node_base"** %9
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_S_maximumEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt18_Rb_tree_node_base10_S_maximumEPS_(%"struct.std::_Rb_tree_node_base"* noundef %3) #3
  ret %"struct.std::_Rb_tree_node_base"* %4
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %8, i32 0, i32 3
  ret %"struct.std::_Rb_tree_node_base"** %9
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_M_clone_nodeILb0ENS8_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS2_ESD_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %2) #8 comdat align 2 {
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %6 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %5, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %2, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %6, align 8
  %8 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %9 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %6, align 8
  %10 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %11 = call noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %10)
  %12 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %11) #3
  %13 = call noundef %"struct.std::_Rb_tree_node"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeclIRKS2_EEPSt13_Rb_tree_nodeIS2_EOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %9, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %12)
  store %"struct.std::_Rb_tree_node"* %13, %"struct.std::_Rb_tree_node"** %7, align 8
  %14 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %15 = bitcast %"struct.std::_Rb_tree_node"* %14 to %"struct.std::_Rb_tree_node_base"*
  %16 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %15, i32 0, i32 0
  %17 = load i32, i32* %16, align 8
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %19 = bitcast %"struct.std::_Rb_tree_node"* %18 to %"struct.std::_Rb_tree_node_base"*
  %20 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %19, i32 0, i32 0
  store i32 %17, i32* %20, align 8
  %21 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %22 = bitcast %"struct.std::_Rb_tree_node"* %21 to %"struct.std::_Rb_tree_node_base"*
  %23 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %22, i32 0, i32 2
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %23, align 8
  %24 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %25 = bitcast %"struct.std::_Rb_tree_node"* %24 to %"struct.std::_Rb_tree_node_base"*
  %26 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %25, i32 0, i32 3
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %26, align 8
  %27 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  ret %"struct.std::_Rb_tree_node"* %27
}

declare void @__cxa_rethrow()

declare void @__cxa_end_catch()

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeclIRKS2_EEPSt13_Rb_tree_nodeIS2_EOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %1) #8 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %4 = alloca %"struct.std::pair"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %3, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %4, align 8
  %5 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %5, i32 0, i32 0
  %7 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %6, align 8
  %8 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  %9 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %8) #3
  %10 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %7, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %9)
  ret %"struct.std::_Rb_tree_node"* %10
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat {
  %2 = alloca %"struct.std::pair"*, align 8
  store %"struct.std::pair"* %0, %"struct.std::pair"** %2, align 8
  %3 = load %"struct.std::pair"*, %"struct.std::pair"** %2, align 8
  ret %"struct.std::pair"* %3
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKS2_EEEPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %1) #8 comdat align 2 {
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  %4 = alloca %"struct.std::pair"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %4, align 8
  %6 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %7 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_get_nodeEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6)
  store %"struct.std::_Rb_tree_node"* %7, %"struct.std::_Rb_tree_node"** %5, align 8
  %8 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %9 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  %10 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %9) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKS2_EEEvPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6, %"struct.std::_Rb_tree_node"* noundef %8, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %10)
  %11 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  ret %"struct.std::_Rb_tree_node"* %11
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_get_nodeEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #8 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %3) #3
  %5 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE8allocateERS5_m(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %4, i64 noundef 1)
  ret %"struct.std::_Rb_tree_node"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKS2_EEEvPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %2) #4 comdat align 2 {
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %6 = alloca %"struct.std::pair"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %5, align 8
  store %"struct.std::pair"* %2, %"struct.std::pair"** %6, align 8
  %7 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %8 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %9 = bitcast %"struct.std::_Rb_tree_node"* %8 to i8*
  %10 = bitcast i8* %9 to %"struct.std::_Rb_tree_node"*
  %11 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %7) #3
  %12 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %5, align 8
  %13 = call noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %12)
  %14 = load %"struct.std::pair"*, %"struct.std::pair"** %6, align 8
  %15 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %14) #3
  call void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKS3_EEEvRS5_PT_DpOT0_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %11, %"struct.std::pair"* noundef %13, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %15) #3
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE8allocateERS5_m(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, i64 noundef %1) #8 comdat align 2 {
  %3 = alloca %"class.std::allocator"*, align 8
  %4 = alloca i64, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load %"class.std::allocator"*, %"class.std::allocator"** %3, align 8
  %6 = bitcast %"class.std::allocator"* %5 to %"class.std::__new_allocator"*
  %7 = load i64, i64* %4, align 8
  %8 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE8allocateEmPKv(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %6, i64 noundef %7, i8* noundef null)
  ret %"struct.std::_Rb_tree_node"* %8
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE8allocateEmPKv(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, i64 noundef %1, i8* noundef %2) #8 comdat align 2 {
  %4 = alloca %"class.std::__new_allocator"*, align 8
  %5 = alloca i64, align 8
  %6 = alloca i8*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %4, align 8
  store i64 %1, i64* %5, align 8
  store i8* %2, i8** %6, align 8
  %7 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %4, align 8
  %8 = load i64, i64* %5, align 8
  %9 = call noundef i64 @_ZNKSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE11_M_max_sizeEv(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %7) #3
  %10 = icmp ugt i64 %8, %9
  br i1 %10, label %11, label %16

11:                                               ; preds = %3
  %12 = load i64, i64* %5, align 8
  %13 = icmp ugt i64 %12, 384307168202282325
  br i1 %13, label %14, label %15

14:                                               ; preds = %11
  call void @_ZSt28__throw_bad_array_new_lengthv() #19
  unreachable

15:                                               ; preds = %11
  call void @_ZSt17__throw_bad_allocv() #19
  unreachable

16:                                               ; preds = %3
  %17 = load i64, i64* %5, align 8
  %18 = mul i64 %17, 48
  %19 = call noalias noundef nonnull i8* @_Znwm(i64 noundef %18) #20
  %20 = bitcast i8* %19 to %"struct.std::_Rb_tree_node"*
  ret %"struct.std::_Rb_tree_node"* %20
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef i64 @_ZNKSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE11_M_max_sizeEv(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator"*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %2, align 8
  %3 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %2, align 8
  ret i64 192153584101141162
}

; Function Attrs: noreturn
declare void @_ZSt28__throw_bad_array_new_lengthv() #13

; Function Attrs: noreturn
declare void @_ZSt17__throw_bad_allocv() #13

; Function Attrs: nobuiltin allocsize(0)
declare noundef nonnull i8* @_Znwm(i64 noundef) #14

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKS3_EEEvRS5_PT_DpOT0_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %2) #4 comdat align 2 {
  %4 = alloca %"class.std::allocator"*, align 8
  %5 = alloca %"struct.std::pair"*, align 8
  %6 = alloca %"struct.std::pair"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %4, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %5, align 8
  store %"struct.std::pair"* %2, %"struct.std::pair"** %6, align 8
  %7 = load %"class.std::allocator"*, %"class.std::allocator"** %4, align 8
  %8 = bitcast %"class.std::allocator"* %7 to %"class.std::__new_allocator"*
  %9 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  %10 = load %"struct.std::pair"*, %"struct.std::pair"** %6, align 8
  %11 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %10) #3
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKS3_EEEvPT_DpOT0_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %8, %"struct.std::pair"* noundef %9, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %11) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKS3_EEEvPT_DpOT0_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %2) #4 comdat align 2 {
  %4 = alloca %"class.std::__new_allocator"*, align 8
  %5 = alloca %"struct.std::pair"*, align 8
  %6 = alloca %"struct.std::pair"*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %4, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %5, align 8
  store %"struct.std::pair"* %2, %"struct.std::pair"** %6, align 8
  %7 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %4, align 8
  %8 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  %9 = bitcast %"struct.std::pair"* %8 to i8*
  %10 = bitcast i8* %9 to %"struct.std::pair"*
  %11 = load %"struct.std::pair"*, %"struct.std::pair"** %6, align 8
  %12 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %11) #3
  %13 = bitcast %"struct.std::pair"* %10 to i8*
  %14 = bitcast %"struct.std::pair"* %12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %13, i8* align 8 %14, i64 16, i1 false)
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt18_Rb_tree_node_base10_S_minimumEPS_(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  br label %3

3:                                                ; preds = %8, %1
  %4 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %4, i32 0, i32 2
  %6 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %5, align 8
  %7 = icmp ne %"struct.std::_Rb_tree_node_base"* %6, null
  br i1 %7, label %8, label %12

8:                                                ; preds = %3
  %9 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %9, i32 0, i32 2
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  store %"struct.std::_Rb_tree_node_base"* %11, %"struct.std::_Rb_tree_node_base"** %2, align 8
  br label %3, !llvm.loop !17

12:                                               ; preds = %3
  %13 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  ret %"struct.std::_Rb_tree_node_base"* %13
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt18_Rb_tree_node_base10_S_maximumEPS_(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  br label %3

3:                                                ; preds = %8, %1
  %4 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %4, i32 0, i32 3
  %6 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %5, align 8
  %7 = icmp ne %"struct.std::_Rb_tree_node_base"* %6, null
  br i1 %7, label %8, label %12

8:                                                ; preds = %3
  %9 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %9, i32 0, i32 3
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  store %"struct.std::_Rb_tree_node_base"* %11, %"struct.std::_Rb_tree_node_base"** %2, align 8
  br label %3, !llvm.loop !18

12:                                               ; preds = %3
  %13 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  ret %"struct.std::_Rb_tree_node_base"* %13
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2Ev(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %3 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %4 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %3 to %"class.std::allocator"*
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %4) #3
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %3 to %"struct.std::_Rb_tree_key_compare"*
  call void @_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2Ev(%"struct.std::_Rb_tree_key_compare"* noundef nonnull align 1 dereferenceable(1) %5) #3
  %6 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %3 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 8
  %8 = bitcast i8* %7 to %"struct.std::_Rb_tree_header"*
  call void @_ZNSt15_Rb_tree_headerC2Ev(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %8) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::allocator"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %2, align 8
  %3 = load %"class.std::allocator"*, %"class.std::allocator"** %2, align 8
  %4 = bitcast %"class.std::allocator"* %3 to %"class.std::__new_allocator"*
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2Ev(%"struct.std::_Rb_tree_key_compare"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_key_compare"*, align 8
  store %"struct.std::_Rb_tree_key_compare"* %0, %"struct.std::_Rb_tree_key_compare"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_key_compare"*, %"struct.std::_Rb_tree_key_compare"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %3, i32 0, i32 0
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator"*, align 8
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %2, align 8
  %3 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %2, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2Ev(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree.11"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %2, align 8
  %3 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree.11", %"class.std::_Rb_tree.11"* %3, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %3 = load %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %4 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %3 to %"class.std::allocator.12"*
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKccEEEC2Ev(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %4) #3
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %3 to %"struct.std::_Rb_tree_key_compare"*
  call void @_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2Ev(%"struct.std::_Rb_tree_key_compare"* noundef nonnull align 1 dereferenceable(1) %5) #3
  %6 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %3 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 8
  %8 = bitcast i8* %7 to %"struct.std::_Rb_tree_header"*
  call void @_ZNSt15_Rb_tree_headerC2Ev(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %8) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt13_Rb_tree_nodeISt4pairIKccEEEC2Ev(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::allocator.12"*, align 8
  store %"class.std::allocator.12"* %0, %"class.std::allocator.12"** %2, align 8
  %3 = load %"class.std::allocator.12"*, %"class.std::allocator.12"** %2, align 8
  %4 = bitcast %"class.std::allocator.12"* %3 to %"class.std::__new_allocator.13"*
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEEC2Ev(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEEC2Ev(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator.13"*, align 8
  store %"class.std::__new_allocator.13"* %0, %"class.std::__new_allocator.13"** %2, align 8
  %3 = load %"class.std::__new_allocator.13"*, %"class.std::__new_allocator.13"** %2, align 8
  ret void
}

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #15

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EED2Ev(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %2 = alloca %"class.std::_Rb_tree.11"*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i32, align 4
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %2, align 8
  %5 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %2, align 8
  %6 = call noundef %"struct.std::_Rb_tree_node.15"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %5) #3
  invoke void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %5, %"struct.std::_Rb_tree_node.15"* noundef %6)
          to label %7 unwind label %9

7:                                                ; preds = %1
  %8 = getelementptr inbounds %"class.std::_Rb_tree.11", %"class.std::_Rb_tree.11"* %5, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %8) #3
  ret void

9:                                                ; preds = %1
  %10 = landingpad { i8*, i32 }
          catch i8* null
  %11 = extractvalue { i8*, i32 } %10, 0
  store i8* %11, i8** %3, align 8
  %12 = extractvalue { i8*, i32 } %10, 1
  store i32 %12, i32* %4, align 4
  %13 = getelementptr inbounds %"class.std::_Rb_tree.11", %"class.std::_Rb_tree.11"* %5, i32 0, i32 0
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %13) #3
  br label %14

14:                                               ; preds = %9
  %15 = load i8*, i8** %3, align 8
  call void @__clang_call_terminate(i8* %15) #16
  unreachable
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node.15"* noundef %1) #8 comdat align 2 {
  %3 = alloca %"class.std::_Rb_tree.11"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %3, align 8
  store %"struct.std::_Rb_tree_node.15"* %1, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %6 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %3, align 8
  br label %7

7:                                                ; preds = %10, %2
  %8 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %9 = icmp ne %"struct.std::_Rb_tree_node.15"* %8, null
  br i1 %9, label %10, label %19

10:                                               ; preds = %7
  %11 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %12 = bitcast %"struct.std::_Rb_tree_node.15"* %11 to %"struct.std::_Rb_tree_node_base"*
  %13 = call noundef %"struct.std::_Rb_tree_node.15"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %12) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %6, %"struct.std::_Rb_tree_node.15"* noundef %13)
  %14 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %15 = bitcast %"struct.std::_Rb_tree_node.15"* %14 to %"struct.std::_Rb_tree_node_base"*
  %16 = call noundef %"struct.std::_Rb_tree_node.15"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %15) #3
  store %"struct.std::_Rb_tree_node.15"* %16, %"struct.std::_Rb_tree_node.15"** %5, align 8
  %17 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %6, %"struct.std::_Rb_tree_node.15"* noundef %17) #3
  %18 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %5, align 8
  store %"struct.std::_Rb_tree_node.15"* %18, %"struct.std::_Rb_tree_node.15"** %4, align 8
  br label %7, !llvm.loop !19

19:                                               ; preds = %7
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node.15"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree.11"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %2, align 8
  %3 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %2, align 8
  %4 = call noundef %"struct.std::_Rb_tree_node.15"* @_ZNKSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %3) #3
  ret %"struct.std::_Rb_tree_node.15"* %4
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EED2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %3 = load %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"** %2, align 8
  %4 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %3 to %"class.std::allocator.12"*
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKccEEED2Ev(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node.15"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %3, i32 0, i32 3
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %5 to %"struct.std::_Rb_tree_node.15"*
  ret %"struct.std::_Rb_tree_node.15"* %6
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node.15"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %3, i32 0, i32 2
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %5 to %"struct.std::_Rb_tree_node.15"*
  ret %"struct.std::_Rb_tree_node.15"* %6
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node.15"* noundef %1) #4 comdat align 2 {
  %3 = alloca %"class.std::_Rb_tree.11"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %3, align 8
  store %"struct.std::_Rb_tree_node.15"* %1, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %5 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %3, align 8
  %6 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE15_M_destroy_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %5, %"struct.std::_Rb_tree_node.15"* noundef %6) #3
  %7 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %5, %"struct.std::_Rb_tree_node.15"* noundef %7) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE15_M_destroy_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node.15"* noundef %1) #4 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::_Rb_tree.11"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %3, align 8
  store %"struct.std::_Rb_tree_node.15"* %1, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %5 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %3, align 8
  %6 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator.12"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %8 = invoke noundef %"struct.std::pair.17"* @_ZNSt13_Rb_tree_nodeISt4pairIKccEE9_M_valptrEv(%"struct.std::_Rb_tree_node.15"* noundef nonnull align 8 dereferenceable(34) %7)
          to label %9 unwind label %11

9:                                                ; preds = %2
  call void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKccEEEE7destroyIS3_EEvRS5_PT_(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::pair.17"* noundef %8) #3
  %10 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  ret void

11:                                               ; preds = %2
  %12 = landingpad { i8*, i32 }
          catch i8* null
  %13 = extractvalue { i8*, i32 } %12, 0
  call void @__clang_call_terminate(i8* %13) #16
  unreachable
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node.15"* noundef %1) #4 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::_Rb_tree.11"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %3, align 8
  store %"struct.std::_Rb_tree_node.15"* %1, %"struct.std::_Rb_tree_node.15"** %4, align 8
  %5 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %3, align 8
  %6 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator.12"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %4, align 8
  invoke void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKccEEEE10deallocateERS5_PS4_m(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::_Rb_tree_node.15"* noundef %7, i64 noundef 1)
          to label %8 unwind label %9

8:                                                ; preds = %2
  ret void

9:                                                ; preds = %2
  %10 = landingpad { i8*, i32 }
          catch i8* null
  %11 = extractvalue { i8*, i32 } %10, 0
  call void @__clang_call_terminate(i8* %11) #16
  unreachable
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKccEEEE7destroyIS3_EEvRS5_PT_(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair.17"* noundef %1) #4 comdat align 2 {
  %3 = alloca %"class.std::allocator.12"*, align 8
  %4 = alloca %"struct.std::pair.17"*, align 8
  store %"class.std::allocator.12"* %0, %"class.std::allocator.12"** %3, align 8
  store %"struct.std::pair.17"* %1, %"struct.std::pair.17"** %4, align 8
  %5 = load %"class.std::allocator.12"*, %"class.std::allocator.12"** %3, align 8
  %6 = bitcast %"class.std::allocator.12"* %5 to %"class.std::__new_allocator.13"*
  %7 = load %"struct.std::pair.17"*, %"struct.std::pair.17"** %4, align 8
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEE7destroyIS3_EEvPT_(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %6, %"struct.std::pair.17"* noundef %7) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) %"class.std::allocator.12"* @_ZNSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree.11"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %2, align 8
  %3 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree.11", %"class.std::_Rb_tree.11"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %4 to %"class.std::allocator.12"*
  ret %"class.std::allocator.12"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair.17"* @_ZNSt13_Rb_tree_nodeISt4pairIKccEE9_M_valptrEv(%"struct.std::_Rb_tree_node.15"* noundef nonnull align 8 dereferenceable(34) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  store %"struct.std::_Rb_tree_node.15"* %0, %"struct.std::_Rb_tree_node.15"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node.15", %"struct.std::_Rb_tree_node.15"* %3, i32 0, i32 1
  %5 = call noundef %"struct.std::pair.17"* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKccEE6_M_ptrEv(%"struct.__gnu_cxx::__aligned_membuf.16"* noundef nonnull align 1 dereferenceable(2) %4) #3
  ret %"struct.std::pair.17"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEE7destroyIS3_EEvPT_(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair.17"* noundef %1) #4 comdat align 2 {
  %3 = alloca %"class.std::__new_allocator.13"*, align 8
  %4 = alloca %"struct.std::pair.17"*, align 8
  store %"class.std::__new_allocator.13"* %0, %"class.std::__new_allocator.13"** %3, align 8
  store %"struct.std::pair.17"* %1, %"struct.std::pair.17"** %4, align 8
  %5 = load %"class.std::__new_allocator.13"*, %"class.std::__new_allocator.13"** %3, align 8
  %6 = load %"struct.std::pair.17"*, %"struct.std::pair.17"** %4, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair.17"* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKccEE6_M_ptrEv(%"struct.__gnu_cxx::__aligned_membuf.16"* noundef nonnull align 1 dereferenceable(2) %0) #4 comdat align 2 {
  %2 = alloca %"struct.__gnu_cxx::__aligned_membuf.16"*, align 8
  store %"struct.__gnu_cxx::__aligned_membuf.16"* %0, %"struct.__gnu_cxx::__aligned_membuf.16"** %2, align 8
  %3 = load %"struct.__gnu_cxx::__aligned_membuf.16"*, %"struct.__gnu_cxx::__aligned_membuf.16"** %2, align 8
  %4 = call noundef i8* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKccEE7_M_addrEv(%"struct.__gnu_cxx::__aligned_membuf.16"* noundef nonnull align 1 dereferenceable(2) %3) #3
  %5 = bitcast i8* %4 to %"struct.std::pair.17"*
  ret %"struct.std::pair.17"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef i8* @_ZN9__gnu_cxx16__aligned_membufISt4pairIKccEE7_M_addrEv(%"struct.__gnu_cxx::__aligned_membuf.16"* noundef nonnull align 1 dereferenceable(2) %0) #4 comdat align 2 {
  %2 = alloca %"struct.__gnu_cxx::__aligned_membuf.16"*, align 8
  store %"struct.__gnu_cxx::__aligned_membuf.16"* %0, %"struct.__gnu_cxx::__aligned_membuf.16"** %2, align 8
  %3 = load %"struct.__gnu_cxx::__aligned_membuf.16"*, %"struct.__gnu_cxx::__aligned_membuf.16"** %2, align 8
  %4 = getelementptr inbounds %"struct.__gnu_cxx::__aligned_membuf.16", %"struct.__gnu_cxx::__aligned_membuf.16"* %3, i32 0, i32 0
  %5 = bitcast [2 x i8]* %4 to i8*
  ret i8* %5
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKccEEEE10deallocateERS5_PS4_m(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::_Rb_tree_node.15"* noundef %1, i64 noundef %2) #8 comdat align 2 {
  %4 = alloca %"class.std::allocator.12"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  %6 = alloca i64, align 8
  store %"class.std::allocator.12"* %0, %"class.std::allocator.12"** %4, align 8
  store %"struct.std::_Rb_tree_node.15"* %1, %"struct.std::_Rb_tree_node.15"** %5, align 8
  store i64 %2, i64* %6, align 8
  %7 = load %"class.std::allocator.12"*, %"class.std::allocator.12"** %4, align 8
  %8 = bitcast %"class.std::allocator.12"* %7 to %"class.std::__new_allocator.13"*
  %9 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %5, align 8
  %10 = load i64, i64* %6, align 8
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEE10deallocateEPS4_m(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %8, %"struct.std::_Rb_tree_node.15"* noundef %9, i64 noundef %10)
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEE10deallocateEPS4_m(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::_Rb_tree_node.15"* noundef %1, i64 noundef %2) #4 comdat align 2 {
  %4 = alloca %"class.std::__new_allocator.13"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node.15"*, align 8
  %6 = alloca i64, align 8
  store %"class.std::__new_allocator.13"* %0, %"class.std::__new_allocator.13"** %4, align 8
  store %"struct.std::_Rb_tree_node.15"* %1, %"struct.std::_Rb_tree_node.15"** %5, align 8
  store i64 %2, i64* %6, align 8
  %7 = load %"class.std::__new_allocator.13"*, %"class.std::__new_allocator.13"** %4, align 8
  %8 = load %"struct.std::_Rb_tree_node.15"*, %"struct.std::_Rb_tree_node.15"** %5, align 8
  %9 = bitcast %"struct.std::_Rb_tree_node.15"* %8 to i8*
  call void @_ZdlPv(i8* noundef %9) #17
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node.15"* @_ZNKSt8_Rb_treeIcSt4pairIKccESt10_Select1stIS2_ESt4lessIcESaIS2_EE9_M_mbeginEv(%"class.std::_Rb_tree.11"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree.11"*, align 8
  store %"class.std::_Rb_tree.11"* %0, %"class.std::_Rb_tree.11"** %2, align 8
  %3 = load %"class.std::_Rb_tree.11"*, %"class.std::_Rb_tree.11"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree.11", %"class.std::_Rb_tree.11"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, char>, std::_Select1st<std::pair<const char, char>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 0
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %8, i32 0, i32 1
  %10 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %11 = bitcast %"struct.std::_Rb_tree_node_base"* %10 to %"struct.std::_Rb_tree_node.15"*
  ret %"struct.std::_Rb_tree_node.15"* %11
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt13_Rb_tree_nodeISt4pairIKccEEED2Ev(%"class.std::allocator.12"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::allocator.12"*, align 8
  store %"class.std::allocator.12"* %0, %"class.std::allocator.12"** %2, align 8
  %3 = load %"class.std::allocator.12"*, %"class.std::allocator.12"** %2, align 8
  %4 = bitcast %"class.std::allocator.12"* %3 to %"class.std::__new_allocator.13"*
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEED2Ev(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKccEEED2Ev(%"class.std::__new_allocator.13"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator.13"*, align 8
  store %"class.std::__new_allocator.13"* %0, %"class.std::__new_allocator.13"** %2, align 8
  %3 = load %"class.std::__new_allocator.13"*, %"class.std::__new_allocator.13"** %2, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt4pairIKcdEEC2Ev(%"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator.1"*, align 8
  store %"class.std::__new_allocator.1"* %0, %"class.std::__new_allocator.1"** %2, align 8
  %3 = load %"class.std::__new_allocator.1"*, %"class.std::__new_allocator.1"** %2, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt4pairIKcdEED2Ev(%"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"class.std::__new_allocator.1"*, align 8
  store %"class.std::__new_allocator.1"* %0, %"class.std::__new_allocator.1"** %2, align 8
  %3 = load %"class.std::__new_allocator.1"*, %"class.std::__new_allocator.1"** %2, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt4pairIKcdEEC2ERKS2_(%"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %0, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::allocator.0"*, align 8
  %4 = alloca %"class.std::allocator.0"*, align 8
  store %"class.std::allocator.0"* %0, %"class.std::allocator.0"** %3, align 8
  store %"class.std::allocator.0"* %1, %"class.std::allocator.0"** %4, align 8
  %5 = load %"class.std::allocator.0"*, %"class.std::allocator.0"** %3, align 8
  %6 = bitcast %"class.std::allocator.0"* %5 to %"class.std::__new_allocator.1"*
  %7 = load %"class.std::allocator.0"*, %"class.std::allocator.0"** %4, align 8
  %8 = bitcast %"class.std::allocator.0"* %7 to %"class.std::__new_allocator.1"*
  call void @_ZNSt15__new_allocatorISt4pairIKcdEEC2ERKS3_(%"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %6, %"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %8) #3
  ret void
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EEC2ERKS6_RKS7_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %1, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %2) unnamed_addr #7 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca %"struct.std::less"*, align 8
  %6 = alloca %"class.std::allocator.0"*, align 8
  %7 = alloca %"class.std::allocator", align 1
  %8 = alloca i8*, align 8
  %9 = alloca i32, align 4
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store %"struct.std::less"* %1, %"struct.std::less"** %5, align 8
  store %"class.std::allocator.0"* %2, %"class.std::allocator.0"** %6, align 8
  %10 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %11 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %10, i32 0, i32 0
  %12 = load %"struct.std::less"*, %"struct.std::less"** %5, align 8
  %13 = load %"class.std::allocator.0"*, %"class.std::allocator.0"** %6, align 8
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2IS2_EERKSaIT_E(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %7, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %13) #3
  invoke void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2ERKS6_OSaISt13_Rb_tree_nodeIS2_EE(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %11, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %12, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %7)
          to label %14 unwind label %15

14:                                               ; preds = %3
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %7) #3
  ret void

15:                                               ; preds = %3
  %16 = landingpad { i8*, i32 }
          cleanup
  %17 = extractvalue { i8*, i32 } %16, 0
  store i8* %17, i8** %8, align 8
  %18 = extractvalue { i8*, i32 } %16, 1
  store i32 %18, i32* %9, align 4
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEED2Ev(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %7) #3
  br label %19

19:                                               ; preds = %15
  %20 = load i8*, i8** %8, align 8
  %21 = load i32, i32* %9, align 4
  %22 = insertvalue { i8*, i32 } undef, i8* %20, 0
  %23 = insertvalue { i8*, i32 } %22, i32 %21, 1
  resume { i8*, i32 } %23
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_insert_range_uniqueIPKS2_EENSt9enable_ifIXsr17__same_value_typeIT_EE5valueEvE4typeESD_SD_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::pair"* noundef %1, %"struct.std::pair"* noundef %2) #8 comdat align 2 {
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca %"struct.std::pair"*, align 8
  %6 = alloca %"struct.std::pair"*, align 8
  %7 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node", align 8
  %8 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %9 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %10 = alloca %"struct.std::_Rb_tree_iterator", align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %5, align 8
  store %"struct.std::pair"* %2, %"struct.std::pair"** %6, align 8
  %11 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeC2ERS8_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %7, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %11)
  br label %12

12:                                               ; preds = %24, %3
  %13 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  %14 = load %"struct.std::pair"*, %"struct.std::pair"** %6, align 8
  %15 = icmp ne %"struct.std::pair"* %13, %14
  br i1 %15, label %16, label %27

16:                                               ; preds = %12
  %17 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %11) #3
  %18 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %9, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %17, %"struct.std::_Rb_tree_node_base"** %18, align 8
  call void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2ERKSt17_Rb_tree_iteratorIS2_E(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %8, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %9) #3
  %19 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  %20 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %8, i32 0, i32 0
  %21 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %20, align 8
  %22 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_insert_unique_IRKS2_NS8_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EOT_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %11, %"struct.std::_Rb_tree_node_base"* %21, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %19, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %7)
  %23 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %10, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %22, %"struct.std::_Rb_tree_node_base"** %23, align 8
  br label %24

24:                                               ; preds = %16
  %25 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  %26 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %25, i32 1
  store %"struct.std::pair"* %26, %"struct.std::pair"** %5, align 8
  br label %12, !llvm.loop !20

27:                                               ; preds = %12
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair"* @_ZNKSt16initializer_listISt4pairIKcdEE5beginEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::initializer_list"*, align 8
  store %"class.std::initializer_list"* %0, %"class.std::initializer_list"** %2, align 8
  %3 = load %"class.std::initializer_list"*, %"class.std::initializer_list"** %2, align 8
  %4 = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %3, i32 0, i32 0
  %5 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  ret %"struct.std::pair"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair"* @_ZNKSt16initializer_listISt4pairIKcdEE3endEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::initializer_list"*, align 8
  store %"class.std::initializer_list"* %0, %"class.std::initializer_list"** %2, align 8
  %3 = load %"class.std::initializer_list"*, %"class.std::initializer_list"** %2, align 8
  %4 = call noundef %"struct.std::pair"* @_ZNKSt16initializer_listISt4pairIKcdEE5beginEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %3) #3
  %5 = call noundef i64 @_ZNKSt16initializer_listISt4pairIKcdEE4sizeEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %3) #3
  %6 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %4, i64 %5
  ret %"struct.std::pair"* %6
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt4pairIKcdEEC2ERKS3_(%"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %0, %"class.std::__new_allocator.1"* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::__new_allocator.1"*, align 8
  %4 = alloca %"class.std::__new_allocator.1"*, align 8
  store %"class.std::__new_allocator.1"* %0, %"class.std::__new_allocator.1"** %3, align 8
  store %"class.std::__new_allocator.1"* %1, %"class.std::__new_allocator.1"** %4, align 8
  %5 = load %"class.std::__new_allocator.1"*, %"class.std::__new_allocator.1"** %3, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2IS2_EERKSaIT_E(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"class.std::allocator.0"* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::allocator"*, align 8
  %4 = alloca %"class.std::allocator.0"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %3, align 8
  store %"class.std::allocator.0"* %1, %"class.std::allocator.0"** %4, align 8
  %5 = load %"class.std::allocator"*, %"class.std::allocator"** %3, align 8
  %6 = bitcast %"class.std::allocator"* %5 to %"class.std::__new_allocator"*
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEEC2Ev(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %6) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE13_Rb_tree_implIS6_Lb1EEC2ERKS6_OSaISt13_Rb_tree_nodeIS2_EE(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %1, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %2) unnamed_addr #6 comdat align 2 {
  %4 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, align 8
  %5 = alloca %"struct.std::less"*, align 8
  %6 = alloca %"class.std::allocator"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %4, align 8
  store %"struct.std::less"* %1, %"struct.std::less"** %5, align 8
  store %"class.std::allocator"* %2, %"class.std::allocator"** %6, align 8
  %7 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"** %4, align 8
  %8 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %7 to %"class.std::allocator"*
  %9 = load %"class.std::allocator"*, %"class.std::allocator"** %6, align 8
  %10 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZSt4moveIRSaISt13_Rb_tree_nodeISt4pairIKcdEEEEONSt16remove_referenceIT_E4typeEOS8_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %9) #3
  call void @_ZNSaISt13_Rb_tree_nodeISt4pairIKcdEEEC2ERKS4_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %8, %"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %10) #3
  %11 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %7 to %"struct.std::_Rb_tree_key_compare"*
  %12 = load %"struct.std::less"*, %"struct.std::less"** %5, align 8
  call void @_ZNSt20_Rb_tree_key_compareISt4lessIcEEC2ERKS1_(%"struct.std::_Rb_tree_key_compare"* noundef nonnull align 1 dereferenceable(1) %11, %"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %12)
  %13 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %7 to i8*
  %14 = getelementptr inbounds i8, i8* %13, i64 8
  %15 = bitcast i8* %14 to %"struct.std::_Rb_tree_header"*
  call void @_ZNSt15_Rb_tree_headerC2Ev(%"struct.std::_Rb_tree_header"* noundef nonnull align 8 dereferenceable(40) %15) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZSt4moveIRSaISt13_Rb_tree_nodeISt4pairIKcdEEEEONSt16remove_referenceIT_E4typeEOS8_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca %"class.std::allocator"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %2, align 8
  %3 = load %"class.std::allocator"*, %"class.std::allocator"** %2, align 8
  ret %"class.std::allocator"* %3
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_insert_unique_IRKS2_NS8_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EOT_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %2, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %3) #8 comdat align 2 {
  %5 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %6 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %7 = alloca %"class.std::_Rb_tree"*, align 8
  %8 = alloca %"struct.std::pair"*, align 8
  %9 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %10 = alloca %"struct.std::pair.20", align 8
  %11 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %12 = alloca %"struct.std::_Select1st", align 1
  %13 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %6, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %13, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %7, align 8
  store %"struct.std::pair"* %2, %"struct.std::pair"** %8, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %3, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %9, align 8
  %14 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %7, align 8
  %15 = bitcast %"struct.std::_Rb_tree_const_iterator"* %11 to i8*
  %16 = bitcast %"struct.std::_Rb_tree_const_iterator"* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %15, i8* align 8 %16, i64 8, i1 false)
  %17 = load %"struct.std::pair"*, %"struct.std::pair"** %8, align 8
  %18 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt10_Select1stISt4pairIKcdEEclERKS2_(%"struct.std::_Select1st"* noundef nonnull align 1 dereferenceable(1) %12, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %17)
  %19 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %11, i32 0, i32 0
  %20 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %19, align 8
  %21 = call { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %14, %"struct.std::_Rb_tree_node_base"* %20, i8* noundef nonnull align 1 dereferenceable(1) %18)
  %22 = bitcast %"struct.std::pair.20"* %10 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %23 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %22, i32 0, i32 0
  %24 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %21, 0
  store %"struct.std::_Rb_tree_node_base"* %24, %"struct.std::_Rb_tree_node_base"** %23, align 8
  %25 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %22, i32 0, i32 1
  %26 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %21, 1
  store %"struct.std::_Rb_tree_node_base"* %26, %"struct.std::_Rb_tree_node_base"** %25, align 8
  %27 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %10, i32 0, i32 1
  %28 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %27, align 8
  %29 = icmp ne %"struct.std::_Rb_tree_node_base"* %28, null
  br i1 %29, label %30, label %40

30:                                               ; preds = %4
  %31 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %10, i32 0, i32 0
  %32 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %31, align 8
  %33 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %10, i32 0, i32 1
  %34 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %33, align 8
  %35 = load %"struct.std::pair"*, %"struct.std::pair"** %8, align 8
  %36 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %35) #3
  %37 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %9, align 8
  %38 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_M_insert_IRKS2_NS8_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS2_EPSt18_Rb_tree_node_baseSG_OT_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %14, %"struct.std::_Rb_tree_node_base"* noundef %32, %"struct.std::_Rb_tree_node_base"* noundef %34, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %36, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %37)
  %39 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %38, %"struct.std::_Rb_tree_node_base"** %39, align 8
  br label %43

40:                                               ; preds = %4
  %41 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %10, i32 0, i32 0
  %42 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %41, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5, %"struct.std::_Rb_tree_node_base"* noundef %42) #3
  br label %43

43:                                               ; preds = %40, %30
  %44 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  %45 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %44, align 8
  ret %"struct.std::_Rb_tree_node_base"* %45
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  %4 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %5 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %4, i32 0, i32 0
  %6 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %5 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 8
  %8 = bitcast i8* %7 to %"struct.std::_Rb_tree_header"*
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %8, i32 0, i32 0
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %2, %"struct.std::_Rb_tree_node_base"* noundef %9) #3
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %2, i32 0, i32 0
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  ret %"struct.std::_Rb_tree_node_base"* %11
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2ERKSt17_Rb_tree_iteratorIS2_E(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  store %"struct.std::_Rb_tree_iterator"* %1, %"struct.std::_Rb_tree_iterator"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  %7 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %4, align 8
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %7, i32 0, i32 0
  %9 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  store %"struct.std::_Rb_tree_node_base"* %9, %"struct.std::_Rb_tree_node_base"** %6, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node_base"* %1, i8* noundef nonnull align 1 dereferenceable(1) %2) #8 comdat align 2 {
  %4 = alloca %"struct.std::pair.20", align 8
  %5 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %6 = alloca %"class.std::_Rb_tree"*, align 8
  %7 = alloca i8*, align 8
  %8 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %9 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %10 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %11 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %12 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %13 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %14 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %15 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %16 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %16, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %6, align 8
  store i8* %2, i8** %7, align 8
  %17 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %6, align 8
  %18 = call %"struct.std::_Rb_tree_node_base"* @_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEE13_M_const_castEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %5) #3
  %19 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %18, %"struct.std::_Rb_tree_node_base"** %19, align 8
  %20 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %21 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %20, align 8
  %22 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  %23 = icmp eq %"struct.std::_Rb_tree_node_base"* %21, %22
  br i1 %23, label %24, label %46

24:                                               ; preds = %3
  %25 = call noundef i64 @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE4sizeEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  %26 = icmp ugt i64 %25, 0
  br i1 %26, label %27, label %38

27:                                               ; preds = %24
  %28 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %17, i32 0, i32 0
  %29 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %28 to %"struct.std::_Rb_tree_key_compare"*
  %30 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %29, i32 0, i32 0
  %31 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  %32 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %31, align 8
  %33 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %32)
  %34 = load i8*, i8** %7, align 8
  %35 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %30, i8* noundef nonnull align 1 dereferenceable(1) %33, i8* noundef nonnull align 1 dereferenceable(1) %34)
  br i1 %35, label %36, label %38

36:                                               ; preds = %27
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %37 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %9, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %37)
  br label %143

38:                                               ; preds = %27, %24
  %39 = load i8*, i8** %7, align 8
  %40 = call { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE24_M_get_insert_unique_posERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17, i8* noundef nonnull align 1 dereferenceable(1) %39)
  %41 = bitcast %"struct.std::pair.20"* %4 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %42 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %41, i32 0, i32 0
  %43 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %40, 0
  store %"struct.std::_Rb_tree_node_base"* %43, %"struct.std::_Rb_tree_node_base"** %42, align 8
  %44 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %41, i32 0, i32 1
  %45 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %40, 1
  store %"struct.std::_Rb_tree_node_base"* %45, %"struct.std::_Rb_tree_node_base"** %44, align 8
  br label %143

46:                                               ; preds = %3
  %47 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %17, i32 0, i32 0
  %48 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %47 to %"struct.std::_Rb_tree_key_compare"*
  %49 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %48, i32 0, i32 0
  %50 = load i8*, i8** %7, align 8
  %51 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %52 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %51, align 8
  %53 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %52)
  %54 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %49, i8* noundef nonnull align 1 dereferenceable(1) %50, i8* noundef nonnull align 1 dereferenceable(1) %53)
  br i1 %54, label %55, label %94

55:                                               ; preds = %46
  %56 = bitcast %"struct.std::_Rb_tree_iterator"* %10 to i8*
  %57 = bitcast %"struct.std::_Rb_tree_iterator"* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %56, i8* align 8 %57, i64 8, i1 false)
  %58 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %59 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %58, align 8
  %60 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_leftmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  %61 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %60, align 8
  %62 = icmp eq %"struct.std::_Rb_tree_node_base"* %59, %61
  br i1 %62, label %63, label %66

63:                                               ; preds = %55
  %64 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_leftmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  %65 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_leftmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRS1_S4_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %64, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %65)
  br label %143

66:                                               ; preds = %55
  %67 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %17, i32 0, i32 0
  %68 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %67 to %"struct.std::_Rb_tree_key_compare"*
  %69 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %68, i32 0, i32 0
  %70 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_iterator"* @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEmmEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %10) #3
  %71 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %70, i32 0, i32 0
  %72 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %71, align 8
  %73 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %72)
  %74 = load i8*, i8** %7, align 8
  %75 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %69, i8* noundef nonnull align 1 dereferenceable(1) %73, i8* noundef nonnull align 1 dereferenceable(1) %74)
  br i1 %75, label %76, label %86

76:                                               ; preds = %66
  %77 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %10, i32 0, i32 0
  %78 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %77, align 8
  %79 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %78) #3
  %80 = icmp eq %"struct.std::_Rb_tree_node"* %79, null
  br i1 %80, label %81, label %83

81:                                               ; preds = %76
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %11, align 8
  %82 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %10, i32 0, i32 0
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %11, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %82)
  br label %143

83:                                               ; preds = %76
  %84 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %85 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRS1_S4_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %84, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %85)
  br label %143

86:                                               ; preds = %66
  %87 = load i8*, i8** %7, align 8
  %88 = call { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE24_M_get_insert_unique_posERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17, i8* noundef nonnull align 1 dereferenceable(1) %87)
  %89 = bitcast %"struct.std::pair.20"* %4 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %90 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %89, i32 0, i32 0
  %91 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %88, 0
  store %"struct.std::_Rb_tree_node_base"* %91, %"struct.std::_Rb_tree_node_base"** %90, align 8
  %92 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %89, i32 0, i32 1
  %93 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %88, 1
  store %"struct.std::_Rb_tree_node_base"* %93, %"struct.std::_Rb_tree_node_base"** %92, align 8
  br label %143

94:                                               ; preds = %46
  %95 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %17, i32 0, i32 0
  %96 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %95 to %"struct.std::_Rb_tree_key_compare"*
  %97 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %96, i32 0, i32 0
  %98 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %99 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %98, align 8
  %100 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %99)
  %101 = load i8*, i8** %7, align 8
  %102 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %97, i8* noundef nonnull align 1 dereferenceable(1) %100, i8* noundef nonnull align 1 dereferenceable(1) %101)
  br i1 %102, label %103, label %141

103:                                              ; preds = %94
  %104 = bitcast %"struct.std::_Rb_tree_iterator"* %12 to i8*
  %105 = bitcast %"struct.std::_Rb_tree_iterator"* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %104, i8* align 8 %105, i64 8, i1 false)
  %106 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %107 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %106, align 8
  %108 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  %109 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %108, align 8
  %110 = icmp eq %"struct.std::_Rb_tree_node_base"* %107, %109
  br i1 %110, label %111, label %113

111:                                              ; preds = %103
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %13, align 8
  %112 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_rightmostEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17) #3
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %13, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %112)
  br label %143

113:                                              ; preds = %103
  %114 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %17, i32 0, i32 0
  %115 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %114 to %"struct.std::_Rb_tree_key_compare"*
  %116 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %115, i32 0, i32 0
  %117 = load i8*, i8** %7, align 8
  %118 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_iterator"* @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEppEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %12) #3
  %119 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %118, i32 0, i32 0
  %120 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %119, align 8
  %121 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %120)
  %122 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %116, i8* noundef nonnull align 1 dereferenceable(1) %117, i8* noundef nonnull align 1 dereferenceable(1) %121)
  br i1 %122, label %123, label %133

123:                                              ; preds = %113
  %124 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %125 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %124, align 8
  %126 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %125) #3
  %127 = icmp eq %"struct.std::_Rb_tree_node"* %126, null
  br i1 %127, label %128, label %130

128:                                              ; preds = %123
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %14, align 8
  %129 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %14, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %129)
  br label %143

130:                                              ; preds = %123
  %131 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %12, i32 0, i32 0
  %132 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %12, i32 0, i32 0
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRS1_S4_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %131, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %132)
  br label %143

133:                                              ; preds = %113
  %134 = load i8*, i8** %7, align 8
  %135 = call { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE24_M_get_insert_unique_posERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %17, i8* noundef nonnull align 1 dereferenceable(1) %134)
  %136 = bitcast %"struct.std::pair.20"* %4 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %137 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %136, i32 0, i32 0
  %138 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %135, 0
  store %"struct.std::_Rb_tree_node_base"* %138, %"struct.std::_Rb_tree_node_base"** %137, align 8
  %139 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %136, i32 0, i32 1
  %140 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %135, 1
  store %"struct.std::_Rb_tree_node_base"* %140, %"struct.std::_Rb_tree_node_base"** %139, align 8
  br label %143

141:                                              ; preds = %94
  %142 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %15, align 8
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %4, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %142, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %15)
  br label %143

143:                                              ; preds = %141, %133, %130, %128, %111, %86, %83, %81, %63, %38, %36
  %144 = bitcast %"struct.std::pair.20"* %4 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %145 = load { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %144, align 8
  ret { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %145
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt10_Select1stISt4pairIKcdEEclERKS2_(%"struct.std::_Select1st"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %1) #4 comdat align 2 {
  %3 = alloca %"struct.std::_Select1st"*, align 8
  %4 = alloca %"struct.std::pair"*, align 8
  store %"struct.std::_Select1st"* %0, %"struct.std::_Select1st"** %3, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %4, align 8
  %5 = load %"struct.std::_Select1st"*, %"struct.std::_Select1st"** %3, align 8
  %6 = load %"struct.std::pair"*, %"struct.std::pair"** %4, align 8
  %7 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %6, i32 0, i32 0
  ret i8* %7
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_M_insert_IRKS2_NS8_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS2_EPSt18_Rb_tree_node_baseSG_OT_RT0_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node_base"* noundef %1, %"struct.std::_Rb_tree_node_base"* noundef %2, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %3, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %4) #8 comdat align 2 {
  %6 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %7 = alloca %"class.std::_Rb_tree"*, align 8
  %8 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %9 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %10 = alloca %"struct.std::pair"*, align 8
  %11 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, align 8
  %12 = alloca i8, align 1
  %13 = alloca %"struct.std::_Select1st", align 1
  %14 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %7, align 8
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %8, align 8
  store %"struct.std::_Rb_tree_node_base"* %2, %"struct.std::_Rb_tree_node_base"** %9, align 8
  store %"struct.std::pair"* %3, %"struct.std::pair"** %10, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* %4, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %11, align 8
  %15 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %7, align 8
  %16 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %17 = icmp ne %"struct.std::_Rb_tree_node_base"* %16, null
  br i1 %17, label %31, label %18

18:                                               ; preds = %5
  %19 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %20 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %15) #3
  %21 = icmp eq %"struct.std::_Rb_tree_node_base"* %19, %20
  br i1 %21, label %31, label %22

22:                                               ; preds = %18
  %23 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %15, i32 0, i32 0
  %24 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %23 to %"struct.std::_Rb_tree_key_compare"*
  %25 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %24, i32 0, i32 0
  %26 = load %"struct.std::pair"*, %"struct.std::pair"** %10, align 8
  %27 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt10_Select1stISt4pairIKcdEEclERKS2_(%"struct.std::_Select1st"* noundef nonnull align 1 dereferenceable(1) %13, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %26)
  %28 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %29 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %28)
  %30 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %25, i8* noundef nonnull align 1 dereferenceable(1) %27, i8* noundef nonnull align 1 dereferenceable(1) %29)
  br label %31

31:                                               ; preds = %22, %18, %5
  %32 = phi i1 [ true, %18 ], [ true, %5 ], [ %30, %22 ]
  %33 = zext i1 %32 to i8
  store i8 %33, i8* %12, align 1
  %34 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"** %11, align 8
  %35 = load %"struct.std::pair"*, %"struct.std::pair"** %10, align 8
  %36 = call noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZSt7forwardIRKSt4pairIKcdEEOT_RNSt16remove_referenceIS5_E4typeE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %35) #3
  %37 = call noundef %"struct.std::_Rb_tree_node"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_Alloc_nodeclIRKS2_EEPSt13_Rb_tree_nodeIS2_EOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Alloc_node"* noundef nonnull align 8 dereferenceable(8) %34, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %36)
  store %"struct.std::_Rb_tree_node"* %37, %"struct.std::_Rb_tree_node"** %14, align 8
  %38 = load i8, i8* %12, align 1
  %39 = trunc i8 %38 to i1
  %40 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %14, align 8
  %41 = bitcast %"struct.std::_Rb_tree_node"* %40 to %"struct.std::_Rb_tree_node_base"*
  %42 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %43 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %15, i32 0, i32 0
  %44 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %43 to i8*
  %45 = getelementptr inbounds i8, i8* %44, i64 8
  %46 = bitcast i8* %45 to %"struct.std::_Rb_tree_header"*
  %47 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %46, i32 0, i32 0
  call void @_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_(i1 noundef zeroext %39, %"struct.std::_Rb_tree_node_base"* noundef %41, %"struct.std::_Rb_tree_node_base"* noundef %42, %"struct.std::_Rb_tree_node_base"* noundef nonnull align 8 dereferenceable(32) %47) #3
  %48 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %15, i32 0, i32 0
  %49 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %48 to i8*
  %50 = getelementptr inbounds i8, i8* %49, i64 8
  %51 = bitcast i8* %50 to %"struct.std::_Rb_tree_header"*
  %52 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %51, i32 0, i32 1
  %53 = load i64, i64* %52, align 8
  %54 = add i64 %53, 1
  store i64 %54, i64* %52, align 8
  %55 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %14, align 8
  %56 = bitcast %"struct.std::_Rb_tree_node"* %55 to %"struct.std::_Rb_tree_node_base"*
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Rb_tree_node_base"* noundef %56) #3
  %57 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  %58 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %57, align 8
  ret %"struct.std::_Rb_tree_node_base"* %58
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Rb_tree_node_base"* noundef %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_iterator"* %0, %"struct.std::_Rb_tree_iterator"** %3, align 8
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  %7 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  store %"struct.std::_Rb_tree_node_base"* %7, %"struct.std::_Rb_tree_node_base"** %6, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNKSt23_Rb_tree_const_iteratorISt4pairIKcdEE13_M_const_castEv(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %3 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  %4 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  %5 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %4, i32 0, i32 0
  %6 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %5, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %2, %"struct.std::_Rb_tree_node_base"* noundef %6) #3
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %2, i32 0, i32 0
  %8 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %7, align 8
  ret %"struct.std::_Rb_tree_node_base"* %8
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef i64 @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE4sizeEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 8
  %7 = bitcast i8* %6 to %"struct.std::_Rb_tree_header"*
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %7, i32 0, i32 1
  %9 = load i64, i64* %8, align 8
  ret i64 %9
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %0, i8* noundef nonnull align 1 dereferenceable(1) %1, i8* noundef nonnull align 1 dereferenceable(1) %2) #4 comdat align 2 {
  %4 = alloca %"struct.std::less"*, align 8
  %5 = alloca i8*, align 8
  %6 = alloca i8*, align 8
  store %"struct.std::less"* %0, %"struct.std::less"** %4, align 8
  store i8* %1, i8** %5, align 8
  store i8* %2, i8** %6, align 8
  %7 = load %"struct.std::less"*, %"struct.std::less"** %4, align 8
  %8 = load i8*, i8** %5, align 8
  %9 = load i8, i8* %8, align 1
  %10 = sext i8 %9 to i32
  %11 = load i8*, i8** %6, align 8
  %12 = load i8, i8* %11, align 1
  %13 = sext i8 %12 to i32
  %14 = icmp slt i32 %10, %13
  ret i1 %14
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %0) #8 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_node_base"* %0, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %2, align 8
  %4 = bitcast %"struct.std::_Rb_tree_node_base"* %3 to %"struct.std::_Rb_tree_node"*
  %5 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E(%"struct.std::_Rb_tree_node"* noundef %4)
  ret i8* %5
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %0, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %1, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %2) unnamed_addr #6 comdat align 2 {
  %4 = alloca %"struct.std::pair.20"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node_base"**, align 8
  %6 = alloca %"struct.std::_Rb_tree_node_base"**, align 8
  store %"struct.std::pair.20"* %0, %"struct.std::pair.20"** %4, align 8
  store %"struct.std::_Rb_tree_node_base"** %1, %"struct.std::_Rb_tree_node_base"*** %5, align 8
  store %"struct.std::_Rb_tree_node_base"** %2, %"struct.std::_Rb_tree_node_base"*** %6, align 8
  %7 = load %"struct.std::pair.20"*, %"struct.std::pair.20"** %4, align 8
  %8 = bitcast %"struct.std::pair.20"* %7 to %"class.std::__pair_base.21"*
  %9 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %7, i32 0, i32 0
  %10 = load %"struct.std::_Rb_tree_node_base"**, %"struct.std::_Rb_tree_node_base"*** %5, align 8
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  store %"struct.std::_Rb_tree_node_base"* %11, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %12 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %7, i32 0, i32 1
  %13 = load %"struct.std::_Rb_tree_node_base"**, %"struct.std::_Rb_tree_node_base"*** %6, align 8
  %14 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %13, align 8
  store %"struct.std::_Rb_tree_node_base"* %14, %"struct.std::_Rb_tree_node_base"** %12, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE24_M_get_insert_unique_posERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) #8 comdat align 2 {
  %3 = alloca %"struct.std::pair.20", align 8
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca i8*, align 8
  %6 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %8 = alloca i8, align 1
  %9 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %10 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %11 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store i8* %1, i8** %5, align 8
  %12 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %13 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %12) #3
  store %"struct.std::_Rb_tree_node"* %13, %"struct.std::_Rb_tree_node"** %6, align 8
  %14 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %12) #3
  store %"struct.std::_Rb_tree_node_base"* %14, %"struct.std::_Rb_tree_node_base"** %7, align 8
  store i8 1, i8* %8, align 1
  br label %15

15:                                               ; preds = %39, %2
  %16 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %17 = icmp ne %"struct.std::_Rb_tree_node"* %16, null
  br i1 %17, label %18, label %41

18:                                               ; preds = %15
  %19 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %20 = bitcast %"struct.std::_Rb_tree_node"* %19 to %"struct.std::_Rb_tree_node_base"*
  store %"struct.std::_Rb_tree_node_base"* %20, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %21 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %12, i32 0, i32 0
  %22 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %21 to %"struct.std::_Rb_tree_key_compare"*
  %23 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %22, i32 0, i32 0
  %24 = load i8*, i8** %5, align 8
  %25 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %26 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E(%"struct.std::_Rb_tree_node"* noundef %25)
  %27 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %23, i8* noundef nonnull align 1 dereferenceable(1) %24, i8* noundef nonnull align 1 dereferenceable(1) %26)
  %28 = zext i1 %27 to i8
  store i8 %28, i8* %8, align 1
  %29 = load i8, i8* %8, align 1
  %30 = trunc i8 %29 to i1
  br i1 %30, label %31, label %35

31:                                               ; preds = %18
  %32 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %33 = bitcast %"struct.std::_Rb_tree_node"* %32 to %"struct.std::_Rb_tree_node_base"*
  %34 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %33) #3
  br label %39

35:                                               ; preds = %18
  %36 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %6, align 8
  %37 = bitcast %"struct.std::_Rb_tree_node"* %36 to %"struct.std::_Rb_tree_node_base"*
  %38 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %37) #3
  br label %39

39:                                               ; preds = %35, %31
  %40 = phi %"struct.std::_Rb_tree_node"* [ %34, %31 ], [ %38, %35 ]
  store %"struct.std::_Rb_tree_node"* %40, %"struct.std::_Rb_tree_node"** %6, align 8
  br label %15, !llvm.loop !21

41:                                               ; preds = %15
  %42 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %7, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %9, %"struct.std::_Rb_tree_node_base"* noundef %42) #3
  %43 = load i8, i8* %8, align 1
  %44 = trunc i8 %43 to i1
  br i1 %44, label %45, label %53

45:                                               ; preds = %41
  %46 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE5beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %12) #3
  %47 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %10, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %46, %"struct.std::_Rb_tree_node_base"** %47, align 8
  %48 = call noundef zeroext i1 @_ZSteqRKSt17_Rb_tree_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %9, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %10) #3
  br i1 %48, label %49, label %50

49:                                               ; preds = %45
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRPSt13_Rb_tree_nodeIS_IKcdEERS1_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %3, %"struct.std::_Rb_tree_node"** noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %7)
  br label %65

50:                                               ; preds = %45
  %51 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_iterator"* @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEmmEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %9) #3
  br label %52

52:                                               ; preds = %50
  br label %53

53:                                               ; preds = %52, %41
  %54 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %12, i32 0, i32 0
  %55 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %54 to %"struct.std::_Rb_tree_key_compare"*
  %56 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %55, i32 0, i32 0
  %57 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %9, i32 0, i32 0
  %58 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %57, align 8
  %59 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %58)
  %60 = load i8*, i8** %5, align 8
  %61 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %56, i8* noundef nonnull align 1 dereferenceable(1) %59, i8* noundef nonnull align 1 dereferenceable(1) %60)
  br i1 %61, label %62, label %63

62:                                               ; preds = %53
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRPSt13_Rb_tree_nodeIS_IKcdEERS1_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %3, %"struct.std::_Rb_tree_node"** noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %7)
  br label %65

63:                                               ; preds = %53
  %64 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %9, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* null, %"struct.std::_Rb_tree_node_base"** %11, align 8
  call void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IS1_S1_Lb1EEERKS1_S5_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %3, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %64, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %11)
  br label %65

65:                                               ; preds = %63, %62, %49
  %66 = bitcast %"struct.std::pair.20"* %3 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %67 = load { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %66, align 8
  ret { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %67
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRS1_S4_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %0, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %1, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %2) unnamed_addr #6 comdat align 2 {
  %4 = alloca %"struct.std::pair.20"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node_base"**, align 8
  %6 = alloca %"struct.std::_Rb_tree_node_base"**, align 8
  store %"struct.std::pair.20"* %0, %"struct.std::pair.20"** %4, align 8
  store %"struct.std::_Rb_tree_node_base"** %1, %"struct.std::_Rb_tree_node_base"*** %5, align 8
  store %"struct.std::_Rb_tree_node_base"** %2, %"struct.std::_Rb_tree_node_base"*** %6, align 8
  %7 = load %"struct.std::pair.20"*, %"struct.std::pair.20"** %4, align 8
  %8 = bitcast %"struct.std::pair.20"* %7 to %"class.std::__pair_base.21"*
  %9 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %7, i32 0, i32 0
  %10 = load %"struct.std::_Rb_tree_node_base"**, %"struct.std::_Rb_tree_node_base"*** %5, align 8
  %11 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZSt7forwardIRPSt18_Rb_tree_node_baseEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %10) #3
  %12 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %11, align 8
  store %"struct.std::_Rb_tree_node_base"* %12, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %13 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %7, i32 0, i32 1
  %14 = load %"struct.std::_Rb_tree_node_base"**, %"struct.std::_Rb_tree_node_base"*** %6, align 8
  %15 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZSt7forwardIRPSt18_Rb_tree_node_baseEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %14) #3
  %16 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %15, align 8
  store %"struct.std::_Rb_tree_node_base"* %16, %"struct.std::_Rb_tree_node_base"** %13, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_iterator"* @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEmmEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  store %"struct.std::_Rb_tree_iterator"* %0, %"struct.std::_Rb_tree_iterator"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %5) #18
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %7, align 8
  ret %"struct.std::_Rb_tree_iterator"* %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_iterator"* @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEppEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  store %"struct.std::_Rb_tree_iterator"* %0, %"struct.std::_Rb_tree_iterator"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %5) #18
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %7, align 8
  ret %"struct.std::_Rb_tree_iterator"* %3
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E(%"struct.std::_Rb_tree_node"* noundef %0) #8 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %3 = alloca %"struct.std::_Select1st", align 1
  store %"struct.std::_Rb_tree_node"* %0, %"struct.std::_Rb_tree_node"** %2, align 8
  %4 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %2, align 8
  %5 = call noundef %"struct.std::pair"* @_ZNKSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %4)
  %6 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt10_Select1stISt4pairIKcdEEclERKS2_(%"struct.std::_Select1st"* noundef nonnull align 1 dereferenceable(1) %3, %"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %5)
  ret i8* %6
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair"* @_ZNKSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"struct.std::_Rb_tree_node"* %0, %"struct.std::_Rb_tree_node"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_node", %"struct.std::_Rb_tree_node"* %3, i32 0, i32 1
  %5 = call noundef %"struct.std::pair"* @_ZNK9__gnu_cxx16__aligned_membufISt4pairIKcdEE6_M_ptrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %4) #3
  ret %"struct.std::pair"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::pair"* @_ZNK9__gnu_cxx16__aligned_membufISt4pairIKcdEE6_M_ptrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"struct.__gnu_cxx::__aligned_membuf"*, align 8
  store %"struct.__gnu_cxx::__aligned_membuf"* %0, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %3 = load %"struct.__gnu_cxx::__aligned_membuf"*, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %4 = call noundef i8* @_ZNK9__gnu_cxx16__aligned_membufISt4pairIKcdEE7_M_addrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %3) #3
  %5 = bitcast i8* %4 to %"struct.std::pair"*
  ret %"struct.std::pair"* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef i8* @_ZNK9__gnu_cxx16__aligned_membufISt4pairIKcdEE7_M_addrEv(%"struct.__gnu_cxx::__aligned_membuf"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"struct.__gnu_cxx::__aligned_membuf"*, align 8
  store %"struct.__gnu_cxx::__aligned_membuf"* %0, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %3 = load %"struct.__gnu_cxx::__aligned_membuf"*, %"struct.__gnu_cxx::__aligned_membuf"** %2, align 8
  %4 = getelementptr inbounds %"struct.__gnu_cxx::__aligned_membuf", %"struct.__gnu_cxx::__aligned_membuf"* %3, i32 0, i32 0
  %5 = bitcast [16 x i8]* %4 to i8*
  ret i8* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef zeroext i1 @_ZSteqRKSt17_Rb_tree_iteratorISt4pairIKcdEES5_(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %1) #4 comdat {
  %3 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  store %"struct.std::_Rb_tree_iterator"* %0, %"struct.std::_Rb_tree_iterator"** %3, align 8
  store %"struct.std::_Rb_tree_iterator"* %1, %"struct.std::_Rb_tree_iterator"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  %7 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %6, align 8
  %8 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %4, align 8
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %8, i32 0, i32 0
  %10 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %11 = icmp eq %"struct.std::_Rb_tree_node_base"* %7, %10
  ret i1 %11
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE5beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  %4 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %5 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %4, i32 0, i32 0
  %6 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %5 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 8
  %8 = bitcast i8* %7 to %"struct.std::_Rb_tree_header"*
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %8, i32 0, i32 0
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %9, i32 0, i32 2
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %2, %"struct.std::_Rb_tree_node_base"* noundef %11) #3
  %12 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %2, i32 0, i32 0
  %13 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %12, align 8
  ret %"struct.std::_Rb_tree_node_base"* %13
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIPSt18_Rb_tree_node_baseS1_EC2IRPSt13_Rb_tree_nodeIS_IKcdEERS1_Lb1EEEOT_OT0_(%"struct.std::pair.20"* noundef nonnull align 8 dereferenceable(16) %0, %"struct.std::_Rb_tree_node"** noundef nonnull align 8 dereferenceable(8) %1, %"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %2) unnamed_addr #6 comdat align 2 {
  %4 = alloca %"struct.std::pair.20"*, align 8
  %5 = alloca %"struct.std::_Rb_tree_node"**, align 8
  %6 = alloca %"struct.std::_Rb_tree_node_base"**, align 8
  store %"struct.std::pair.20"* %0, %"struct.std::pair.20"** %4, align 8
  store %"struct.std::_Rb_tree_node"** %1, %"struct.std::_Rb_tree_node"*** %5, align 8
  store %"struct.std::_Rb_tree_node_base"** %2, %"struct.std::_Rb_tree_node_base"*** %6, align 8
  %7 = load %"struct.std::pair.20"*, %"struct.std::pair.20"** %4, align 8
  %8 = bitcast %"struct.std::pair.20"* %7 to %"class.std::__pair_base.21"*
  %9 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %7, i32 0, i32 0
  %10 = load %"struct.std::_Rb_tree_node"**, %"struct.std::_Rb_tree_node"*** %5, align 8
  %11 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node"** @_ZSt7forwardIRPSt13_Rb_tree_nodeISt4pairIKcdEEEOT_RNSt16remove_referenceIS7_E4typeE(%"struct.std::_Rb_tree_node"** noundef nonnull align 8 dereferenceable(8) %10) #3
  %12 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %11, align 8
  %13 = bitcast %"struct.std::_Rb_tree_node"* %12 to %"struct.std::_Rb_tree_node_base"*
  store %"struct.std::_Rb_tree_node_base"* %13, %"struct.std::_Rb_tree_node_base"** %9, align 8
  %14 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %7, i32 0, i32 1
  %15 = load %"struct.std::_Rb_tree_node_base"**, %"struct.std::_Rb_tree_node_base"*** %6, align 8
  %16 = call noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZSt7forwardIRPSt18_Rb_tree_node_baseEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %15) #3
  %17 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %16, align 8
  store %"struct.std::_Rb_tree_node_base"* %17, %"struct.std::_Rb_tree_node_base"** %14, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node"** @_ZSt7forwardIRPSt13_Rb_tree_nodeISt4pairIKcdEEEOT_RNSt16remove_referenceIS7_E4typeE(%"struct.std::_Rb_tree_node"** noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"struct.std::_Rb_tree_node"**, align 8
  store %"struct.std::_Rb_tree_node"** %0, %"struct.std::_Rb_tree_node"*** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node"**, %"struct.std::_Rb_tree_node"*** %2, align 8
  ret %"struct.std::_Rb_tree_node"** %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"struct.std::_Rb_tree_node_base"** @_ZSt7forwardIRPSt18_Rb_tree_node_baseEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::_Rb_tree_node_base"** noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"struct.std::_Rb_tree_node_base"**, align 8
  store %"struct.std::_Rb_tree_node_base"** %0, %"struct.std::_Rb_tree_node_base"*** %2, align 8
  %3 = load %"struct.std::_Rb_tree_node_base"**, %"struct.std::_Rb_tree_node_base"*** %2, align 8
  ret %"struct.std::_Rb_tree_node_base"** %3
}

; Function Attrs: nounwind readonly willreturn
declare noundef %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef) #12

; Function Attrs: nounwind readonly willreturn
declare noundef %"struct.std::_Rb_tree_node_base"* @_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef) #12

; Function Attrs: nounwind
declare void @_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_(i1 noundef zeroext, %"struct.std::_Rb_tree_node_base"* noundef, %"struct.std::_Rb_tree_node_base"* noundef, %"struct.std::_Rb_tree_node_base"* noundef nonnull align 8 dereferenceable(32)) #2

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef i64 @_ZNKSt16initializer_listISt4pairIKcdEE4sizeEv(%"class.std::initializer_list"* noundef nonnull align 8 dereferenceable(16) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::initializer_list"*, align 8
  store %"class.std::initializer_list"* %0, %"class.std::initializer_list"** %2, align 8
  %3 = load %"class.std::initializer_list"*, %"class.std::initializer_list"** %2, align 8
  %4 = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %3, i32 0, i32 1
  %5 = load i64, i64* %4, align 8
  ret i64 %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE5beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  %4 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %5 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %4, i32 0, i32 0
  %6 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %5 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 8
  %8 = bitcast i8* %7 to %"struct.std::_Rb_tree_header"*
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %8, i32 0, i32 0
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_node_base", %"struct.std::_Rb_tree_node_base"* %9, i32 0, i32 2
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  call void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2EPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %2, %"struct.std::_Rb_tree_node_base"* noundef %11) #3
  %12 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %2, i32 0, i32 0
  %13 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %12, align 8
  ret %"struct.std::_Rb_tree_node_base"* %13
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2EPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Rb_tree_node_base"* noundef %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree_const_iterator"*, align 8
  %4 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  store %"struct.std::_Rb_tree_const_iterator"* %0, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %5 = load %"struct.std::_Rb_tree_const_iterator"*, %"struct.std::_Rb_tree_const_iterator"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %5, i32 0, i32 0
  %7 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  store %"struct.std::_Rb_tree_node_base"* %7, %"struct.std::_Rb_tree_node_base"** %6, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %3 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %3, align 8
  %4 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %3, align 8
  %5 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %4, i32 0, i32 0
  %6 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %5 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 8
  %8 = bitcast i8* %7 to %"struct.std::_Rb_tree_header"*
  %9 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %8, i32 0, i32 0
  call void @_ZNSt23_Rb_tree_const_iteratorISt4pairIKcdEEC2EPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_const_iterator"* noundef nonnull align 8 dereferenceable(8) %2, %"struct.std::_Rb_tree_node_base"* noundef %9) #3
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %2, i32 0, i32 0
  %11 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %10, align 8
  ret %"struct.std::_Rb_tree_node_base"* %11
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE11lower_boundERS3_(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) #8 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %4 = alloca %"class.std::map"*, align 8
  %5 = alloca i8*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %4, align 8
  store i8* %1, i8** %5, align 8
  %6 = load %"class.std::map"*, %"class.std::map"** %4, align 8
  %7 = getelementptr inbounds %"class.std::map", %"class.std::map"* %6, i32 0, i32 0
  %8 = load i8*, i8** %5, align 8
  %9 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11lower_boundERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %7, i8* noundef nonnull align 1 dereferenceable(1) %8)
  %10 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %9, %"struct.std::_Rb_tree_node_base"** %10, align 8
  %11 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  %12 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %11, align 8
  ret %"struct.std::_Rb_tree_node_base"* %12
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt3mapIcdSt4lessIcESaISt4pairIKcdEEE3endEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %3 = alloca %"class.std::map"*, align 8
  store %"class.std::map"* %0, %"class.std::map"** %3, align 8
  %4 = load %"class.std::map"*, %"class.std::map"** %3, align 8
  %5 = getelementptr inbounds %"class.std::map", %"class.std::map"* %4, i32 0, i32 0
  %6 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE3endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5) #3
  %7 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %2, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %6, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %8 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %2, i32 0, i32 0
  %9 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  ret %"struct.std::_Rb_tree_node_base"* %9
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNKSt3mapIcdSt4lessIcESaISt4pairIKcdEEE8key_compEv(%"class.std::map"* noundef nonnull align 8 dereferenceable(48) %0) #8 comdat align 2 {
  %2 = alloca %"class.std::map"*, align 8
  %3 = alloca %"struct.std::less", align 1
  store %"class.std::map"* %0, %"class.std::map"** %2, align 8
  %4 = load %"class.std::map"*, %"class.std::map"** %2, align 8
  %5 = getelementptr inbounds %"class.std::map", %"class.std::map"* %4, i32 0, i32 0
  call void @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8key_compEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %5)
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(16) %"struct.std::pair"* @_ZNKSt17_Rb_tree_iteratorISt4pairIKcdEEdeEv(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree_iterator"*, align 8
  store %"struct.std::_Rb_tree_iterator"* %0, %"struct.std::_Rb_tree_iterator"** %2, align 8
  %3 = load %"struct.std::_Rb_tree_iterator"*, %"struct.std::_Rb_tree_iterator"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  %5 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %4, align 8
  %6 = bitcast %"struct.std::_Rb_tree_node_base"* %5 to %"struct.std::_Rb_tree_node"*
  %7 = call noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %6)
  ret %"struct.std::pair"* %7
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %6 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %7 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %8 = alloca %"class.std::_Rb_tree"*, align 8
  %9 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %10 = alloca %"class.std::tuple"*, align 8
  %11 = alloca %"class.std::tuple.23"*, align 8
  %12 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", align 8
  %13 = alloca %"struct.std::pair.20", align 8
  %14 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %15 = alloca i8*, align 8
  %16 = alloca i32, align 4
  %17 = alloca %"struct.std::pair.20", align 8
  %18 = alloca i32, align 4
  %19 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %7, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %19, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %8, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %9, align 8
  store %"class.std::tuple"* %3, %"class.std::tuple"** %10, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %11, align 8
  %20 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %8, align 8
  %21 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %9, align 8
  %22 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %21) #3
  %23 = load %"class.std::tuple"*, %"class.std::tuple"** %10, align 8
  %24 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %23) #3
  %25 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %11, align 8
  %26 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %25) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeC2IJRKSt21piecewise_construct_tSt5tupleIJRS1_EESE_IJEEEEERS8_DpOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %20, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %22, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %24, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %26)
  %27 = bitcast %"struct.std::_Rb_tree_const_iterator"* %14 to i8*
  %28 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %27, i8* align 8 %28, i64 8, i1 false)
  %29 = invoke noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node6_M_keyEv(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12)
          to label %30 unwind label %54

30:                                               ; preds = %5
  %31 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %14, i32 0, i32 0
  %32 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %31, align 8
  %33 = invoke { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %20, %"struct.std::_Rb_tree_node_base"* %32, i8* noundef nonnull align 1 dereferenceable(1) %29)
          to label %34 unwind label %54

34:                                               ; preds = %30
  %35 = bitcast %"struct.std::pair.20"* %13 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %36 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %35, i32 0, i32 0
  %37 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %33, 0
  store %"struct.std::_Rb_tree_node_base"* %37, %"struct.std::_Rb_tree_node_base"** %36, align 8
  %38 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %35, i32 0, i32 1
  %39 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %33, 1
  store %"struct.std::_Rb_tree_node_base"* %39, %"struct.std::_Rb_tree_node_base"** %38, align 8
  %40 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %13, i32 0, i32 1
  %41 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %40, align 8
  %42 = icmp ne %"struct.std::_Rb_tree_node_base"* %41, null
  br i1 %42, label %43, label %58

43:                                               ; preds = %34
  %44 = bitcast %"struct.std::pair.20"* %17 to i8*
  %45 = bitcast %"struct.std::pair.20"* %13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %44, i8* align 8 %45, i64 16, i1 false)
  %46 = bitcast %"struct.std::pair.20"* %17 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %47 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %46, i32 0, i32 0
  %48 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %47, align 8
  %49 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %46, i32 0, i32 1
  %50 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %49, align 8
  %51 = invoke %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node9_M_insertES0_IPSt18_Rb_tree_node_baseSB_E(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12, %"struct.std::_Rb_tree_node_base"* %48, %"struct.std::_Rb_tree_node_base"* %50)
          to label %52 unwind label %54

52:                                               ; preds = %43
  %53 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %51, %"struct.std::_Rb_tree_node_base"** %53, align 8
  store i32 1, i32* %18, align 4
  br label %61

54:                                               ; preds = %43, %30, %5
  %55 = landingpad { i8*, i32 }
          cleanup
  %56 = extractvalue { i8*, i32 } %55, 0
  store i8* %56, i8** %15, align 8
  %57 = extractvalue { i8*, i32 } %55, 1
  store i32 %57, i32* %16, align 4
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeD2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12) #3
  br label %64

58:                                               ; preds = %34
  %59 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %13, i32 0, i32 0
  %60 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %59, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Rb_tree_node_base"* noundef %60) #3
  store i32 1, i32* %18, align 4
  br label %61

61:                                               ; preds = %58, %52
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeD2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12) #3
  %62 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  %63 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %62, align 8
  ret %"struct.std::_Rb_tree_node_base"* %63

64:                                               ; preds = %54
  %65 = load i8*, i8** %15, align 8
  %66 = load i32, i32* %16, align 4
  %67 = insertvalue { i8*, i32 } undef, i8* %65, 0
  %68 = insertvalue { i8*, i32 } %67, i32 %66, 1
  resume { i8*, i32 } %68
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt5tupleIJRKcEEC2ILb1ELb1EEES1_(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::tuple"*, align 8
  %4 = alloca i8*, align 8
  store %"class.std::tuple"* %0, %"class.std::tuple"** %3, align 8
  store i8* %1, i8** %4, align 8
  %5 = load %"class.std::tuple"*, %"class.std::tuple"** %3, align 8
  %6 = bitcast %"class.std::tuple"* %5 to %"struct.std::_Tuple_impl"*
  %7 = load i8*, i8** %4, align 8
  invoke void @_ZNSt11_Tuple_implILm0EJRKcEEC2ES1_(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %6, i8* noundef nonnull align 1 dereferenceable(1) %7)
          to label %8 unwind label %9

8:                                                ; preds = %2
  ret void

9:                                                ; preds = %2
  %10 = landingpad { i8*, i32 }
          catch i8* null
  %11 = extractvalue { i8*, i32 } %10, 0
  call void @__clang_call_terminate(i8* %11) #16
  unreachable
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11lower_boundERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) #8 comdat align 2 {
  %3 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %4 = alloca %"class.std::_Rb_tree"*, align 8
  %5 = alloca i8*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %4, align 8
  store i8* %1, i8** %5, align 8
  %6 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %4, align 8
  %7 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_M_beginEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6) #3
  %8 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6) #3
  %9 = load i8*, i8** %5, align 8
  %10 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_lower_boundEPSt13_Rb_tree_nodeIS2_EPSt18_Rb_tree_node_baseRS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %6, %"struct.std::_Rb_tree_node"* noundef %7, %"struct.std::_Rb_tree_node_base"* noundef %8, i8* noundef nonnull align 1 dereferenceable(1) %9)
  %11 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %10, %"struct.std::_Rb_tree_node_base"** %11, align 8
  %12 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %3, i32 0, i32 0
  %13 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %12, align 8
  ret %"struct.std::_Rb_tree_node_base"* %13
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_lower_boundEPSt13_Rb_tree_nodeIS2_EPSt18_Rb_tree_node_baseRS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1, %"struct.std::_Rb_tree_node_base"* noundef %2, i8* noundef nonnull align 1 dereferenceable(1) %3) #8 comdat align 2 {
  %5 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %6 = alloca %"class.std::_Rb_tree"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %8 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %9 = alloca i8*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %6, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %7, align 8
  store %"struct.std::_Rb_tree_node_base"* %2, %"struct.std::_Rb_tree_node_base"** %8, align 8
  store i8* %3, i8** %9, align 8
  %10 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %6, align 8
  br label %11

11:                                               ; preds = %32, %4
  %12 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %13 = icmp ne %"struct.std::_Rb_tree_node"* %12, null
  br i1 %13, label %14, label %33

14:                                               ; preds = %11
  %15 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %10, i32 0, i32 0
  %16 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %15 to %"struct.std::_Rb_tree_key_compare"*
  %17 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %16, i32 0, i32 0
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %19 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E(%"struct.std::_Rb_tree_node"* noundef %18)
  %20 = load i8*, i8** %9, align 8
  %21 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %17, i8* noundef nonnull align 1 dereferenceable(1) %19, i8* noundef nonnull align 1 dereferenceable(1) %20)
  br i1 %21, label %28, label %22

22:                                               ; preds = %14
  %23 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %24 = bitcast %"struct.std::_Rb_tree_node"* %23 to %"struct.std::_Rb_tree_node_base"*
  store %"struct.std::_Rb_tree_node_base"* %24, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %25 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %26 = bitcast %"struct.std::_Rb_tree_node"* %25 to %"struct.std::_Rb_tree_node_base"*
  %27 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE7_S_leftEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %26) #3
  store %"struct.std::_Rb_tree_node"* %27, %"struct.std::_Rb_tree_node"** %7, align 8
  br label %32

28:                                               ; preds = %14
  %29 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %30 = bitcast %"struct.std::_Rb_tree_node"* %29 to %"struct.std::_Rb_tree_node_base"*
  %31 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8_S_rightEPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %30) #3
  store %"struct.std::_Rb_tree_node"* %31, %"struct.std::_Rb_tree_node"** %7, align 8
  br label %32

32:                                               ; preds = %28, %22
  br label %11, !llvm.loop !22

33:                                               ; preds = %11
  %34 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5, %"struct.std::_Rb_tree_node_base"* noundef %34) #3
  %35 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  %36 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %35, align 8
  ret %"struct.std::_Rb_tree_node_base"* %36
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE8key_compEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0) #4 comdat align 2 {
  %2 = alloca %"class.std::_Rb_tree"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %2, align 8
  %3 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %2, align 8
  %4 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %3, i32 0, i32 0
  %5 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %4 to %"struct.std::_Rb_tree_key_compare"*
  %6 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %5, i32 0, i32 0
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca %"struct.std::piecewise_construct_t"*, align 8
  store %"struct.std::piecewise_construct_t"* %0, %"struct.std::piecewise_construct_t"** %2, align 8
  %3 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %2, align 8
  ret %"struct.std::piecewise_construct_t"* %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"class.std::tuple"*, align 8
  store %"class.std::tuple"* %0, %"class.std::tuple"** %2, align 8
  %3 = load %"class.std::tuple"*, %"class.std::tuple"** %2, align 8
  ret %"class.std::tuple"* %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca %"class.std::tuple.23"*, align 8
  store %"class.std::tuple.23"* %0, %"class.std::tuple.23"** %2, align 8
  %3 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %2, align 8
  ret %"class.std::tuple.23"* %3
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeC2IJRKSt21piecewise_construct_tSt5tupleIJRS1_EESE_IJEEEEERS8_DpOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %0, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) unnamed_addr #7 comdat align 2 {
  %6 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, align 8
  %7 = alloca %"class.std::_Rb_tree"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %6, align 8
  store %"class.std::_Rb_tree"* %1, %"class.std::_Rb_tree"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple"* %3, %"class.std::tuple"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %11 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %6, align 8
  %12 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %11, i32 0, i32 0
  %13 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %7, align 8
  store %"class.std::_Rb_tree"* %13, %"class.std::_Rb_tree"** %12, align 8
  %14 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %11, i32 0, i32 1
  %15 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %7, align 8
  %16 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %17 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %16) #3
  %18 = load %"class.std::tuple"*, %"class.std::tuple"** %9, align 8
  %19 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %18) #3
  %20 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %21 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %20) #3
  %22 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEEPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %15, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %17, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %19, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %21)
  store %"struct.std::_Rb_tree_node"* %22, %"struct.std::_Rb_tree_node"** %14, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node6_M_keyEv(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %0) #8 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %2, align 8
  %3 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %3, i32 0, i32 1
  %5 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  %6 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E(%"struct.std::_Rb_tree_node"* noundef %5)
  ret i8* %6
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node9_M_insertES0_IPSt18_Rb_tree_node_baseSB_E(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %0, %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"* %2) #8 comdat align 2 {
  %4 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %5 = alloca %"struct.std::pair.20", align 8
  %6 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, align 8
  %7 = bitcast %"struct.std::pair.20"* %5 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %8 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %7, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %9 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %7, i32 0, i32 1
  store %"struct.std::_Rb_tree_node_base"* %2, %"struct.std::_Rb_tree_node_base"** %9, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %6, align 8
  %10 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %6, align 8
  %11 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %10, i32 0, i32 0
  %12 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %11, align 8
  %13 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %5, i32 0, i32 0
  %14 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %13, align 8
  %15 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %5, i32 0, i32 1
  %16 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %15, align 8
  %17 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %10, i32 0, i32 1
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %17, align 8
  %19 = call %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %12, %"struct.std::_Rb_tree_node_base"* noundef %14, %"struct.std::_Rb_tree_node_base"* noundef %16, %"struct.std::_Rb_tree_node"* noundef %18)
  %20 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %4, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %19, %"struct.std::_Rb_tree_node_base"** %20, align 8
  %21 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %10, i32 0, i32 1
  store %"struct.std::_Rb_tree_node"* null, %"struct.std::_Rb_tree_node"** %21, align 8
  %22 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %4, i32 0, i32 0
  %23 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %22, align 8
  ret %"struct.std::_Rb_tree_node_base"* %23
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeD2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %0) unnamed_addr #6 comdat align 2 {
  %2 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %2, align 8
  %3 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %3, i32 0, i32 1
  %5 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %4, align 8
  %6 = icmp ne %"struct.std::_Rb_tree_node"* %5, null
  br i1 %6, label %7, label %12

7:                                                ; preds = %1
  %8 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %3, i32 0, i32 0
  %9 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %8, align 8
  %10 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %3, i32 0, i32 1
  %11 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %10, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE12_M_drop_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %9, %"struct.std::_Rb_tree_node"* noundef %11) #3
  br label %12

12:                                               ; preds = %7, %1
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEEPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %1, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %2, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %3) #8 comdat align 2 {
  %5 = alloca %"class.std::_Rb_tree"*, align 8
  %6 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %7 = alloca %"class.std::tuple"*, align 8
  %8 = alloca %"class.std::tuple.23"*, align 8
  %9 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %5, align 8
  store %"struct.std::piecewise_construct_t"* %1, %"struct.std::piecewise_construct_t"** %6, align 8
  store %"class.std::tuple"* %2, %"class.std::tuple"** %7, align 8
  store %"class.std::tuple.23"* %3, %"class.std::tuple.23"** %8, align 8
  %10 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %5, align 8
  %11 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_get_nodeEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %10)
  store %"struct.std::_Rb_tree_node"* %11, %"struct.std::_Rb_tree_node"** %9, align 8
  %12 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %13 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %6, align 8
  %14 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %13) #3
  %15 = load %"class.std::tuple"*, %"class.std::tuple"** %7, align 8
  %16 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %15) #3
  %17 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %8, align 8
  %18 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %17) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEEvPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %10, %"struct.std::_Rb_tree_node"* noundef %12, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %14, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %16, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %18)
  %19 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  ret %"struct.std::_Rb_tree_node"* %19
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEEvPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %6 = alloca %"class.std::_Rb_tree"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  %11 = alloca i8*, align 8
  %12 = alloca i32, align 4
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %6, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple"* %3, %"class.std::tuple"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %13 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %6, align 8
  %14 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %15 = bitcast %"struct.std::_Rb_tree_node"* %14 to i8*
  %16 = bitcast i8* %15 to %"struct.std::_Rb_tree_node"*
  %17 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13) #3
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %19 = call noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %18)
  %20 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %21 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %20) #3
  %22 = load %"class.std::tuple"*, %"class.std::tuple"** %9, align 8
  %23 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %22) #3
  %24 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %25 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %24) #3
  invoke void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJRS2_EESB_IJEEEEEvRS5_PT_DpOT0_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %17, %"struct.std::pair"* noundef %19, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %21, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %23, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %25)
          to label %26 unwind label %27

26:                                               ; preds = %5
  br label %41

27:                                               ; preds = %5
  %28 = landingpad { i8*, i32 }
          catch i8* null
  %29 = extractvalue { i8*, i32 } %28, 0
  store i8* %29, i8** %11, align 8
  %30 = extractvalue { i8*, i32 } %28, 1
  store i32 %30, i32* %12, align 4
  br label %31

31:                                               ; preds = %27
  %32 = load i8*, i8** %11, align 8
  %33 = call i8* @__cxa_begin_catch(i8* %32) #3
  %34 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %35 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %35) #3
  invoke void @__cxa_rethrow() #19
          to label %50 unwind label %36

36:                                               ; preds = %31
  %37 = landingpad { i8*, i32 }
          cleanup
  %38 = extractvalue { i8*, i32 } %37, 0
  store i8* %38, i8** %11, align 8
  %39 = extractvalue { i8*, i32 } %37, 1
  store i32 %39, i32* %12, align 4
  invoke void @__cxa_end_catch()
          to label %40 unwind label %47

40:                                               ; preds = %36
  br label %42

41:                                               ; preds = %26
  ret void

42:                                               ; preds = %40
  %43 = load i8*, i8** %11, align 8
  %44 = load i32, i32* %12, align 4
  %45 = insertvalue { i8*, i32 } undef, i8* %43, 0
  %46 = insertvalue { i8*, i32 } %45, i32 %44, 1
  resume { i8*, i32 } %46

47:                                               ; preds = %36
  %48 = landingpad { i8*, i32 }
          catch i8* null
  %49 = extractvalue { i8*, i32 } %48, 0
  call void @__clang_call_terminate(i8* %49) #16
  unreachable

50:                                               ; preds = %31
  unreachable
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJRS2_EESB_IJEEEEEvRS5_PT_DpOT0_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 {
  %6 = alloca %"class.std::allocator"*, align 8
  %7 = alloca %"struct.std::pair"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %6, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple"* %3, %"class.std::tuple"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %11 = load %"class.std::allocator"*, %"class.std::allocator"** %6, align 8
  %12 = bitcast %"class.std::allocator"* %11 to %"class.std::__new_allocator"*
  %13 = load %"struct.std::pair"*, %"struct.std::pair"** %7, align 8
  %14 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %15 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %14) #3
  %16 = load %"class.std::tuple"*, %"class.std::tuple"** %9, align 8
  %17 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %16) #3
  %18 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %19 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %18) #3
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJRS2_EESA_IJEEEEEvPT_DpOT0_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %12, %"struct.std::pair"* noundef %13, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %15, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %17, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %19)
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJRS2_EESA_IJEEEEEvPT_DpOT0_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 {
  %6 = alloca %"class.std::__new_allocator"*, align 8
  %7 = alloca %"struct.std::pair"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  %11 = alloca %"struct.std::piecewise_construct_t", align 1
  %12 = alloca %"class.std::tuple", align 8
  %13 = alloca %"class.std::tuple.23", align 1
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %6, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple"* %3, %"class.std::tuple"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %14 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %6, align 8
  %15 = load %"struct.std::pair"*, %"struct.std::pair"** %7, align 8
  %16 = bitcast %"struct.std::pair"* %15 to i8*
  %17 = bitcast i8* %16 to %"struct.std::pair"*
  %18 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %19 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %18) #3
  %20 = load %"class.std::tuple"*, %"class.std::tuple"** %9, align 8
  %21 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple"* @_ZSt7forwardISt5tupleIJRKcEEEOT_RNSt16remove_referenceIS4_E4typeE(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %20) #3
  call void @_ZNSt5tupleIJRKcEEC2EOS2_(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %12, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %21) #3
  %22 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %23 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %22) #3
  call void @_ZNSt4pairIKcdEC2IJRS0_EJEEESt21piecewise_construct_tSt5tupleIJDpT_EES5_IJDpT0_EE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %17, %"class.std::tuple"* noundef %12)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt5tupleIJRKcEEC2EOS2_(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %0, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::tuple"*, align 8
  %4 = alloca %"class.std::tuple"*, align 8
  store %"class.std::tuple"* %0, %"class.std::tuple"** %3, align 8
  store %"class.std::tuple"* %1, %"class.std::tuple"** %4, align 8
  %5 = load %"class.std::tuple"*, %"class.std::tuple"** %3, align 8
  %6 = bitcast %"class.std::tuple"* %5 to %"struct.std::_Tuple_impl"*
  %7 = load %"class.std::tuple"*, %"class.std::tuple"** %4, align 8
  %8 = bitcast %"class.std::tuple"* %7 to %"struct.std::_Tuple_impl"*
  call void @_ZNSt11_Tuple_implILm0EJRKcEEC2EOS2_(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %8) #3
  ret void
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIKcdEC2IJRS0_EJEEESt21piecewise_construct_tSt5tupleIJDpT_EES5_IJDpT0_EE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %0, %"class.std::tuple"* noundef %1) unnamed_addr #7 comdat align 2 {
  %3 = alloca %"struct.std::piecewise_construct_t", align 1
  %4 = alloca %"class.std::tuple.23", align 1
  %5 = alloca %"struct.std::pair"*, align 8
  %6 = alloca %"struct.std::_Index_tuple", align 1
  %7 = alloca %"struct.std::_Index_tuple.24", align 1
  store %"struct.std::pair"* %0, %"struct.std::pair"** %5, align 8
  %8 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  call void @_ZNSt4pairIKcdEC2IJRS0_EJLm0EEJEJEEERSt5tupleIJDpT_EERS4_IJDpT1_EESt12_Index_tupleIJXspT0_EEESD_IJXspT2_EEE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %8, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %1, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt11_Tuple_implILm0EJRKcEEC2EOS2_(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Tuple_impl"*, align 8
  %4 = alloca %"struct.std::_Tuple_impl"*, align 8
  store %"struct.std::_Tuple_impl"* %0, %"struct.std::_Tuple_impl"** %3, align 8
  store %"struct.std::_Tuple_impl"* %1, %"struct.std::_Tuple_impl"** %4, align 8
  %5 = load %"struct.std::_Tuple_impl"*, %"struct.std::_Tuple_impl"** %3, align 8
  %6 = bitcast %"struct.std::_Tuple_impl"* %5 to %"struct.std::_Head_base"*
  %7 = load %"struct.std::_Tuple_impl"*, %"struct.std::_Tuple_impl"** %4, align 8
  %8 = bitcast %"struct.std::_Tuple_impl"* %7 to %"struct.std::_Head_base"*
  %9 = bitcast %"struct.std::_Head_base"* %6 to i8*
  %10 = bitcast %"struct.std::_Head_base"* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %9, i8* align 8 %10, i64 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIKcdEC2IJRS0_EJLm0EEJEJEEERSt5tupleIJDpT_EERS4_IJDpT1_EESt12_Index_tupleIJXspT0_EEESD_IJXspT2_EEE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %0, %"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %1, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %2) unnamed_addr #6 comdat align 2 {
  %4 = alloca %"struct.std::_Index_tuple", align 1
  %5 = alloca %"struct.std::_Index_tuple.24", align 1
  %6 = alloca %"struct.std::pair"*, align 8
  %7 = alloca %"class.std::tuple"*, align 8
  %8 = alloca %"class.std::tuple.23"*, align 8
  store %"struct.std::pair"* %0, %"struct.std::pair"** %6, align 8
  store %"class.std::tuple"* %1, %"class.std::tuple"** %7, align 8
  store %"class.std::tuple.23"* %2, %"class.std::tuple.23"** %8, align 8
  %9 = load %"struct.std::pair"*, %"struct.std::pair"** %6, align 8
  %10 = bitcast %"struct.std::pair"* %9 to %"class.std::__pair_base"*
  %11 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %9, i32 0, i32 0
  %12 = load %"class.std::tuple"*, %"class.std::tuple"** %7, align 8
  %13 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt3getILm0EJRKcEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS6_(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %12) #3
  %14 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIRKcEOT_RNSt16remove_referenceIS2_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %13) #3
  %15 = load i8, i8* %14, align 1
  store i8 %15, i8* %11, align 8
  %16 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %9, i32 0, i32 1
  store double 0.000000e+00, double* %16, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIRKcEOT_RNSt16remove_referenceIS2_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  ret i8* %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt3getILm0EJRKcEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS6_(%"class.std::tuple"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"class.std::tuple"*, align 8
  store %"class.std::tuple"* %0, %"class.std::tuple"** %2, align 8
  %3 = load %"class.std::tuple"*, %"class.std::tuple"** %2, align 8
  %4 = bitcast %"class.std::tuple"* %3 to %"struct.std::_Tuple_impl"*
  %5 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt12__get_helperILm0ERKcJEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %4) #3
  ret i8* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt12__get_helperILm0ERKcJEERT0_RSt11_Tuple_implIXT_EJS2_DpT1_EE(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"struct.std::_Tuple_impl"*, align 8
  store %"struct.std::_Tuple_impl"* %0, %"struct.std::_Tuple_impl"** %2, align 8
  %3 = load %"struct.std::_Tuple_impl"*, %"struct.std::_Tuple_impl"** %2, align 8
  %4 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt11_Tuple_implILm0EJRKcEE7_M_headERS2_(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %3) #3
  ret i8* %4
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt11_Tuple_implILm0EJRKcEE7_M_headERS2_(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Tuple_impl"*, align 8
  store %"struct.std::_Tuple_impl"* %0, %"struct.std::_Tuple_impl"** %2, align 8
  %3 = load %"struct.std::_Tuple_impl"*, %"struct.std::_Tuple_impl"** %2, align 8
  %4 = bitcast %"struct.std::_Tuple_impl"* %3 to %"struct.std::_Head_base"*
  %5 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt10_Head_baseILm0ERKcLb0EE7_M_headERS2_(%"struct.std::_Head_base"* noundef nonnull align 8 dereferenceable(8) %4) #3
  ret i8* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt10_Head_baseILm0ERKcLb0EE7_M_headERS2_(%"struct.std::_Head_base"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Head_base"*, align 8
  store %"struct.std::_Head_base"* %0, %"struct.std::_Head_base"** %2, align 8
  %3 = load %"struct.std::_Head_base"*, %"struct.std::_Head_base"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Head_base", %"struct.std::_Head_base"* %3, i32 0, i32 0
  %5 = load i8*, i8** %4, align 8
  ret i8* %5
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_insert_nodeEPSt18_Rb_tree_node_baseSA_PSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node_base"* noundef %1, %"struct.std::_Rb_tree_node_base"* noundef %2, %"struct.std::_Rb_tree_node"* noundef %3) #8 comdat align 2 {
  %5 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %6 = alloca %"class.std::_Rb_tree"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %8 = alloca %"struct.std::_Rb_tree_node_base"*, align 8
  %9 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %10 = alloca i8, align 1
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %6, align 8
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %7, align 8
  store %"struct.std::_Rb_tree_node_base"* %2, %"struct.std::_Rb_tree_node_base"** %8, align 8
  store %"struct.std::_Rb_tree_node"* %3, %"struct.std::_Rb_tree_node"** %9, align 8
  %11 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %6, align 8
  %12 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %7, align 8
  %13 = icmp ne %"struct.std::_Rb_tree_node_base"* %12, null
  br i1 %13, label %27, label %14

14:                                               ; preds = %4
  %15 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %16 = call noundef %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_M_endEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %11) #3
  %17 = icmp eq %"struct.std::_Rb_tree_node_base"* %15, %16
  br i1 %17, label %27, label %18

18:                                               ; preds = %14
  %19 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %11, i32 0, i32 0
  %20 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %19 to %"struct.std::_Rb_tree_key_compare"*
  %21 = getelementptr inbounds %"struct.std::_Rb_tree_key_compare", %"struct.std::_Rb_tree_key_compare"* %20, i32 0, i32 0
  %22 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %23 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt13_Rb_tree_nodeIS2_E(%"struct.std::_Rb_tree_node"* noundef %22)
  %24 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %25 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE6_S_keyEPKSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_node_base"* noundef %24)
  %26 = call noundef zeroext i1 @_ZNKSt4lessIcEclERKcS2_(%"struct.std::less"* noundef nonnull align 1 dereferenceable(1) %21, i8* noundef nonnull align 1 dereferenceable(1) %23, i8* noundef nonnull align 1 dereferenceable(1) %25)
  br label %27

27:                                               ; preds = %18, %14, %4
  %28 = phi i1 [ true, %14 ], [ true, %4 ], [ %26, %18 ]
  %29 = zext i1 %28 to i8
  store i8 %29, i8* %10, align 1
  %30 = load i8, i8* %10, align 1
  %31 = trunc i8 %30 to i1
  %32 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %33 = bitcast %"struct.std::_Rb_tree_node"* %32 to %"struct.std::_Rb_tree_node_base"*
  %34 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %8, align 8
  %35 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %11, i32 0, i32 0
  %36 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %35 to i8*
  %37 = getelementptr inbounds i8, i8* %36, i64 8
  %38 = bitcast i8* %37 to %"struct.std::_Rb_tree_header"*
  %39 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %38, i32 0, i32 0
  call void @_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_(i1 noundef zeroext %31, %"struct.std::_Rb_tree_node_base"* noundef %33, %"struct.std::_Rb_tree_node_base"* noundef %34, %"struct.std::_Rb_tree_node_base"* noundef nonnull align 8 dereferenceable(32) %39) #3
  %40 = getelementptr inbounds %"class.std::_Rb_tree", %"class.std::_Rb_tree"* %11, i32 0, i32 0
  %41 = bitcast %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Rb_tree_impl"* %40 to i8*
  %42 = getelementptr inbounds i8, i8* %41, i64 8
  %43 = bitcast i8* %42 to %"struct.std::_Rb_tree_header"*
  %44 = getelementptr inbounds %"struct.std::_Rb_tree_header", %"struct.std::_Rb_tree_header"* %43, i32 0, i32 1
  %45 = load i64, i64* %44, align 8
  %46 = add i64 %45, 1
  store i64 %46, i64* %44, align 8
  %47 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %48 = bitcast %"struct.std::_Rb_tree_node"* %47 to %"struct.std::_Rb_tree_node_base"*
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %5, %"struct.std::_Rb_tree_node_base"* noundef %48) #3
  %49 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %5, i32 0, i32 0
  %50 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %49, align 8
  ret %"struct.std::_Rb_tree_node_base"* %50
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt11_Tuple_implILm0EJRKcEEC2ES1_(%"struct.std::_Tuple_impl"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #7 comdat align 2 {
  %3 = alloca %"struct.std::_Tuple_impl"*, align 8
  %4 = alloca i8*, align 8
  store %"struct.std::_Tuple_impl"* %0, %"struct.std::_Tuple_impl"** %3, align 8
  store i8* %1, i8** %4, align 8
  %5 = load %"struct.std::_Tuple_impl"*, %"struct.std::_Tuple_impl"** %3, align 8
  %6 = bitcast %"struct.std::_Tuple_impl"* %5 to %"struct.std::_Head_base"*
  %7 = load i8*, i8** %4, align 8
  call void @_ZNSt10_Head_baseILm0ERKcLb0EEC2ES1_(%"struct.std::_Head_base"* noundef nonnull align 8 dereferenceable(8) %6, i8* noundef nonnull align 1 dereferenceable(1) %7)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt10_Head_baseILm0ERKcLb0EEC2ES1_(%"struct.std::_Head_base"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Head_base"*, align 8
  %4 = alloca i8*, align 8
  store %"struct.std::_Head_base"* %0, %"struct.std::_Head_base"** %3, align 8
  store i8* %1, i8** %4, align 8
  %5 = load %"struct.std::_Head_base"*, %"struct.std::_Head_base"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Head_base", %"struct.std::_Head_base"* %5, i32 0, i32 0
  %7 = load i8*, i8** %4, align 8
  store i8* %7, i8** %6, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %6 = alloca %"struct.std::_Rb_tree_iterator", align 8
  %7 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %8 = alloca %"class.std::_Rb_tree"*, align 8
  %9 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %10 = alloca %"class.std::tuple.25"*, align 8
  %11 = alloca %"class.std::tuple.23"*, align 8
  %12 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", align 8
  %13 = alloca %"struct.std::pair.20", align 8
  %14 = alloca %"struct.std::_Rb_tree_const_iterator", align 8
  %15 = alloca i8*, align 8
  %16 = alloca i32, align 4
  %17 = alloca %"struct.std::pair.20", align 8
  %18 = alloca i32, align 4
  %19 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %7, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %1, %"struct.std::_Rb_tree_node_base"** %19, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %8, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %9, align 8
  store %"class.std::tuple.25"* %3, %"class.std::tuple.25"** %10, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %11, align 8
  %20 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %8, align 8
  %21 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %9, align 8
  %22 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %21) #3
  %23 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %10, align 8
  %24 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %23) #3
  %25 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %11, align 8
  %26 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %25) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeC2IJRKSt21piecewise_construct_tSt5tupleIJOcEESE_IJEEEEERS8_DpOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %20, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %22, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %24, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %26)
  %27 = bitcast %"struct.std::_Rb_tree_const_iterator"* %14 to i8*
  %28 = bitcast %"struct.std::_Rb_tree_const_iterator"* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %27, i8* align 8 %28, i64 8, i1 false)
  %29 = invoke noundef nonnull align 1 dereferenceable(1) i8* @_ZNKSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node6_M_keyEv(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12)
          to label %30 unwind label %54

30:                                               ; preds = %5
  %31 = getelementptr inbounds %"struct.std::_Rb_tree_const_iterator", %"struct.std::_Rb_tree_const_iterator"* %14, i32 0, i32 0
  %32 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %31, align 8
  %33 = invoke { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %20, %"struct.std::_Rb_tree_node_base"* %32, i8* noundef nonnull align 1 dereferenceable(1) %29)
          to label %34 unwind label %54

34:                                               ; preds = %30
  %35 = bitcast %"struct.std::pair.20"* %13 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %36 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %35, i32 0, i32 0
  %37 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %33, 0
  store %"struct.std::_Rb_tree_node_base"* %37, %"struct.std::_Rb_tree_node_base"** %36, align 8
  %38 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %35, i32 0, i32 1
  %39 = extractvalue { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* } %33, 1
  store %"struct.std::_Rb_tree_node_base"* %39, %"struct.std::_Rb_tree_node_base"** %38, align 8
  %40 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %13, i32 0, i32 1
  %41 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %40, align 8
  %42 = icmp ne %"struct.std::_Rb_tree_node_base"* %41, null
  br i1 %42, label %43, label %58

43:                                               ; preds = %34
  %44 = bitcast %"struct.std::pair.20"* %17 to i8*
  %45 = bitcast %"struct.std::pair.20"* %13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %44, i8* align 8 %45, i64 16, i1 false)
  %46 = bitcast %"struct.std::pair.20"* %17 to { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }*
  %47 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %46, i32 0, i32 0
  %48 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %47, align 8
  %49 = getelementptr inbounds { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }, { %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"* }* %46, i32 0, i32 1
  %50 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %49, align 8
  %51 = invoke %"struct.std::_Rb_tree_node_base"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_node9_M_insertES0_IPSt18_Rb_tree_node_baseSB_E(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12, %"struct.std::_Rb_tree_node_base"* %48, %"struct.std::_Rb_tree_node_base"* %50)
          to label %52 unwind label %54

52:                                               ; preds = %43
  %53 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  store %"struct.std::_Rb_tree_node_base"* %51, %"struct.std::_Rb_tree_node_base"** %53, align 8
  store i32 1, i32* %18, align 4
  br label %61

54:                                               ; preds = %43, %30, %5
  %55 = landingpad { i8*, i32 }
          cleanup
  %56 = extractvalue { i8*, i32 } %55, 0
  store i8* %56, i8** %15, align 8
  %57 = extractvalue { i8*, i32 } %55, 1
  store i32 %57, i32* %16, align 4
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeD2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12) #3
  br label %64

58:                                               ; preds = %34
  %59 = getelementptr inbounds %"struct.std::pair.20", %"struct.std::pair.20"* %13, i32 0, i32 0
  %60 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %59, align 8
  call void @_ZNSt17_Rb_tree_iteratorISt4pairIKcdEEC2EPSt18_Rb_tree_node_base(%"struct.std::_Rb_tree_iterator"* noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Rb_tree_node_base"* noundef %60) #3
  store i32 1, i32* %18, align 4
  br label %61

61:                                               ; preds = %58, %52
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeD2Ev(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %12) #3
  %62 = getelementptr inbounds %"struct.std::_Rb_tree_iterator", %"struct.std::_Rb_tree_iterator"* %6, i32 0, i32 0
  %63 = load %"struct.std::_Rb_tree_node_base"*, %"struct.std::_Rb_tree_node_base"** %62, align 8
  ret %"struct.std::_Rb_tree_node_base"* %63

64:                                               ; preds = %54
  %65 = load i8*, i8** %15, align 8
  %66 = load i32, i32* %16, align 4
  %67 = insertvalue { i8*, i32 } undef, i8* %65, 0
  %68 = insertvalue { i8*, i32 } %67, i32 %66, 1
  resume { i8*, i32 } %68
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZSt16forward_as_tupleIJcEESt5tupleIJDpOT_EES3_(%"class.std::tuple.25"* noalias sret(%"class.std::tuple.25") align 8 %0, i8* noundef nonnull align 1 dereferenceable(1) %1) #4 comdat {
  %3 = alloca i8*, align 8
  %4 = alloca i8*, align 8
  %5 = bitcast %"class.std::tuple.25"* %0 to i8*
  store i8* %5, i8** %3, align 8
  store i8* %1, i8** %4, align 8
  %6 = load i8*, i8** %4, align 8
  %7 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIcEOT_RNSt16remove_referenceIS0_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %6) #3
  call void @_ZNSt5tupleIJOcEEC2IJcELb1ELb1EEEDpOT_(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %7) #3
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt4moveIRcEONSt16remove_referenceIT_E4typeEOS2_(i8* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  ret i8* %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"class.std::tuple.25"*, align 8
  store %"class.std::tuple.25"* %0, %"class.std::tuple.25"** %2, align 8
  %3 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %2, align 8
  ret %"class.std::tuple.25"* %3
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE10_Auto_nodeC2IJRKSt21piecewise_construct_tSt5tupleIJOcEESE_IJEEEEERS8_DpOT_(%"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* noundef nonnull align 8 dereferenceable(16) %0, %"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) unnamed_addr #7 comdat align 2 {
  %6 = alloca %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, align 8
  %7 = alloca %"class.std::_Rb_tree"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple.25"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  store %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %0, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %6, align 8
  store %"class.std::_Rb_tree"* %1, %"class.std::_Rb_tree"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple.25"* %3, %"class.std::tuple.25"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %11 = load %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"*, %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"** %6, align 8
  %12 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %11, i32 0, i32 0
  %13 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %7, align 8
  store %"class.std::_Rb_tree"* %13, %"class.std::_Rb_tree"** %12, align 8
  %14 = getelementptr inbounds %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node", %"struct.std::_Rb_tree<char, std::pair<const char, double>, std::_Select1st<std::pair<const char, double>>, std::less<char>>::_Auto_node"* %11, i32 0, i32 1
  %15 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %7, align 8
  %16 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %17 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %16) #3
  %18 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %9, align 8
  %19 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %18) #3
  %20 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %21 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %20) #3
  %22 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEEPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %15, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %17, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %19, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %21)
  store %"struct.std::_Rb_tree_node"* %22, %"struct.std::_Rb_tree_node"** %14, align 8
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE14_M_create_nodeIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEEPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %1, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %2, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %3) #8 comdat align 2 {
  %5 = alloca %"class.std::_Rb_tree"*, align 8
  %6 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %7 = alloca %"class.std::tuple.25"*, align 8
  %8 = alloca %"class.std::tuple.23"*, align 8
  %9 = alloca %"struct.std::_Rb_tree_node"*, align 8
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %5, align 8
  store %"struct.std::piecewise_construct_t"* %1, %"struct.std::piecewise_construct_t"** %6, align 8
  store %"class.std::tuple.25"* %2, %"class.std::tuple.25"** %7, align 8
  store %"class.std::tuple.23"* %3, %"class.std::tuple.23"** %8, align 8
  %10 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %5, align 8
  %11 = call noundef %"struct.std::_Rb_tree_node"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_get_nodeEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %10)
  store %"struct.std::_Rb_tree_node"* %11, %"struct.std::_Rb_tree_node"** %9, align 8
  %12 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  %13 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %6, align 8
  %14 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %13) #3
  %15 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %7, align 8
  %16 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %15) #3
  %17 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %8, align 8
  %18 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %17) #3
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEEvPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %10, %"struct.std::_Rb_tree_node"* noundef %12, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %14, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %16, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %18)
  %19 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %9, align 8
  ret %"struct.std::_Rb_tree_node"* %19
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJOcEESD_IJEEEEEvPSt13_Rb_tree_nodeIS2_EDpOT_(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %0, %"struct.std::_Rb_tree_node"* noundef %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %6 = alloca %"class.std::_Rb_tree"*, align 8
  %7 = alloca %"struct.std::_Rb_tree_node"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple.25"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  %11 = alloca i8*, align 8
  %12 = alloca i32, align 4
  store %"class.std::_Rb_tree"* %0, %"class.std::_Rb_tree"** %6, align 8
  store %"struct.std::_Rb_tree_node"* %1, %"struct.std::_Rb_tree_node"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple.25"* %3, %"class.std::tuple.25"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %13 = load %"class.std::_Rb_tree"*, %"class.std::_Rb_tree"** %6, align 8
  %14 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %15 = bitcast %"struct.std::_Rb_tree_node"* %14 to i8*
  %16 = bitcast i8* %15 to %"struct.std::_Rb_tree_node"*
  %17 = call noundef nonnull align 1 dereferenceable(1) %"class.std::allocator"* @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE21_M_get_Node_allocatorEv(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13) #3
  %18 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %19 = call noundef %"struct.std::pair"* @_ZNSt13_Rb_tree_nodeISt4pairIKcdEE9_M_valptrEv(%"struct.std::_Rb_tree_node"* noundef nonnull align 8 dereferenceable(48) %18)
  %20 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %21 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %20) #3
  %22 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %9, align 8
  %23 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %22) #3
  %24 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %25 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %24) #3
  invoke void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJOcEESB_IJEEEEEvRS5_PT_DpOT0_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %17, %"struct.std::pair"* noundef %19, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %21, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %23, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %25)
          to label %26 unwind label %27

26:                                               ; preds = %5
  br label %41

27:                                               ; preds = %5
  %28 = landingpad { i8*, i32 }
          catch i8* null
  %29 = extractvalue { i8*, i32 } %28, 0
  store i8* %29, i8** %11, align 8
  %30 = extractvalue { i8*, i32 } %28, 1
  store i32 %30, i32* %12, align 4
  br label %31

31:                                               ; preds = %27
  %32 = load i8*, i8** %11, align 8
  %33 = call i8* @__cxa_begin_catch(i8* %32) #3
  %34 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  %35 = load %"struct.std::_Rb_tree_node"*, %"struct.std::_Rb_tree_node"** %7, align 8
  call void @_ZNSt8_Rb_treeIcSt4pairIKcdESt10_Select1stIS2_ESt4lessIcESaIS2_EE11_M_put_nodeEPSt13_Rb_tree_nodeIS2_E(%"class.std::_Rb_tree"* noundef nonnull align 8 dereferenceable(48) %13, %"struct.std::_Rb_tree_node"* noundef %35) #3
  invoke void @__cxa_rethrow() #19
          to label %50 unwind label %36

36:                                               ; preds = %31
  %37 = landingpad { i8*, i32 }
          cleanup
  %38 = extractvalue { i8*, i32 } %37, 0
  store i8* %38, i8** %11, align 8
  %39 = extractvalue { i8*, i32 } %37, 1
  store i32 %39, i32* %12, align 4
  invoke void @__cxa_end_catch()
          to label %40 unwind label %47

40:                                               ; preds = %36
  br label %42

41:                                               ; preds = %26
  ret void

42:                                               ; preds = %40
  %43 = load i8*, i8** %11, align 8
  %44 = load i32, i32* %12, align 4
  %45 = insertvalue { i8*, i32 } undef, i8* %43, 0
  %46 = insertvalue { i8*, i32 } %45, i32 %44, 1
  resume { i8*, i32 } %46

47:                                               ; preds = %36
  %48 = landingpad { i8*, i32 }
          catch i8* null
  %49 = extractvalue { i8*, i32 } %48, 0
  call void @__clang_call_terminate(i8* %49) #16
  unreachable

50:                                               ; preds = %31
  unreachable
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt16allocator_traitsISaISt13_Rb_tree_nodeISt4pairIKcdEEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJOcEESB_IJEEEEEvRS5_PT_DpOT0_(%"class.std::allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 {
  %6 = alloca %"class.std::allocator"*, align 8
  %7 = alloca %"struct.std::pair"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple.25"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  store %"class.std::allocator"* %0, %"class.std::allocator"** %6, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple.25"* %3, %"class.std::tuple.25"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %11 = load %"class.std::allocator"*, %"class.std::allocator"** %6, align 8
  %12 = bitcast %"class.std::allocator"* %11 to %"class.std::__new_allocator"*
  %13 = load %"struct.std::pair"*, %"struct.std::pair"** %7, align 8
  %14 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %15 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %14) #3
  %16 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %9, align 8
  %17 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %16) #3
  %18 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %19 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %18) #3
  call void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJOcEESA_IJEEEEEvPT_DpOT0_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %12, %"struct.std::pair"* noundef %13, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %15, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %17, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %19)
  ret void
}

; Function Attrs: mustprogress noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt15__new_allocatorISt13_Rb_tree_nodeISt4pairIKcdEEE9constructIS3_JRKSt21piecewise_construct_tSt5tupleIJOcEESA_IJEEEEEvPT_DpOT0_(%"class.std::__new_allocator"* noundef nonnull align 1 dereferenceable(1) %0, %"struct.std::pair"* noundef %1, %"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %2, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %3, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4) #8 comdat align 2 {
  %6 = alloca %"class.std::__new_allocator"*, align 8
  %7 = alloca %"struct.std::pair"*, align 8
  %8 = alloca %"struct.std::piecewise_construct_t"*, align 8
  %9 = alloca %"class.std::tuple.25"*, align 8
  %10 = alloca %"class.std::tuple.23"*, align 8
  %11 = alloca %"struct.std::piecewise_construct_t", align 1
  %12 = alloca %"class.std::tuple.25", align 8
  %13 = alloca %"class.std::tuple.23", align 1
  store %"class.std::__new_allocator"* %0, %"class.std::__new_allocator"** %6, align 8
  store %"struct.std::pair"* %1, %"struct.std::pair"** %7, align 8
  store %"struct.std::piecewise_construct_t"* %2, %"struct.std::piecewise_construct_t"** %8, align 8
  store %"class.std::tuple.25"* %3, %"class.std::tuple.25"** %9, align 8
  store %"class.std::tuple.23"* %4, %"class.std::tuple.23"** %10, align 8
  %14 = load %"class.std::__new_allocator"*, %"class.std::__new_allocator"** %6, align 8
  %15 = load %"struct.std::pair"*, %"struct.std::pair"** %7, align 8
  %16 = bitcast %"struct.std::pair"* %15 to i8*
  %17 = bitcast i8* %16 to %"struct.std::pair"*
  %18 = load %"struct.std::piecewise_construct_t"*, %"struct.std::piecewise_construct_t"** %8, align 8
  %19 = call noundef nonnull align 1 dereferenceable(1) %"struct.std::piecewise_construct_t"* @_ZSt7forwardIRKSt21piecewise_construct_tEOT_RNSt16remove_referenceIS3_E4typeE(%"struct.std::piecewise_construct_t"* noundef nonnull align 1 dereferenceable(1) %18) #3
  %20 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %9, align 8
  %21 = call noundef nonnull align 8 dereferenceable(8) %"class.std::tuple.25"* @_ZSt7forwardISt5tupleIJOcEEEOT_RNSt16remove_referenceIS3_E4typeE(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %20) #3
  call void @_ZNSt5tupleIJOcEEC2EOS1_(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %12, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %21) #3
  %22 = load %"class.std::tuple.23"*, %"class.std::tuple.23"** %10, align 8
  %23 = call noundef nonnull align 1 dereferenceable(1) %"class.std::tuple.23"* @_ZSt7forwardISt5tupleIJEEEOT_RNSt16remove_referenceIS2_E4typeE(%"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %22) #3
  call void @_ZNSt4pairIKcdEC2IJOcEJEEESt21piecewise_construct_tSt5tupleIJDpT_EES5_IJDpT0_EE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %17, %"class.std::tuple.25"* noundef %12)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt5tupleIJOcEEC2EOS1_(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %0, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"class.std::tuple.25"*, align 8
  %4 = alloca %"class.std::tuple.25"*, align 8
  store %"class.std::tuple.25"* %0, %"class.std::tuple.25"** %3, align 8
  store %"class.std::tuple.25"* %1, %"class.std::tuple.25"** %4, align 8
  %5 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %3, align 8
  %6 = bitcast %"class.std::tuple.25"* %5 to %"struct.std::_Tuple_impl.26"*
  %7 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %4, align 8
  %8 = bitcast %"class.std::tuple.25"* %7 to %"struct.std::_Tuple_impl.26"*
  call void @_ZNSt11_Tuple_implILm0EJOcEEC2EOS1_(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %6, %"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %8) #3
  ret void
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIKcdEC2IJOcEJEEESt21piecewise_construct_tSt5tupleIJDpT_EES5_IJDpT0_EE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %0, %"class.std::tuple.25"* noundef %1) unnamed_addr #7 comdat align 2 {
  %3 = alloca %"struct.std::piecewise_construct_t", align 1
  %4 = alloca %"class.std::tuple.23", align 1
  %5 = alloca %"struct.std::pair"*, align 8
  %6 = alloca %"struct.std::_Index_tuple", align 1
  %7 = alloca %"struct.std::_Index_tuple.24", align 1
  store %"struct.std::pair"* %0, %"struct.std::pair"** %5, align 8
  %8 = load %"struct.std::pair"*, %"struct.std::pair"** %5, align 8
  call void @_ZNSt4pairIKcdEC2IJOcEJLm0EEJEJEEERSt5tupleIJDpT_EERS4_IJDpT1_EESt12_Index_tupleIJXspT0_EEESD_IJXspT2_EEE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %8, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %1, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %4)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt11_Tuple_implILm0EJOcEEC2EOS1_(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %0, %"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Tuple_impl.26"*, align 8
  %4 = alloca %"struct.std::_Tuple_impl.26"*, align 8
  store %"struct.std::_Tuple_impl.26"* %0, %"struct.std::_Tuple_impl.26"** %3, align 8
  store %"struct.std::_Tuple_impl.26"* %1, %"struct.std::_Tuple_impl.26"** %4, align 8
  %5 = load %"struct.std::_Tuple_impl.26"*, %"struct.std::_Tuple_impl.26"** %3, align 8
  %6 = bitcast %"struct.std::_Tuple_impl.26"* %5 to %"struct.std::_Head_base.27"*
  %7 = load %"struct.std::_Tuple_impl.26"*, %"struct.std::_Tuple_impl.26"** %4, align 8
  %8 = bitcast %"struct.std::_Tuple_impl.26"* %7 to %"struct.std::_Head_base.27"*
  %9 = bitcast %"struct.std::_Head_base.27"* %6 to i8*
  %10 = bitcast %"struct.std::_Head_base.27"* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %9, i8* align 8 %10, i64 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt4pairIKcdEC2IJOcEJLm0EEJEJEEERSt5tupleIJDpT_EERS4_IJDpT1_EESt12_Index_tupleIJXspT0_EEESD_IJXspT2_EEE(%"struct.std::pair"* noundef nonnull align 8 dereferenceable(16) %0, %"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %1, %"class.std::tuple.23"* noundef nonnull align 1 dereferenceable(1) %2) unnamed_addr #6 comdat align 2 {
  %4 = alloca %"struct.std::_Index_tuple", align 1
  %5 = alloca %"struct.std::_Index_tuple.24", align 1
  %6 = alloca %"struct.std::pair"*, align 8
  %7 = alloca %"class.std::tuple.25"*, align 8
  %8 = alloca %"class.std::tuple.23"*, align 8
  store %"struct.std::pair"* %0, %"struct.std::pair"** %6, align 8
  store %"class.std::tuple.25"* %1, %"class.std::tuple.25"** %7, align 8
  store %"class.std::tuple.23"* %2, %"class.std::tuple.23"** %8, align 8
  %9 = load %"struct.std::pair"*, %"struct.std::pair"** %6, align 8
  %10 = bitcast %"struct.std::pair"* %9 to %"class.std::__pair_base"*
  %11 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %9, i32 0, i32 0
  %12 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %7, align 8
  %13 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt3getILm0EJOcEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS5_(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %12) #3
  %14 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIOcEOT_RNSt16remove_referenceIS1_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %13) #3
  %15 = load i8, i8* %14, align 1
  store i8 %15, i8* %11, align 8
  %16 = getelementptr inbounds %"struct.std::pair", %"struct.std::pair"* %9, i32 0, i32 1
  store double 0.000000e+00, double* %16, align 8
  ret void
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIOcEOT_RNSt16remove_referenceIS1_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  ret i8* %3
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt3getILm0EJOcEERNSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeERS5_(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"class.std::tuple.25"*, align 8
  store %"class.std::tuple.25"* %0, %"class.std::tuple.25"** %2, align 8
  %3 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %2, align 8
  %4 = bitcast %"class.std::tuple.25"* %3 to %"struct.std::_Tuple_impl.26"*
  %5 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt12__get_helperILm0EOcJEERT0_RSt11_Tuple_implIXT_EJS1_DpT1_EE(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %4) #3
  ret i8* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt12__get_helperILm0EOcJEERT0_RSt11_Tuple_implIXT_EJS1_DpT1_EE(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat {
  %2 = alloca %"struct.std::_Tuple_impl.26"*, align 8
  store %"struct.std::_Tuple_impl.26"* %0, %"struct.std::_Tuple_impl.26"** %2, align 8
  %3 = load %"struct.std::_Tuple_impl.26"*, %"struct.std::_Tuple_impl.26"** %2, align 8
  %4 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt11_Tuple_implILm0EJOcEE7_M_headERS1_(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %3) #3
  ret i8* %4
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt11_Tuple_implILm0EJOcEE7_M_headERS1_(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Tuple_impl.26"*, align 8
  store %"struct.std::_Tuple_impl.26"* %0, %"struct.std::_Tuple_impl.26"** %2, align 8
  %3 = load %"struct.std::_Tuple_impl.26"*, %"struct.std::_Tuple_impl.26"** %2, align 8
  %4 = bitcast %"struct.std::_Tuple_impl.26"* %3 to %"struct.std::_Head_base.27"*
  %5 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt10_Head_baseILm0EOcLb0EE7_M_headERS1_(%"struct.std::_Head_base.27"* noundef nonnull align 8 dereferenceable(8) %4) #3
  ret i8* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZNSt10_Head_baseILm0EOcLb0EE7_M_headERS1_(%"struct.std::_Head_base.27"* noundef nonnull align 8 dereferenceable(8) %0) #4 comdat align 2 {
  %2 = alloca %"struct.std::_Head_base.27"*, align 8
  store %"struct.std::_Head_base.27"* %0, %"struct.std::_Head_base.27"** %2, align 8
  %3 = load %"struct.std::_Head_base.27"*, %"struct.std::_Head_base.27"** %2, align 8
  %4 = getelementptr inbounds %"struct.std::_Head_base.27", %"struct.std::_Head_base.27"* %3, i32 0, i32 0
  %5 = load i8*, i8** %4, align 8
  ret i8* %5
}

; Function Attrs: mustprogress noinline nounwind optnone uwtable
define linkonce_odr dso_local noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIcEOT_RNSt16remove_referenceIS0_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %0) #4 comdat {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt5tupleIJOcEEC2IJcELb1ELb1EEEDpOT_(%"class.std::tuple.25"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %"class.std::tuple.25"*, align 8
  %4 = alloca i8*, align 8
  store %"class.std::tuple.25"* %0, %"class.std::tuple.25"** %3, align 8
  store i8* %1, i8** %4, align 8
  %5 = load %"class.std::tuple.25"*, %"class.std::tuple.25"** %3, align 8
  %6 = bitcast %"class.std::tuple.25"* %5 to %"struct.std::_Tuple_impl.26"*
  %7 = load i8*, i8** %4, align 8
  %8 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIcEOT_RNSt16remove_referenceIS0_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %7) #3
  invoke void @_ZNSt11_Tuple_implILm0EJOcEEC2IcEEOT_(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %6, i8* noundef nonnull align 1 dereferenceable(1) %8)
          to label %9 unwind label %10

9:                                                ; preds = %2
  ret void

10:                                               ; preds = %2
  %11 = landingpad { i8*, i32 }
          catch i8* null
  %12 = extractvalue { i8*, i32 } %11, 0
  call void @__clang_call_terminate(i8* %12) #16
  unreachable
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZNSt11_Tuple_implILm0EJOcEEC2IcEEOT_(%"struct.std::_Tuple_impl.26"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #7 comdat align 2 {
  %3 = alloca %"struct.std::_Tuple_impl.26"*, align 8
  %4 = alloca i8*, align 8
  store %"struct.std::_Tuple_impl.26"* %0, %"struct.std::_Tuple_impl.26"** %3, align 8
  store i8* %1, i8** %4, align 8
  %5 = load %"struct.std::_Tuple_impl.26"*, %"struct.std::_Tuple_impl.26"** %3, align 8
  %6 = bitcast %"struct.std::_Tuple_impl.26"* %5 to %"struct.std::_Head_base.27"*
  %7 = load i8*, i8** %4, align 8
  %8 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIcEOT_RNSt16remove_referenceIS0_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %7) #3
  call void @_ZNSt10_Head_baseILm0EOcLb0EEC2IcEEOT_(%"struct.std::_Head_base.27"* noundef nonnull align 8 dereferenceable(8) %6, i8* noundef nonnull align 1 dereferenceable(1) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZNSt10_Head_baseILm0EOcLb0EEC2IcEEOT_(%"struct.std::_Head_base.27"* noundef nonnull align 8 dereferenceable(8) %0, i8* noundef nonnull align 1 dereferenceable(1) %1) unnamed_addr #6 comdat align 2 {
  %3 = alloca %"struct.std::_Head_base.27"*, align 8
  %4 = alloca i8*, align 8
  store %"struct.std::_Head_base.27"* %0, %"struct.std::_Head_base.27"** %3, align 8
  store i8* %1, i8** %4, align 8
  %5 = load %"struct.std::_Head_base.27"*, %"struct.std::_Head_base.27"** %3, align 8
  %6 = getelementptr inbounds %"struct.std::_Head_base.27", %"struct.std::_Head_base.27"* %5, i32 0, i32 0
  %7 = load i8*, i8** %4, align 8
  %8 = call noundef nonnull align 1 dereferenceable(1) i8* @_ZSt7forwardIcEOT_RNSt16remove_referenceIS0_E4typeE(i8* noundef nonnull align 1 dereferenceable(1) %7) #3
  store i8* %8, i8** %6, align 8
  ret void
}

; Function Attrs: noinline uwtable
define internal void @_GLOBAL__sub_I_program.cpp() #0 section ".text.startup" {
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  ret void
}

attributes #0 = { noinline uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { nounwind "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nounwind }
attributes #4 = { mustprogress noinline nounwind optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { argmemonly nofree nounwind willreturn }
attributes #6 = { noinline nounwind optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #7 = { noinline optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #8 = { mustprogress noinline optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #9 = { mustprogress noinline norecurse optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #10 = { noinline noreturn nounwind }
attributes #11 = { nobuiltin nounwind "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #12 = { nounwind readonly willreturn "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #13 = { noreturn "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #14 = { nobuiltin allocsize(0) "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #15 = { nofree nosync nounwind readnone speculatable willreturn }
attributes #16 = { noreturn nounwind }
attributes #17 = { builtin nounwind }
attributes #18 = { nounwind readonly willreturn }
attributes #19 = { noreturn }
attributes #20 = { builtin allocsize(0) }

!llvm.module.flags = !{!0, !1, !2, !3, !4}
!llvm.ident = !{!5}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{i32 7, !"uwtable", i32 1}
!4 = !{i32 7, !"frame-pointer", i32 2}
!5 = !{!"Ubuntu clang version 14.0.0-1ubuntu1.1"}
!6 = distinct !{!6, !7}
!7 = !{!"llvm.loop.mustprogress"}
!8 = distinct !{!8, !7}
!9 = distinct !{!9, !7}
!10 = distinct !{!10, !7}
!11 = distinct !{!11, !7}
!12 = distinct !{!12, !7}
!13 = distinct !{!13, !7}
!14 = distinct !{!14, !7}
!15 = distinct !{!15, !7}
!16 = distinct !{!16, !7}
!17 = distinct !{!17, !7}
!18 = distinct !{!18, !7}
!19 = distinct !{!19, !7}
!20 = distinct !{!20, !7}
!21 = distinct !{!21, !7}
!22 = distinct !{!22, !7}
