#include <stddef.h>

struct simple_struct {
  int* a;
  int* b;
  int* c;
  int* d;
};

int main() {
  struct simple_struct a;
  a.a = NULL;
  a.b = a.a;
  a.c = a.b;
  a.d = a.c;
  int b = *a.d;
  return 0;
}
