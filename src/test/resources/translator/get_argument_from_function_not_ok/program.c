#include <stddef.h>

int* get_ref(int b) {
  if (b) {
    return NULL;
  } else {
    int a = 0;
    return &a;
  }
}

int main() {
  const int* a = get_ref(1);
  int b = *a;
}
