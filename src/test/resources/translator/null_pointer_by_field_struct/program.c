#include <stddef.h>

struct simple_struct {
  int* a;
};

int main() {
  struct simple_struct a;
  a.a = NULL;
  int b = *a.a;
  return 0;
}
