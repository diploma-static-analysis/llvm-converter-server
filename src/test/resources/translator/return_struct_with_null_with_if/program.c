#include <stddef.h>

struct simple_struct {
  int* a;
};

struct simple_struct get_struct(int is_null) {
  struct simple_struct a;
  if (is_null) {
    a.a = NULL;
  } else {
    int b = 0;
    a.a = &b;
  }
  return a;
}

int main() {
  const struct simple_struct a = get_struct(1);
  int b = *a.a;
  return 0;
}
