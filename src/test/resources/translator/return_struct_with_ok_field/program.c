#include <stddef.h>

struct simple_struct {
  int* a;
};

struct simple_struct get_struct() {
  struct simple_struct a;
  int b = 0;
  a.a = &b;
  return a;
}

int main() {
  const struct simple_struct a = get_struct();
  int b = *a.a;
  return 0;
}
