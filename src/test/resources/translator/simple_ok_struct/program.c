struct simple_struct {
  int* a;
};

int main() {
  struct simple_struct a;
  int subInt = 0;
  a.a = &subInt;
  int b = *a.a;
  return 0;
}
