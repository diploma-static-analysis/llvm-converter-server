#include <stdio.h>

int main() {
  int* null_var = NULL;
  int** plus_1 = &null_var;
  int*** plus_2 = &plus_1;
  int** plus_1_minus = *plus_2;
  int* null_var_minus = *plus_1_minus;
}